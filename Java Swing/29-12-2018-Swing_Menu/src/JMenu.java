import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JToolBar;
import javax.annotation.processing.Filer;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JSeparator;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;


public class JMenu extends JFrame {

	private JPanel contentPane;
	private boolean flagBackground;
	private boolean flagForceGround;
	private boolean changeState;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JMenu frame = new JMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JMenu() {
		changeState = true;
		flagBackground = true;
		flagForceGround = false;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1063, 574);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		javax.swing.JMenu mnColors = new javax.swing.JMenu("Colors");
		menuBar.add(mnColors);
		
		JMenuItem mntmRed = new JMenuItem("Red");
		mnColors.add(mntmRed);
		
		JMenuItem mntmBlue = new JMenuItem("Blue");
		mnColors.add(mntmBlue);
		
		JMenuItem mntmBlack = new JMenuItem("Black");
		mnColors.add(mntmBlack);
		
		JMenuItem mntmGreen = new JMenuItem("Green");
		mnColors.add(mntmGreen);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Clear");
		mnColors.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Exit");
		mnColors.add(mntmNewMenuItem_3);
		
		JSeparator separator = new JSeparator();
		mnColors.add(separator);
		
		javax.swing.JMenu mnFile = new javax.swing.JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 1037, 30);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Change Forceground");
		toolBar.add(btnNewButton);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 83, 242, 191);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JTextArea txtrText = new JTextArea();
		txtrText.setText("Text");
		txtrText.setBounds(10, 5, 222, 175);
		panel.add(txtrText);
		
			mntmRed.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (flagBackground){
						txtrText.setBackground(Color.RED);
					}
					if (flagForceGround){
						txtrText.setForeground(Color.RED);
					}
				}
			});
			
			mntmBlue.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (flagBackground){
						txtrText.setBackground(Color.BLUE);
					}
					if (flagForceGround){
						txtrText.setForeground(Color.BLUE);
					}
				}
			});
			
			mntmBlack.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (flagBackground){
						txtrText.setBackground(Color.BLACK);
					}
					if (flagForceGround){
						txtrText.setForeground(Color.BLACK);
					}
				}
			});
			
			mntmGreen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (flagBackground){
						txtrText.setBackground(Color.GREEN);
					}
					if (flagForceGround){
						txtrText.setForeground(Color.GREEN);
					}
				}
			});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*if (changeState){
					changeState = false;
					btnNewButton.setText("Change Background");
					flagBackground = false;
					flagForceGround = true;
				}else {
					changeState = true;
					btnNewButton.setText("Change Forceground");
					flagBackground = true;
					flagForceGround = false;
				}*/
				if (flagBackground && !flagForceGround){
					btnNewButton.setText("Change Background");
					flagBackground = false;
					flagForceGround = true;
				}else {
					btnNewButton.setText("Change Forceground");
					flagBackground = true;
					flagForceGround = false;
				}
				
				System.out.println("Back : " + flagBackground + "\n" + "Force : " + flagForceGround);
			}
		});
		
		/*mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File(fileName);
				try {
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					String lineData = "";
					do {
						if (br.readLine() != null)
							lineData += br.readLine();
					}while (br.readLine() != null);
					System.out.println(lineData);
					br.close();
					FileWriter fw = new FileWriter(file);
					lineData += ("\n" + txtrText.getText()+"\n");
					fw.write(lineData);
					fw.close();
					txtrText.setText("File saved successfully!!!");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File(fileName);
				try {
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					String lineData = "";
					do {
						if (br.readLine() != null)
							lineData += br.readLine();
					}while (br.readLine() != null);
					System.out.println(lineData);
					txtrText.setText(lineData);
					br.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});*/
		
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(DISPOSE_ON_CLOSE);
			}
		});
		
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtrText.setText("");
				txtrText.setBackground(null);
				txtrText.setForeground(null);
			}
		});
		
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser save = new JFileChooser();
				int saveornot = save.showSaveDialog(getParent());
				
				if (saveornot == save.APPROVE_OPTION){
					String fileName = save.getSelectedFile().getAbsolutePath();
					
					try {
						FileReader fr = new FileReader(fileName);
						BufferedReader br = new BufferedReader(fr);
						String lineData = "";
						String data = "";
						do {
							data = br.readLine();
							if (data != null) {
								lineData += (data + "\r\n");
								System.out.println(lineData);
							}
						}while (data != null);
						
						FileWriter fw = new FileWriter(fileName);
						BufferedWriter bw = new BufferedWriter(fw);
						System.out.println("GET : " + txtrText.getText());
						bw.write(lineData +/* "\r\n" + */txtrText.getText() + "\r\n");
						bw.close();
						txtrText.setText("File saved successfully!!!");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser open = new JFileChooser();
				int openornot = open.showOpenDialog(getParent());
				
				if (openornot == open.APPROVE_OPTION){
					String fileName = open.getSelectedFile().getAbsolutePath();
					
					try {
						FileReader fr = new FileReader(fileName);
						BufferedReader br = new BufferedReader(fr);
						String lineData = "";
						String data = "";
						do {
							data = br.readLine();
							if (data != null) {
								lineData += (data + "\r\n");
								System.out.println(lineData);
							}
						}while (data != null);
						txtrText.setText(lineData);
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});

	}
}
