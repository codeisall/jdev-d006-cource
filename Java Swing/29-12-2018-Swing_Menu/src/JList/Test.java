package JList;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Test extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		String[] listName = {"Nguyen", "Lam", "Thanh", "Michael"};	
		Vector<String> list1 = new Vector<>();
		Vector<String> list2 = new Vector<>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton(">");
		btnNewButton.setBounds(260, 70, 89, 23);
		contentPane.add(btnNewButton);
		
		JList list = new JList();
		list.setBounds(111, 73, 139, 224);
		contentPane.add(list);
		
		JList list_1 = new JList();
		list_1.setBounds(359, 73, 139, 224);
		contentPane.add(list_1);
		
		JButton btnNewButton_1 = new JButton("<");
		btnNewButton_1.setBounds(260, 106, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnTakeMany = new JButton("<<");
		
		btnTakeMany.setBounds(260, 274, 89, 23);
		contentPane.add(btnTakeMany);
		
		JButton btnGiveMany = new JButton(">>");
		btnGiveMany.setBounds(260, 240, 89, 23);
		contentPane.add(btnGiveMany);
		
		list1.addAll(Arrays.asList(listName));
		list.setListData(list1);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] name = list.getSelectedIndices();
				for (Integer i : name) {
					list2.add(list1.get(i));
					list1.remove(list1.get(i));
				}
				System.out.println(list1);
				list_1.setListData(list2);
				list.setListData(list1);
			}
		});
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] name = list_1.getSelectedIndices();
				for (Integer i : name) {
					list1.add(list2.get(i));
					list2.remove(list2.get(i));
				}
				System.out.println(list1);
				list_1.setListData(list2);
				list.setListData(list1);
			}
		});
		
		btnTakeMany.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] myArray = new String[list_1.getModel().getSize()];
				for (int i = 0; i < list_1.getModel().getSize(); i++) {
					myArray[i] = String.valueOf(list_1.getModel().getElementAt(i));
				}
				list1.addAll(Arrays.asList(myArray));
				list.setListData(list1);
				list2.removeAllElements();
				list_1.setListData(list2);
			}
		});
		
		btnGiveMany.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] myArray = new String[list.getModel().getSize()];
				for (int i = 0; i < list.getModel().getSize(); i++) {
					myArray[i] = String.valueOf(list.getModel().getElementAt(i));
				}
				list2.addAll(Arrays.asList(myArray));
				System.out.println(list2);
				list_1.setListData(list2);
				list1.removeAllElements();
				list.setListData(list1);
			}
		});
	}

}
