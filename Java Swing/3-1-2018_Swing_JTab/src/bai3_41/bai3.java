package bai3_41;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class bai3 extends JFrame implements Serializable{

	private JPanel contentPane;
	private JTextField txtProductName;
	private JTextField txtCategory;
	private JTextField txtPrice;
	private JTable table;
	private int row;
	private String flieName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai3 frame = new bai3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai3() {
		this.flieName = "testJTable.txt";
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 798, 633);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel pnInfo = new JPanel();
		pnInfo.setBounds(155, 11, 484, 124);
		contentPane.add(pnInfo);
		pnInfo.setLayout(null);
		
		JLabel lblProductName = new JLabel("Product Name");
		lblProductName.setBounds(10, 11, 106, 14);
		pnInfo.add(lblProductName);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setBounds(10, 53, 106, 14);
		pnInfo.add(lblCategory);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(10, 100, 106, 14);
		pnInfo.add(lblPrice);
		
		txtProductName = new JTextField();
		txtProductName.setBounds(175, 11, 233, 20);
		pnInfo.add(txtProductName);
		txtProductName.setColumns(10);
		
		txtCategory = new JTextField();
		txtCategory.setBounds(175, 53, 233, 20);
		pnInfo.add(txtCategory);
		txtCategory.setColumns(10);
		
		txtPrice = new JTextField();
		txtPrice.setBounds(175, 100, 233, 20);
		pnInfo.add(txtPrice);
		txtPrice.setColumns(10);
		
		JComboBox cbCate = new JComboBox();
		cbCate.setModel(new DefaultComboBoxModel(new String[] {"LapTop", "Smart Phone", "Televition", "Computer"}));
		cbCate.setBounds(418, 53, 56, 20);
		pnInfo.add(cbCate);
		
		JPanel pnButtons = new JPanel();
		pnButtons.setBounds(155, 174, 484, 33);
		contentPane.add(pnButtons);
		pnButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnAdd = new JButton("Add");
		pnButtons.add(btnAdd);
		
		JButton btnUpdate = new JButton("Update");
		pnButtons.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		pnButtons.add(btnDelete);
		
		JButton btnReadFile = new JButton("Read File");
		pnButtons.add(btnReadFile);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(155, 296, 484, 223);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		
		DefaultTableModel tableModel = new DefaultTableModel();
	
		Object[] data = new Object[3];
		
		Object[] columnName = {"Product Name", "Category", "Price"};
		tableModel.setColumnIdentifiers(columnName);
		table.setModel(tableModel);
		
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				row = table.getSelectedRow();
				txtProductName.setText((String) tableModel.getValueAt(row, 0));
				txtCategory.setText((String) tableModel.getValueAt(row, 1));
				txtPrice.setText((String) tableModel.getValueAt(row, 2));
			}
		});
		
		btnAdd.addActionListener(event -> {
			data[0] = txtProductName.getText();
			data[1] = cbCate.getSelectedItem();
			data[2] = txtPrice.getText();
			tableModel.addRow(data);
			this.writeFile(tableModel);
		});
		
		btnUpdate.addActionListener(event -> {
			tableModel.setValueAt(txtProductName.getText(), row, 0);
			tableModel.setValueAt(txtCategory.getText(), row, 1);
			tableModel.setValueAt(txtPrice.getText(), row, 2);
		});
		
		btnDelete.addActionListener(event -> {
			if (row >= 0){
				tableModel.removeRow(row);
			}
		});
		
		//doc tu file len
		btnReadFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bai3 b3 = new bai3();
				b3.readFile(tableModel);
			}
		});
	}
	
	public void writeFile(DefaultTableModel model){
		File file = new File(this.flieName);
		String lineData = "";
		FileWriter fr;
		try {
			fr = new FileWriter(file);
			BufferedWriter br = new BufferedWriter(fr);
			
			for(int i = 0; i < model.getRowCount(); i++){
				lineData = model.getValueAt(i, 0) + ""
						+ "\t" + model.getValueAt(i, 1)
						+ "\t" + model.getValueAt(i, 2) + "\r\n";
				br.write(lineData);
			}
			br.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readFile(DefaultTableModel model){
		String lineData = "";
		String[] data = null;
		Object[] dataTable = new Object[3];
		try {
			FileReader fr = new FileReader(this.flieName);
			BufferedReader br = new BufferedReader(fr);
			
			do {
				lineData = br.readLine();
				if (lineData != null){
					data = lineData.split("\t");
					for (int i = 0; i < data.length; i++){
						System.out.println(data[i]);
						dataTable[i] = data[i];
					}
					model.addRow(dataTable);
				}
			}while (lineData != null);
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
