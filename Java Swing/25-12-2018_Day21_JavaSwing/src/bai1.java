import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class bai1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtUserName;
	private JPasswordField txtPassword;
	private JTextField txtUserNameRegister;
	private JPasswordField txtPassRegister;
	private String nameRegister;
	private String passRegister;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai1 frame = new bai1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai1() {
		this.nameRegister = "";
		this.passRegister = "";
		
		setVisible(true);
		setTitle("FisrtApp");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100, 676);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dark Facebook", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(11, 11, 1068, 340);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtUserName = new JTextField();
		txtUserName.setBounds(86, 74, 267, 23);
		panel.add(txtUserName);
		txtUserName.setColumns(10);
		
		JButton btnLogin = new JButton("Log in");
		
		btnLogin.setBounds(363, 91, 89, 23);
		panel.add(btnLogin);
		
		JLabel lbUserName = new JLabel("User Name");
		lbUserName.setBounds(10, 74, 66, 23);
		panel.add(lbUserName);
		
		JLabel lblshowUserName = new JLabel("Your Name Will Display Here...");
		lblshowUserName.setBounds(86, 153, 267, 23);
		panel.add(lblshowUserName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 108, 66, 23);
		panel.add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(86, 108, 267, 23);
		panel.add(txtPassword);
		
		JLabel lblshowPassword = new JLabel("Your Pass Will Display Here...");
		lblshowPassword.setBounds(86, 191, 267, 23);
		panel.add(lblshowPassword);
		
		JRadioButton rdbtnHaveAcc = new JRadioButton("Have account");
		rdbtnHaveAcc.setBackground(Color.LIGHT_GRAY);
		rdbtnHaveAcc.setSelected(true);
		rdbtnHaveAcc.setBounds(96, 221, 109, 23);
		panel.add(rdbtnHaveAcc);
		
		JRadioButton rdbtnNoAcc = new JRadioButton("Have account yet");
		rdbtnNoAcc.setBackground(Color.LIGHT_GRAY);
		rdbtnNoAcc.setBounds(96, 253, 123, 23);
		panel.add(rdbtnNoAcc);
		
		ButtonGroup groupAccount = new ButtonGroup();
		groupAccount.add(rdbtnHaveAcc);
		groupAccount.add(rdbtnNoAcc);
		
		JLabel lblNewLabel = new JLabel("You have account already");
		lblNewLabel.setBounds(135, 22, 150, 14);
		panel.add(lblNewLabel);
		
		JLabel lblRegister = new JLabel("Register");
		lblRegister.setBounds(897, 22, 66, 14);
		panel.add(lblRegister);
		
		JLabel lblUserNameRegister = new JLabel("User Name");
		lblUserNameRegister.setBounds(715, 74, 66, 23);
		panel.add(lblUserNameRegister);
		
		JLabel lblPassRegister = new JLabel("Password");
		lblPassRegister.setBounds(715, 108, 66, 23);
		panel.add(lblPassRegister);
		
		txtUserNameRegister = new JTextField();
		txtUserNameRegister.setColumns(10);
		txtUserNameRegister.setBounds(791, 74, 267, 23);
		panel.add(txtUserNameRegister);
		
		txtPassRegister = new JPasswordField();
		txtPassRegister.setBounds(791, 108, 267, 23);
		panel.add(txtPassRegister);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(617, 91, 89, 23);
		panel.add(btnRegister);
		
		JCheckBox chckbxMale = new JCheckBox("Male");
		chckbxMale.setBackground(Color.LIGHT_GRAY);
		chckbxMale.setBounds(715, 183, 97, 23);
		panel.add(chckbxMale);
		
		JCheckBox chckbxFemale = new JCheckBox("Female");
		chckbxFemale.setBackground(Color.LIGHT_GRAY);
		chckbxFemale.setBounds(715, 221, 97, 23);
		panel.add(chckbxFemale);
		
		JLabel lblSex = new JLabel("Sex");
		lblSex.setBounds(715, 162, 66, 14);
		panel.add(lblSex);
		
		JComboBox comboBoxYear = new JComboBox();
		comboBoxYear.setModel(new DefaultComboBoxModel(new String[] {"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999"}));
		comboBoxYear.setBounds(844, 188, 56, 28);
		panel.add(comboBoxYear);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(843, 162, 46, 14);
		panel.add(lblYear);
		
		btnLogin.addActionListener(event -> {
			/*		public void actionPerformed(ActionEvent arg0) {
					}*/
			String outputName = txtUserName.getText().trim();
			String outputPass = new String(txtPassword.getPassword());
			/*JOptionPane.showMessageDialog(rootPane, "Hello " + ouputName);*/
			lblshowUserName.setText(outputName);
			lblshowPassword.setText(outputPass);
			if (outputName.equals(nameRegister) && outputPass.equals(passRegister)){
				JOptionPane.showMessageDialog(rootPane, "Loged in sucessfully!!!");
			}else {
				JOptionPane.showMessageDialog(rootPane, "Loged in failed!!!");
			}
		});
		
		rdbtnHaveAcc.addActionListener(event -> {
			txtUserNameRegister.setEditable(false);
			txtPassRegister.setEnabled(false);
			txtPassword.setEnabled(true);
			txtUserName.setEditable(true);
			btnRegister.setEnabled(false);
		});
		
		rdbtnNoAcc.addActionListener(event -> {
			txtPassword.setEnabled(false);
			txtUserName.setEditable(false);
			txtUserNameRegister.setEditable(true);
			txtPassRegister.setEnabled(true);
			btnRegister.setEnabled(true);
			
		});
		
		if (rdbtnHaveAcc.isSelected()){
			txtUserNameRegister.setEditable(false);
			txtPassRegister.setEnabled(false);
			txtPassword.setEnabled(true);
			txtUserName.setEditable(true);
			btnRegister.setEnabled(false);
		}
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(Color.BLACK);
		panel_1.setBounds(11, 362, 1068, 184);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblRule = new JLabel("Rules");
		lblRule.setBounds(10, 11, 34, 14);
		panel_1.add(lblRule);
		
		JTextArea txtareaRule = new JTextArea();
		txtareaRule.setBackground(Color.WHITE);
		txtareaRule.setForeground(Color.GRAY);
		txtareaRule.setColumns(3);
		txtareaRule.setRows(5);
		txtareaRule.setBounds(10, 36, 150, 71);
		txtareaRule.setText("1. AAA\n2. BBB\n3. CCC\n4. DDD");
		panel_1.add(txtareaRule);
		
		JCheckBox chckbxAgree = new JCheckBox("Agree");
		chckbxAgree.setBackground(SystemColor.control);
		chckbxAgree.setBounds(10, 125, 97, 23);
		panel_1.add(chckbxAgree);
		
		btnRegister.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			nameRegister = txtUserNameRegister.getText();
			passRegister = new String(txtPassRegister.getPassword());
			if (chckbxAgree.isSelected()){
				JOptionPane.showMessageDialog(rootPane, "Created Accout Successfully!!!");
				txtUserNameRegister.setText("");
				txtPassRegister.setText("");
			}
			else
				JOptionPane.showMessageDialog(rootPane, "Must agree our rule!!!");
			}
		});
	}
}
