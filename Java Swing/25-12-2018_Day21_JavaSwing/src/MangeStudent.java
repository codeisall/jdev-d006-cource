import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MangeStudent {
	private String fileName;
	private ArrayList<Student> listStudent;
	public MangeStudent() {
		this.fileName = "student.txt";
		this.listStudent = new ArrayList<>();
	}
	
	public boolean writeStudent(Student st){
		FileInputStream fis;
		try {
			fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			this.listStudent = (ArrayList<Student>) ois.readObject();
			ois.close();
			fis.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!this.checkExistStudent(st)){
			this.listStudent.add(st);
			File file = new File("student.txt");
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(file);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(this.listStudent);
				oos.close();
				fos.close();
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean checkExistStudent(Student st){
		for (Student student : listStudent){
			if (st.getNameStudent().equalsIgnoreCase(student.getNameStudent())) return true;
		}
		return false;
	}
	
	public String reaadStudent(Student st){
		return "";
	}
}
