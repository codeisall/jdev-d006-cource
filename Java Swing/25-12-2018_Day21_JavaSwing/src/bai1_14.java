import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;

public class bai1_14 extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtEmail;
	private String patternEmail;
	private int countCheckBox;
	private MangeStudent manage;
	private Student st;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai1_14 frame = new bai1_14();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai1_14() {
		manage = new MangeStudent();
		st = new Student();
		countCheckBox = 0;
		patternEmail = "^[a-zA-Z0-9]+@[a-zA-Z]+(\\.[a-zA-Z]{2,4}){1,2}$";
		Pattern pt = Pattern.compile(patternEmail);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 764, 454);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Register", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 369, 109);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel(" Register Form");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(120, 11, 160, 25);
		panel.add(lblNewLabel);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 50, 46, 14);
		panel.add(lblName);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 81, 46, 14);
		panel.add(lblEmail);
		
		txtName = new JTextField();
		txtName.setBounds(60, 47, 299, 20);
		panel.add(txtName);
		txtName.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(60, 78, 299, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Sex", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(594, 11, 123, 109);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JRadioButton rbtnMale = new JRadioButton("Male");
		rbtnMale.setBounds(6, 23, 109, 23);
		panel_1.add(rbtnMale);
		
		JRadioButton rbtnFemale = new JRadioButton("Female");
		rbtnFemale.setBounds(6, 68, 109, 23);
		panel_1.add(rbtnFemale);
		
		ButtonGroup groupSex = new ButtonGroup();
		groupSex.add(rbtnMale);
		groupSex.add(rbtnFemale);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Hobby", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 136, 707, 53);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JCheckBox chkbxGame = new JCheckBox("Game");
		chkbxGame.setBounds(6, 23, 97, 23);
		panel_2.add(chkbxGame);
		
		JCheckBox chkbxReading = new JCheckBox("Reading");
		chkbxReading.setBounds(317, 23, 97, 23);
		panel_2.add(chkbxReading);
		
		JCheckBox chkbxLearning = new JCheckBox("Learning English");
		chkbxLearning.setBounds(579, 23, 122, 23);
		panel_2.add(chkbxLearning);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "More Details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 200, 707, 112);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Level");
		lblNewLabel_3.setBounds(10, 21, 46, 14);
		panel_3.add(lblNewLabel_3);
		
		JComboBox comboBoxLevel = new JComboBox();
		comboBoxLevel.setModel(new DefaultComboBoxModel(new String[] {"[Choose your level]", "Secondary School", "Hight School", "Collage", "University"}));
		comboBoxLevel.setBounds(10, 46, 104, 20);
		panel_3.add(comboBoxLevel);
		
		JLabel lblNewLabel_4 = new JLabel("Your opion");
		lblNewLabel_4.setBounds(460, 21, 68, 14);
		panel_3.add(lblNewLabel_4);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(460, 44, 237, 57);
		panel_3.add(scrollPane);
		
		JTextArea txtAreaOpion = new JTextArea();
		scrollPane.setViewportView(txtAreaOpion);
		txtAreaOpion.setText("nlthanhititiu17038@gmail.com");
		txtAreaOpion.setColumns(2);
		txtAreaOpion.setLineWrap(true);
		txtAreaOpion.setWrapStyleWord(true);
		txtAreaOpion.setRows(20);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(10, 323, 707, 90);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblInfo = new JLabel("");
		lblInfo.setBounds(10, 22, 171, 14);
		panel_4.add(lblInfo);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String gameHobby = "";
				String readingHobby = "";
				String learningHobby = "";
				//Handle CheckBox
				if (chkbxGame.isSelected()) {
					gameHobby = chkbxGame.getText();
					countCheckBox++;
				}
				if (chkbxReading.isSelected()) {
					readingHobby = chkbxReading.getText();
					countCheckBox++;
				}
				if (chkbxLearning.isSelected()) {
					learningHobby = chkbxLearning.getText();
					countCheckBox++;
				}
				Matcher mc = pt.matcher(txtEmail.getText());
				
				//Handle Name and Mail
				if (!txtName.getText().isEmpty() && !txtEmail.getText().isEmpty()){
					//Handle Mail
					if(!mc.matches()){
						lblInfo.setText("Please, input all information");
						JOptionPane.showMessageDialog(getParent(), "Your mail is invalid!!!");
						txtEmail.requestFocus();
					}else {
						//Handle Sex
						if (!rbtnMale.isSelected() && !rbtnFemale.isSelected()) {
							lblInfo.setText("Please, input all information");
							JOptionPane.showMessageDialog(getParent(), "You must choose your sex!!!");
						}
						else {
							//Handle CheckBox
							if (countCheckBox == 0) {
								lblInfo.setText("Please, input all information");
								JOptionPane.showMessageDialog(getParent(), "You must choose your hobby!!!");
							}
							else {
								if (comboBoxLevel.getSelectedIndex() != 0) {
									String comboBoxContent = (String) comboBoxLevel.getItemAt(comboBoxLevel.getSelectedIndex());
									if (rbtnMale.isSelected()) {
										txtAreaOpion.setText(txtAreaOpion.getText()+ "\n" + "Your background : \n" +
															"Name : " + txtName.getText() + 
															"\nEmail : " +  txtEmail.getText() + 
															"\nSex : " + rbtnMale.getText() +
															"\nHobby : " + gameHobby + ", " + readingHobby + ", " + learningHobby + 
															"\n Level : " + comboBoxContent);
									}else {
										txtAreaOpion.setText(txtAreaOpion.getText()+ "\n" + "Your background : \n" +
												"Name : " + txtName.getText() + 
												"\nEmail : " +  txtEmail.getText() + 
												"\nSex : " + rbtnFemale.getText() +
												"\nHobby : " + gameHobby + ", " + readingHobby + ", " + learningHobby + 
												"\n Level : " + comboBoxContent);
									}
									st = new Student(txtName.getText(), txtEmail.getText());
									if (manage.writeStudent(st))
										lblInfo.setText("Register successfully");
									else {
										lblInfo.setText("Register failed");
									}
									countCheckBox = 0;
								}else {
									lblInfo.setText("Please, input all information");
									JOptionPane.showMessageDialog(getParent(), "You must choose your level!!!");
								}
							}
						}
					}
				//Handle Name and Mail
				}else {
					lblInfo.setText("Please, input all information");
					JOptionPane.showMessageDialog(getParent(), "Name and Email must be filled!!!");
					if (txtName.getText().isEmpty()) txtName.requestFocus();
					if (txtEmail.getText().isEmpty()) txtEmail.requestFocus();
				}
			}
		});
		btnRegister.setBounds(174, 56, 89, 23);
		panel_4.add(btnRegister);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(event -> {
			txtName.setText("");
			txtEmail.setText("");
			
			groupSex.clearSelection();
			
			chkbxGame.setSelected(false);
			chkbxReading.setSelected(false);
			chkbxLearning.setSelected(false);
			
			comboBoxLevel.setSelectedIndex(0);
			
			txtAreaOpion.setText("nlthanhititiu17038@gmail.com");
			
			lblInfo.setText("");
			
		});
		btnReset.setBounds(437, 56, 89, 23);
		panel_4.add(btnReset);
	}
	
	public boolean checkRadioButtonSex(JRadioButton rbtn1, JRadioButton rbtn2){
		if (rbtn1.isSelected() || rbtn2.isSelected()) return true;
		return false;
	}

}
