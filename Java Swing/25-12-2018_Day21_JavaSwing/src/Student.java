import java.io.Serializable;

public class Student implements Serializable{
	private String nameStudent;
	private String email;
	
	public Student() {
		nameStudent = "";
		email = "";
	}
	
	public Student(String name, String mail) {
		nameStudent = name;
		email = mail;
	}

	public String getNameStudent() {
		return nameStudent;
	}

	public void setNameStudent(String nameStudent) {
		this.nameStudent = nameStudent;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student [nameStudent=" + nameStudent + ", email=" + email + "]+\n";
	}
}
