package bai1_47;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckInput {
	private String emptyInput;
	private String phoneInput;
	
	public CheckInput() {
		this.emptyInput = "[^\\s]";
		this.phoneInput = "^[0-9]{7,12}$";
	}
	
	public boolean checkEmpty(String str) {
		Pattern ptEmpty = Pattern.compile(this.emptyInput);
		Matcher mcEmpty = ptEmpty.matcher(str);
		if (mcEmpty.find()) return true;
		return false;
	}
	
	public boolean checkPhone(String str) {
		Pattern ptPhone = Pattern.compile(this.phoneInput);
		Matcher mcPhone = ptPhone.matcher(str);
		if (mcPhone.matches()) return true;
		return false;
	}
}
