package bai1_47;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Myconnect.MyConnect;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.FlowLayout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class bai1_fixed extends JFrame {

	private JPanel contentPane;
	private JTextField txtRegID;
	private JTextField txtName;
	private JTextField txtAddress;
	private JTextField txtParentName;
	private JTextField txtPhone;
	private JLabel lblNotify;
	private Calendar cal;
	private SimpleDateFormat format;
	private Connection cn;
	private TakeData take;
	private CheckInput check;
	private JTable table;
	private int indexClickedRow;
	private boolean indexClicked;
	private DefaultTableModel tableModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai1_fixed frame = new bai1_fixed();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai1_fixed() {
		this.cal = Calendar.getInstance();
		this.format = new SimpleDateFormat("YYY-MM-DD");
		this.cn = new MyConnect().getcn();
		this.take = new TakeData();
		this.indexClicked = false;
		this.check = new CheckInput();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 893, 645);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel pnFormRegister = new JPanel();
		pnFormRegister.setBounds(105, 11, 646, 263);
		contentPane.add(pnFormRegister);
		pnFormRegister.setLayout(null);
		
		JLabel lblRegID = new JLabel("RegID");
		lblRegID.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegID.setBounds(108, 17, 88, 14);
		pnFormRegister.add(lblRegID);
		
		JLabel lblName = new JLabel("Name");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setBounds(108, 59, 88, 14);
		pnFormRegister.add(lblName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddress.setBounds(108, 98, 88, 14);
		pnFormRegister.add(lblAddress);
		
		JLabel lblParentName = new JLabel("Parent Name");
		lblParentName.setHorizontalAlignment(SwingConstants.CENTER);
		lblParentName.setBounds(108, 143, 88, 14);
		pnFormRegister.add(lblParentName);
		
		txtRegID = new JTextField();
		txtRegID.setBounds(206, 11, 314, 20);
		pnFormRegister.add(txtRegID);
		txtRegID.setColumns(10);
		
		txtName = new JTextField();
		txtName.setBounds(206, 53, 314, 20);
		pnFormRegister.add(txtName);
		txtName.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBounds(206, 95, 314, 20);
		pnFormRegister.add(txtAddress);
		txtAddress.setColumns(10);
		
		txtParentName = new JTextField();
		txtParentName.setBounds(206, 137, 314, 20);
		pnFormRegister.add(txtParentName);
		txtParentName.setColumns(10);
		
		JLabel lblPhone = new JLabel("Contact No.");
		lblPhone.setHorizontalAlignment(SwingConstants.CENTER);
		lblPhone.setBounds(108, 185, 88, 14);
		pnFormRegister.add(lblPhone);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(206, 179, 314, 20);
		pnFormRegister.add(txtPhone);
		txtPhone.setColumns(10);
		
		JLabel lblStandard = new JLabel("Standard");
		lblStandard.setHorizontalAlignment(SwingConstants.CENTER);
		lblStandard.setBounds(108, 228, 88, 14);
		pnFormRegister.add(lblStandard);
		
		JComboBox cbStandard = new JComboBox();
		cbStandard.setBounds(206, 224, 100, 22);
		pnFormRegister.add(cbStandard);
		
		JComboBox cbFee = new JComboBox();
		cbFee.setBounds(420, 224, 100, 22);
		pnFormRegister.add(cbFee);
		
		JLabel lblFee = new JLabel("Fee");
		lblFee.setBounds(364, 228, 46, 14);
		pnFormRegister.add(lblFee);
		
		JPanel pnButtons = new JPanel();
		pnButtons.setBounds(105, 285, 646, 39);
		contentPane.add(pnButtons);
		pnButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnSave = new JButton("Save");
		pnButtons.add(btnSave);
		
		JButton btnUpdate = new JButton("Update");
		pnButtons.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		pnButtons.add(btnDelete);
		
		JButton btnReset = new JButton("Reset");
		pnButtons.add(btnReset);
		
		lblNotify = new JLabel("");
		lblNotify.setForeground(Color.RED);
		lblNotify.setHorizontalAlignment(SwingConstants.CENTER);
		lblNotify.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblNotify.setBounds(105, 562, 646, 33);
		contentPane.add(lblNotify);
		
		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(10, 335, 857, 216);
		contentPane.add(scrollPaneTable);
		
		table = new JTable();
		scrollPaneTable.setViewportView(table);
		
		//Check connection
		if (this.checkConnectionSate()) {
			lblNotify.setText("Connected to Database successfully!!!");
		}else System.exit(DISPOSE_ON_CLOSE);
				
		//Set Column Names
		Object[] colName = {"RegID", "Name", "Address", "Parent Name", "Phone", "Standard", "RegDate"};
		tableModel = new DefaultTableModel();
		tableModel.setColumnIdentifiers(colName);
		table.setModel(tableModel);
				
		//Take date for combobox Standard
		ArrayList<String> takeStand = new ArrayList<>();
		ArrayList<String> takeFee = new ArrayList<>();
		take.takeStandardFee(cn, takeStand, takeFee);
		String[] standard = new String[takeStand.size()];
		String[] fee = new String[takeStand.size()];
		for (int i = 0; i < takeStand.size(); i++) {
			standard[i] = takeStand.get(i);
			fee[i] = takeFee.get(i);
		}
		DefaultComboBoxModel standModel = new DefaultComboBoxModel<>(standard);
		DefaultComboBoxModel feeModel = new DefaultComboBoxModel<>(fee);
		cbStandard.setModel(standModel);
		cbFee.setModel(feeModel);	
		
		//Take student data
		ArrayList<String> takeStudent = new ArrayList<>();
		take.takeStudent(cn, takeStudent);
		Object[] student = new Object[table.getColumnCount()];
		int count = 0;
		for (int i = 0; i < takeStudent.size(); i++) {
			student[count] = takeStudent.get(i);
			count++;
			if (count == 7) {
				tableModel.addRow(student);
				count = 0;
			}
		}
		
		//Clicking on table
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				indexClickedRow = table.getSelectedRow(); 
				indexClicked = true;
				
				txtRegID.setText((String) tableModel.getValueAt(indexClickedRow, 0));
				txtName.setText((String) tableModel.getValueAt(indexClickedRow, 1));
				txtAddress.setText((String) tableModel.getValueAt(indexClickedRow, 2));
				txtParentName.setText((String) tableModel.getValueAt(indexClickedRow, 3));
				txtPhone.setText((String) tableModel.getValueAt(indexClickedRow, 4));
				cbStandard.setSelectedItem((String) tableModel.getValueAt(indexClickedRow, 5));
				cbFee.setSelectedItem(take.takeSpecificFee(cn, (String) tableModel.getValueAt(indexClickedRow, 5)));
			}
		});
		
		//Delete
		btnDelete.addActionListener(event -> {
			if (this.indexClicked && this.indexClickedRow >= 0) {
				String sql = "delete from Student where RegID like ?";
				try {
					PreparedStatement pt = cn.prepareStatement(sql);
					pt.setString(1, (String) tableModel.getValueAt(indexClickedRow, 0));
					pt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				tableModel.removeRow(indexClickedRow);
				this.indexClicked = false;
			}
		});
		
		//Update
		btnUpdate.addActionListener(event -> {
			String date = format.format(cal.getTime());
			if (this.checkAll()) {
				/*tableModel.setValueAt(txtRegID.getText(), indexClickedRow, 0);*/
				tableModel.setValueAt(txtName.getText(), indexClickedRow, 1);
				tableModel.setValueAt(txtAddress.getText(), indexClickedRow, 2);
				tableModel.setValueAt(txtParentName.getText(), indexClickedRow, 3);
				tableModel.setValueAt(txtPhone.getText(), indexClickedRow, 4);
				tableModel.setValueAt(cbStandard.getSelectedItem(), indexClickedRow, 5);
				tableModel.setValueAt(date, indexClickedRow, 6);
				
				String sql = "update Student set "
						+ "Name = ?, Address = ?, ParentName = ?, Phone = ?, "
						+ "Standard = ?, RrgDate = ? "
						+ "where RegID like ?";
				try {
					PreparedStatement pt = cn.prepareStatement(sql);
					pt.setString(1, txtName.getText());
					pt.setString(2, txtAddress.getText());
					pt.setString(3, txtParentName.getText());
					pt.setString(4, txtPhone.getText());
					pt.setString(5, (String) cbStandard.getSelectedItem());
					pt.setString(6, date);
					pt.setString(7, txtRegID.getText());
					pt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				indexClicked = false;
				lblNotify.setText("");
			}
		});
		
		//Save
		btnSave.addActionListener(event -> {
			String date = format.format(cal.getTime());
			if (this.checkAll()) {
				if (!this.checkExistRegID()) {
					String sql = "insert into Student "
							+ "values(?, ?, ?, ?, ?, ?, ?)";
					try {
						PreparedStatement pt = cn.prepareStatement(sql);
						pt.setString(1, txtRegID.getText());
						pt.setString(2, txtName.getText());
						pt.setString(3, txtAddress.getText());
						pt.setString(4, txtParentName.getText());
						pt.setString(5, txtPhone.getText());
						pt.setString(6, (String) cbStandard.getSelectedItem());
						pt.setString(7, date);
						pt.executeUpdate();
						lblNotify.setText("Student inserted successfully!!!");
						this.removeAndUpate();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else lblNotify.setText("This ID does exist!!!");
			}
		});
		
		//Reset
		btnReset.addActionListener(event -> {
			txtRegID.setText("");
			txtName.setText("");
			txtParentName.setText("");
			txtPhone.setText("");
			txtAddress.setText("");
			cbStandard.setSelectedIndex(-1);
			cbFee.setSelectedIndex(-1);
			lblNotify.setText("");
		});
	}
	
	public boolean checkConnectionSate() {
		if (this.cn != null) return true;
		return false;
	}
	
	public Connection getConnection() {
		return this.cn;
	}
	
	public boolean checkAll() {
		if (check.checkEmpty(txtRegID.getText())) {
			if (check.checkEmpty(txtName.getText())) {
				if (check.checkEmpty(txtAddress.getText())) {
					if (check.checkEmpty(txtParentName.getText())) {
						if (check.checkEmpty(txtPhone.getText()) &&check.checkPhone(txtPhone.getText())) {
							return true;
						}else lblNotify.setText("Phone is invalid!!!");
					}else lblNotify.setText("Parent name have been filled yet!!!");
				}else lblNotify.setText("Address have been filled yet!!!");
			}else lblNotify.setText("Name have been filled yet!!!");
		}else lblNotify.setText("RegID have been filled yet!!!");
		return false;
	}
	
	public boolean checkExistRegID() {
		String sql = "select * from Student where "
				+ "RegID like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, txtRegID.getText());
			ResultSet rs = pt.executeQuery();
			if (rs.next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void removeAndUpate() {
		//Remove all must remove from the bottom;
		for (int i = tableModel.getRowCount()-1; i >= 0; i--)
			tableModel.removeRow(i);
		
		ArrayList<String> takeStudent = new ArrayList<>();
		take.takeStudent(cn, takeStudent);
		Object[] student = new Object[table.getColumnCount()];
		int count = 0;
		for (int i = 0; i < takeStudent.size(); i++) {
			student[count] = takeStudent.get(i);
			count++;
			if (count == 7) {
				tableModel.addRow(student);
				count = 0;
			}
		}
	}
}
