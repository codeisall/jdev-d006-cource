package main;

import java.util.Scanner;

import Handle.Book;
import Handle.Category;
import Handle.Database;

public class Test_Fixed {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Database database = new Database();
		Book book = new Book();
		Category category = new Category();
		String userName, password;
		int idCate, idCateUpdating, idBook, idBookUpdating;
		String nameCate, titleBook;
		float priceBook;
		int choice = 0;
		
		if (database.checkConnectionState()) {
			System.out.println("Input your username : ");
			/*sc = new Scanner(System.in);*/
			userName = "aaa";
			System.out.println("Input your password : ");
			/*sc = new Scanner(System.in);*/
			password = "111";
			if (database.checkLogInState(userName, password)) {
				System.out.println("\nLoged in successfully!!!\n");
				
				do {
					System.out.println("**************************");
					System.out.println("*   BOOKSHOP MANAGEMENT  *");
					System.out.println("* 1. Mange Book Category *");
					System.out.println("* 2. Mange Book          *");
					System.out.println("* 3. Close Program       *");
					System.out.println("**************************");
					do {
						try {
							System.out.print("\nPlease, enter your choice : ");
							sc = new Scanner(System.in);
							choice = sc.nextInt();
							break;
						} catch (Exception e) {
							System.out.println("\nYou must enter a number!!!\n");
						}
					}while (true);
					
					switch (choice) {
					case 1:
						do {
							System.out.println("******************************************************************");
							System.out.println("*                         CATEGORY MANAGEMENT                    *");
							System.out.println("* 1. Show all book category                                      *");
							System.out.println("* 2. Showing books and the number of book of a specific category *");
							System.out.println("* 3. Add a new category                                          *");
							System.out.println("* 4. Delete a category                                           *");
							System.out.println("* 5. Edit a category                                             *"); 
							System.out.println("* 6. Exit                                                        *");
							System.out.println("******************************************************************");
							do {
								try {
									System.out.print("\nPlease, enter your choice : ");
									sc = new Scanner(System.in);
									choice = sc.nextInt();
									break;
								} catch (Exception e) {
									System.out.println("\nYou must enter a number!!!\n");
								}
							}while (true);
							
							switch (choice) {
							case 1 :
								category.showAllCategories();
								break;
							case 2 :
								System.out.print("Input id category for showing : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								category.showBooksOfACategory(idCate);
								break;
							case 3 :
								System.out.print("Input id category : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								System.out.print("Input name category : ");
								sc = new Scanner(System.in);
								nameCate = sc.nextLine();
								category.insertACategory(idCate, nameCate);
								break;
							case 4 :
								System.out.print("Input id category for deleting : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								category.deleteACategory(idCate);
								break;
							case 5 :
								System.out.print("Input id category for updating : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								System.out.println("");
								System.out.print("Input a new id category for updating : ");
								sc = new Scanner(System.in);
								idCateUpdating = sc.nextInt();
								System.out.print("Input name category : ");
								sc = new Scanner(System.in);
								nameCate = sc.nextLine();
								category.updateCategory(idCate, idCateUpdating, nameCate);
								break;
							case 6 :
								System.out.println("\nCame back main page!!!\n");
								break;
							default :
								System.out.println("\nYour choice is invalid!!!\n");
								break;
							}
						}while (choice != 6);
						break;
					case 2 :
						do {
							System.out.println("**********************************");
							System.out.println("*         BOOK MANAGEMENT        *");
							System.out.println("* 1. Showing all books available *");
							System.out.println("* 2. Add a new book              *");
							System.out.println("* 3. Delete a book               *");
							System.out.println("* 4. Edit a book                 *");
							System.out.println("* 5. Exit                        *");
							System.out.println("**********************************");
							do {
								try {
									System.out.print("\nPlease, enter your choice : ");
									sc = new Scanner(System.in);
									choice = sc.nextInt();
									break;
								} catch (Exception e) {
									System.out.println("\nYou must enter a number!!!\n");
								}
							}while (true);
							
							switch (choice) {
							case 1 :
								book.showAllBooks();
								break;
							case 2 :
								System.out.print("Input id book : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								System.out.print("Input titile book : ");
								sc = new Scanner(System.in);
								titleBook = sc.nextLine();
								System.out.print("Input book cost : ");
								sc = new Scanner(System.in);
								priceBook = sc.nextFloat();
								System.out.print("Input id category : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								book.insertBook(idBook, titleBook, priceBook, idCate);
								break;
							case 3 :
								System.out.print("Input id book for deleting : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								book.deleteBook(idBook);
								break;
							case 4 :
								System.out.print("Input id book for updating : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								System.out.print("Input a new id book : ");
								sc = new Scanner(System.in);
								idBookUpdating = sc.nextInt();
								System.out.print("Input a new titile book : ");
								sc = new Scanner(System.in);
								titleBook = sc.nextLine();
								System.out.print("Input a new book cost : ");
								sc = new Scanner(System.in);
								priceBook = sc.nextFloat();
								System.out.print("Input a new id category : ");
								sc = new Scanner(System.in);
								idCate = sc.nextInt();
								book.updateBook(idBook, idBookUpdating, titleBook, priceBook, idCate);
								break;
							case 5 :
								System.out.println("\nCame back main page!!!\n");
								break;
							default :
								System.out.println("\nYour choice is invalid!!!\n");
								break;
							}
						}while (choice != 5);
						break;
					case 3 :
							System.out.println("\nGoodBye!!!\n");	
					default :
						System.out.println("\nYour choice is invalid!!!\n");
						break;
					}
				}while (choice != 3);
			}else {
				System.out.println("\nLog in failed!!!\n");
			}
		}else System.out.println("\nCannot connect to database\n");
	}
}
