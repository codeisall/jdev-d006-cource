package swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import Handle.Book;
import Handle.Category;
import Handle.Database;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;

import java.awt.FlowLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.ButtonGroup;
import javax.swing.border.EtchedBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.ScrollPaneConstants;

public class Library extends JFrame {

	private JPanel contentPane;
	private JTextField txtCateID;
	private JTextField txtCateName;
	private JTextField txtBookID;
	private JTextField txtBookTitle;
	private JTextField txtBookPrice;
	private JComboBox<Integer> comboBoxCateID;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroupFeatures = new ButtonGroup();
	private int idCateUpdate;
	private int idBookUpdate;
	private Database database;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Library frame = new Library();
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Library() {
		Category cate = new Category();
		Book book = new Book();
		database = new Database();
		
		setTitle("Library");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1166, 507);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Features", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 33, 448, 58);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnInsertCate = new JButton("Insert");
		btnInsertCate.setBounds(10, 24, 103, 23);
		panel.add(btnInsertCate);
		
		JButton btnUpdateCate = new JButton("Update");
		btnUpdateCate.setBounds(117, 24, 102, 23);
		panel.add(btnUpdateCate);
		
		JButton btnDeleteCate = new JButton("Delete");
		btnDeleteCate.setBounds(223, 24, 103, 23);
		panel.add(btnDeleteCate);
		
		JButton btnSearchCate = new JButton("Search");
		btnSearchCate.setBounds(330, 24, 102, 23);
		panel.add(btnSearchCate);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Features", TitledBorder.LEFT, TitledBorder.TOP, null, null));
		panel_1.setBounds(692, 33, 448, 58);
		contentPane.add(panel_1);
		panel_1.setLayout(new MigLayout("", "[112px][112px][112px][112px]", "[45px]"));
		
		JButton btnInsertBook = new JButton("Insert");
		panel_1.add(btnInsertBook, "cell 0 0,grow");
		
		JButton btnUpdateBook = new JButton("Update");
		panel_1.add(btnUpdateBook, "cell 1 0,grow");
		
		JButton btnDeleteBook = new JButton("Delete");
		panel_1.add(btnDeleteBook, "cell 2 0,growx");
		
		JButton btnSearchBook = new JButton("Search");
		panel_1.add(btnSearchBook, "cell 3 0,grow");
		
		JLabel lblCategory = new JLabel("CATEGRORY");
		lblCategory.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCategory.setBounds(193, 8, 103, 14);
		contentPane.add(lblCategory);
		
		JLabel lblBook = new JLabel("BOOK");
		lblBook.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblBook.setBounds(922, 8, 46, 14);
		contentPane.add(lblBook);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.BLACK, 2, true));
		panel_2.setBounds(10, 102, 448, 160);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblCateID = new JLabel("Category ID");
		lblCateID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCateID.setBounds(10, 11, 70, 14);
		panel_2.add(lblCateID);
		
		JLabel lblCateName = new JLabel("Category Name");
		lblCateName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCateName.setBounds(10, 69, 101, 14);
		panel_2.add(lblCateName);
		
		txtCateID = new JTextField();
		txtCateID.setBounds(107, 8, 212, 20);
		panel_2.add(txtCateID);
		txtCateID.setColumns(10);
		
		txtCateName = new JTextField();
		txtCateName.setBounds(107, 66, 211, 20);
		panel_2.add(txtCateName);
		txtCateName.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(Color.BLACK, 2, true));
		panel_3.setBounds(692, 102, 448, 160);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblBookID = new JLabel("Book ID");
		lblBookID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBookID.setBounds(10, 11, 46, 14);
		panel_3.add(lblBookID);
		
		JLabel lblBookTitle = new JLabel("Title");
		lblBookTitle.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBookTitle.setBounds(10, 39, 46, 14);
		panel_3.add(lblBookTitle);
		
		JLabel lblBookPrice = new JLabel("Price");
		lblBookPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBookPrice.setBounds(10, 67, 46, 14);
		panel_3.add(lblBookPrice);
		
		JLabel lblCateIDForBook = new JLabel("Category ID");
		lblCateIDForBook.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCateIDForBook.setBounds(10, 92, 82, 14);
		panel_3.add(lblCateIDForBook);
		
		txtBookID = new JTextField();
		txtBookID.setColumns(10);
		txtBookID.setBounds(113, 8, 212, 20);
		panel_3.add(txtBookID);
		
		txtBookTitle = new JTextField();
		txtBookTitle.setColumns(10);
		txtBookTitle.setBounds(113, 36, 212, 20);
		panel_3.add(txtBookTitle);
		
		txtBookPrice = new JTextField();
		txtBookPrice.setColumns(10);
		txtBookPrice.setBounds(113, 64, 212, 20);
		panel_3.add(txtBookPrice);
		
		comboBoxCateID = new JComboBox<>();
		comboBoxCateID.setBounds(113, 88, 82, 22);
		panel_3.add(comboBoxCateID);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 296, 448, 160);
		contentPane.add(scrollPane);
		
		JTextArea txtAreaCate = new JTextArea();
		txtAreaCate.setFont(new Font("Monospaced", Font.BOLD, 14));
		txtAreaCate.setRows(20);
		scrollPane.setViewportView(txtAreaCate);
		
		JLabel lblCateDetail = new JLabel("");
		lblCateDetail.setBounds(10, 273, 448, 14);
		contentPane.add(lblCateDetail);
		
		JLabel lblBookDetail = new JLabel("");
		lblBookDetail.setBounds(692, 273, 448, 14);
		contentPane.add(lblBookDetail);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setBounds(692, 297, 448, 160);
		contentPane.add(scrollPane_1);
		
		JTextArea txtAreaBook = new JTextArea();
		scrollPane_1.setViewportView(txtAreaBook);
		txtAreaBook.setRows(20);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Mange", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(468, 33, 214, 58);
		contentPane.add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JRadioButton rbtnCate = new JRadioButton("Category");
		buttonGroup.add(rbtnCate);
		panel_4.add(rbtnCate);
		
		JRadioButton rbtnBook = new JRadioButton("Book");
		buttonGroup.add(rbtnBook);
		panel_4.add(rbtnBook);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Print All", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_5.setBounds(468, 102, 214, 58);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		JButton btnPrintAllCate = new JButton("Category");
		btnPrintAllCate.setBounds(10, 23, 89, 23);
		panel_5.add(btnPrintAllCate);
		
		JButton btnPrintAllBook = new JButton("Book");
		btnPrintAllBook.setBounds(115, 23, 89, 23);
		panel_5.add(btnPrintAllBook);
		
		//Initilize field 
		btnInsertCate.setEnabled(false);
		btnUpdateCate.setEnabled(false);
		btnDeleteCate.setEnabled(false);
		btnSearchCate.setEnabled(false);
		
		txtCateID.setEditable(false);
		txtCateName.setEditable(false);
		
		JLabel lblCateUp_De = new JLabel("");
		lblCateUp_De.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCateUp_De.setBounds(10, 118, 309, 31);
		panel_2.add(lblCateUp_De);
		
		btnInsertBook.setEnabled(false);
		btnUpdateBook.setEnabled(false);
		btnDeleteBook.setEnabled(false);
		btnSearchBook.setEnabled(false);
		
		txtBookID.setEditable(false);
		txtBookTitle.setEditable(false);
		txtBookPrice.setEditable(false);
		comboBoxCateID.setEnabled(false);
		
		JLabel lblBookForUp_De = new JLabel("");
		lblBookForUp_De.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBookForUp_De.setBounds(10, 117, 309, 31);
		panel_3.add(lblBookForUp_De);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(null, "Features", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_6.setBounds(468, 171, 214, 139);
		contentPane.add(panel_6);
		panel_6.setLayout(null);
		
		JRadioButton rbtnInsert = new JRadioButton("Insert");
		buttonGroupFeatures.add(rbtnInsert);
		rbtnInsert.setBounds(74, 17, 66, 23);
		panel_6.add(rbtnInsert);
		
		JRadioButton rbtnSearch = new JRadioButton("Search");
		buttonGroupFeatures.add(rbtnSearch);
		rbtnSearch.setBounds(74, 43, 66, 23);
		panel_6.add(rbtnSearch);
		
		JLabel label = new JLabel("");
		label.setBounds(90, 60, 0, 0);
		panel_6.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(95, 60, 0, 0);
		panel_6.add(label_1);
		
		JRadioButton rbtnUpdate = new JRadioButton("Update");
		buttonGroupFeatures.add(rbtnUpdate);
		rbtnUpdate.setBounds(74, 69, 66, 23);
		panel_6.add(rbtnUpdate);
		
		JRadioButton rbtnDelete = new JRadioButton("Delete");
		buttonGroupFeatures.add(rbtnDelete);
		rbtnDelete.setBounds(74, 95, 66, 23);
		panel_6.add(rbtnDelete);
		
		JPanel panel_Login = new JPanel();
		panel_Login.setBounds(10, 8, 1130, 448);
		contentPane.add(panel_Login);
		panel_Login.setLayout(null);
		//end initialize
		
		rbtnCate.addActionListener(event -> {
			btnInsertBook.setEnabled(false);
			btnUpdateBook.setEnabled(false);
			btnDeleteBook.setEnabled(false);
			btnSearchBook.setEnabled(false);
			
			txtBookID.setEditable(false);
			txtBookTitle.setEditable(false);
			txtBookPrice.setEditable(false);
			comboBoxCateID.setEnabled(false);
			
/*			btnInsertCate.setEnabled(true);
			btnUpdateCate.setEnabled(true);
			btnDeleteCate.setEnabled(true);
			btnSearchCate.setEnabled(true);
			
			txtCateID.setEditable(true);
			txtCateName.setEditable(true);*/
		});
		
		rbtnBook.addActionListener(event -> {
			this.takeIDCate();
			btnInsertCate.setEnabled(false);
			btnUpdateCate.setEnabled(false);
			btnDeleteCate.setEnabled(false);
			btnSearchCate.setEnabled(false);
			
			txtCateID.setEditable(false);
			txtCateName.setEditable(false);
			
/*			btnInsertBook.setEnabled(true);
			btnUpdateBook.setEnabled(true);
			btnDeleteBook.setEnabled(true);
			btnSearchBook.setEnabled(true);
			
			txtBookID.setEditable(true);
			txtBookTitle.setEditable(true);
			txtBookPrice.setEditable(true);
			comboBoxCateID.setEnabled(true);*/
		});
		
		rbtnUpdate.addActionListener(eventUpdateCate -> {
			if (rbtnCate.isSelected()) {
				String option = JOptionPane.showInputDialog(getParent(), "Input Category ID is neccessary to update");
				if (option != null) {
					if (!option.equals("")) {
						this.idCateUpdate = Integer.parseInt(option);
						if (cate.checkExistCategory(idCateUpdate)) {
							btnInsertCate.setEnabled(false);
							btnDeleteCate.setEnabled(false);
							btnSearchCate.setEnabled(false);
							btnUpdateCate.setEnabled(true);
							txtCateID.setEditable(true);
							txtCateName.setEditable(true);
							lblCateUp_De.setText("Please, fill all information to update!!!");
						}else {
							txtCateName.setEditable(false);
							txtCateID.setEditable(false);
							lblCateUp_De.setText("This Category ID does not exist!!!");
						}
					}
				}
			}else {
				String option = JOptionPane.showInputDialog(getParent(), "Input Book ID is neccessary to update");
				if (option != null) {
					if (!option.equals("")) {
						this.idBookUpdate = Integer.parseInt(option);
						if (book.checkExistBook(idBookUpdate)) {
							btnInsertBook.setEnabled(false);
							btnDeleteBook.setEnabled(false);
							btnSearchBook.setEnabled(false);
							btnUpdateBook.setEnabled(true);
							txtBookID.setEditable(true);
							txtBookTitle.setEditable(true);
							txtBookPrice.setEditable(true);
							comboBoxCateID.setEnabled(true);
							lblBookForUp_De.setText("Please, fill all information to update!!!");
						}else {
							txtBookID.setEditable(false);
							txtBookTitle.setEditable(false);
							txtBookPrice.setEditable(false);
							comboBoxCateID.setEnabled(false);
							lblBookForUp_De.setText("This Book ID does not exist!!!");
						}
					}
				}
			}
		});
		
		rbtnInsert.addActionListener(eventInsertCate -> {
			if (rbtnCate.isSelected()) {
				txtCateID.setEditable(true);
				txtCateName.setEditable(true);
				btnInsertCate.setEnabled(true);
				btnDeleteCate.setEnabled(false);
				btnSearchCate.setEnabled(false);
				btnUpdateCate.setEnabled(false);
			}else {
				txtBookID.setEditable(true);
				txtBookTitle.setEditable(true);
				txtBookPrice.setEditable(true);
				comboBoxCateID.setEnabled(true);
				btnInsertBook.setEnabled(true);
				btnDeleteBook.setEnabled(false);
				btnSearchBook.setEnabled(false);
				btnUpdateBook.setEnabled(false);
			}
		});
		
		rbtnDelete.addActionListener(eventDeleteCate -> {
			if (rbtnCate.isSelected()) {
				txtCateName.setEditable(false);
				txtCateID.setEditable(true);
				btnInsertCate.setEnabled(false);
				btnDeleteCate.setEnabled(true);
				btnSearchCate.setEnabled(false);
				btnUpdateCate.setEnabled(false);
			}else {
				txtBookID.setEditable(true);
				txtBookPrice.setEditable(false);
				txtBookTitle.setEditable(false);
				comboBoxCateID.setEnabled(false);
				btnInsertBook.setEnabled(false);
				btnDeleteBook.setEnabled(true);
				btnSearchBook.setEnabled(false);
				btnUpdateBook.setEnabled(false);
			}
		});

		rbtnSearch.addActionListener(eventSearchCate -> {
			if (rbtnCate.isSelected()) {
				txtCateName.setEditable(false);
				txtCateID.setEditable(true);
				btnInsertCate.setEnabled(false);
				btnDeleteCate.setEnabled(false);
				btnSearchCate.setEnabled(true);
				btnUpdateCate.setEnabled(false);
			} else {
				txtBookID.setEditable(true);
				txtBookPrice.setEditable(false);
				txtBookTitle.setEditable(false);
				comboBoxCateID.setEnabled(false);
				btnInsertBook.setEnabled(false);
				btnDeleteBook.setEnabled(false);
				btnSearchBook.setEnabled(true);
				btnUpdateBook.setEnabled(false);
			}
		});
		
		//Insert Cate
		btnInsertCate.addActionListener(event -> {
			System.out.println(Integer.parseInt(txtCateID.getText()));
			if (!cate.checkExistCategory(Integer.parseInt(txtCateID.getText()))) {
				String sqlInsertCate = "insert into Category values(?, ?)";
				try {
					PreparedStatement psInsertCate = database.getConnection().prepareStatement(sqlInsertCate);
					psInsertCate.setInt(1, Integer.parseInt(txtCateID.getText()));
					psInsertCate.setString(2, txtCateName.getText());
					psInsertCate.executeUpdate();
					txtAreaCate.setText("Category inserted successfully1!!!");
					//Take id cate
					String sqlIDcate = "select IDcate from Category";
					try {
						PreparedStatement ps = database.getConnection().prepareStatement(sqlIDcate);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
							comboBoxCateID.addItem(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}else {
				txtAreaCate.setText("This category ID does exist!!!");
			}
		});
		//Update cate
		btnUpdateCate.addActionListener(eventUpdateBook -> {
			String sqlCateUpdate = "update Category "
					+ "set IDcate = ?, Name = ? "
					+ "where IDcate = ?";
			try {
				PreparedStatement psCateUpdate = database.getConnection().prepareStatement(sqlCateUpdate);
				psCateUpdate.setInt(1, Integer.parseInt(txtCateID.getText()));
				psCateUpdate.setString(2, txtCateName.getText());
				psCateUpdate.setInt(3, idCateUpdate);
				if (Integer.parseInt(txtCateID.getText()) == idCateUpdate) {
					psCateUpdate.executeUpdate();
					lblCateUp_De.setText("Category updated successfully!!!");
				}else {
					if (!cate.checkExistCategory(Integer.parseInt(txtCateID.getText()))) {
						psCateUpdate.executeUpdate();
						lblCateUp_De.setText("Category updated successfully!!!");
					}else lblCateUp_De.setText("This Category ID does not exist!!!");
				}
			} catch (SQLException e) {e.printStackTrace();}
		});
		//Delte Cate
		btnDeleteCate.addActionListener(event -> {
			String sqlDeleteCate = "delete from Catedgory where IDcate = ?";
			int idDeleteCate = Integer.parseInt(txtCateID.getText());
			int optionDelete;
			if (cate.checkExistCategory(idDeleteCate)) {
				String sqlcheckExistBook = "select * from Book where IDcate = ?";
				try {
					PreparedStatement psDeleteCate = database.getConnection().prepareStatement(sqlDeleteCate);
					PreparedStatement psCheckExistBook = database.getConnection().prepareStatement(sqlcheckExistBook);
					psCheckExistBook.setInt(1, idDeleteCate);
					ResultSet rsCheckExitsBook = psCheckExistBook.executeQuery();
					if (rsCheckExitsBook.next()) {
						optionDelete  = JOptionPane.showConfirmDialog(getParent(), "This category still have " + book.countBook(idDeleteCate)
								+ " book/s!!!\nDo you still want to delete?", "Warning", JOptionPane.YES_NO_OPTION);
						if (optionDelete == 0) {
							String sqlDeleteBook = "select Book.IDbook "
									+ "from Book, Category "
									+ "where Book.IDcate = Book.IDcate and Book.IDcate = ?";
							PreparedStatement psDeleteBook = database.getConnection().prepareStatement(sqlDeleteBook);
							psDeleteBook.setInt(1, idDeleteCate);
							ResultSet rsDeleteBook = psDeleteBook.executeQuery();
							while (rsDeleteBook.next()) {
								book.deleteBookSupportForDeleteCategory(rsDeleteBook.getInt(1));
							}
							psDeleteCate.executeUpdate();
							lblCateUp_De.setText("Category deleted successfully!!!");
						}else lblCateUp_De.setText("Category has deleted yet!!!");
					}
					else {
						psDeleteCate.executeUpdate();
						lblCateUp_De.setText("Category deleted successfully!!!");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}else {
				lblCateUp_De.setText("This category ID does not exist!!!");
			}
		});
		//Search Cate
		btnSearchCate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cate.checkExistCategory(Integer.parseInt(txtCateID.getText()))) {
					String sqlSearchCate = "select Book.*, Category.Name from Book, Category "
							+ "where Category.IDcate = Book.IDcate "
							+ "and Category.IDcate = ?";
					String lineData = "";
					try {
						PreparedStatement psSearchCate = database.getConnection().prepareStatement(sqlSearchCate);
						psSearchCate.setInt(1, Integer.parseInt(txtCateID.getText()));
						ResultSet rsSearchCate = psSearchCate.executeQuery();
						if (rsSearchCate.next()) {
							lineData += "Category : " + rsSearchCate.getString(5) + "\n"
									+ "\t" + rsSearchCate.getInt(1) + "- "
									+ rsSearchCate.getString(2) + " - "
									+ rsSearchCate.getFloat(3) + " - "
									+ rsSearchCate.getInt(4) + "\n";
							if(rsSearchCate.next())
								lineData += "\t" + rsSearchCate.getInt(1) + "-"
										+ rsSearchCate.getString(2) + " - "
										+ rsSearchCate.getFloat(3) + " - "
										+ rsSearchCate.getInt(4) + "\n";
						}else {
							lblCateUp_De.setText("There are no book in this category!!!");
						}
						txtAreaCate.setText(lineData);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else lblCateUp_De.setText("This category ID does not exist!!!");
			}
		});
		//Print all
		btnPrintAllCate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblCateDetail.setText("");
				lblCateDetail.setText("List category shown below");
				String sqlPrintAllCate = "select * from Category";
				int count = 0;
				try {
					PreparedStatement psPrintCate = database.getConnection().prepareStatement(sqlPrintAllCate);
					ResultSet rsPrintCate = psPrintCate.executeQuery();
					sqlPrintAllCate = "";
					while (rsPrintCate.next()) {
						count++;
						sqlPrintAllCate += count + " - " + rsPrintCate.getString(2) + " - " + rsPrintCate.getInt(1) + "\n";
					}
					txtAreaCate.setText(sqlPrintAllCate);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		btnPrintAllBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblBookDetail.setText("");
				lblBookDetail.setText("List book shown below");
				String sqlPrintAllBook = "select Book.* from Book, Category where Category.IDcate = Book.IDcate and Book.IDcate = ?";
				String sqlPrintAllCate = "select IDcate, Name from Category";
				String lineData = "";
				int count = 0;
				try {
					PreparedStatement psPrintCate = database.getConnection().prepareStatement(sqlPrintAllCate);
					ResultSet rsPrintCate = psPrintCate.executeQuery();
					PreparedStatement psPrintBook;
					ResultSet rsPrintBook;
					while (rsPrintCate.next()) {
						psPrintBook = database.getConnection().prepareStatement(sqlPrintAllBook);
						psPrintBook.setInt(1, rsPrintCate.getInt(1));
						rsPrintBook = psPrintBook.executeQuery();
						lineData += "Category : " + rsPrintCate.getString(2) + "\n";
						while (rsPrintBook.next()) {
							count++;
							lineData += "\t" +  count + " : " + rsPrintBook.getInt(1) + "- "
									+ rsPrintBook.getString(2) + " - "
									+ rsPrintBook.getFloat(3) + " - "
									+ rsPrintBook.getInt(4) + "\n";
						}
					}
					txtAreaBook.setText(lineData);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		//end print
		
		//Book
		//Insert book
		btnInsertBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!book.checkExistBook(Integer.parseInt(txtBookID.getText()))) {
					String sqlInsertBook = "insert into Book "
							+ "values(?, ?, ?, ?)";
					PreparedStatement psInsertBook;
					try {
						psInsertBook = database.getConnection().prepareStatement(sqlInsertBook);
						psInsertBook.setInt(1, Integer.parseInt(txtBookID.getText()));
						psInsertBook.setString(2, txtBookTitle.getText());
						psInsertBook.setFloat(3, Integer.parseInt(txtBookPrice.getText()));
						psInsertBook.setInt(4, (int) comboBoxCateID.getSelectedItem());
						psInsertBook.executeUpdate();
						lblBookForUp_De.setText("Book inserted successfully!!!");
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					
				}else {
					lblBookForUp_De.setText("This Book id does exisit - Insert book failed!!!");
					txtBookID.setEditable(false);
					txtBookTitle.setEditable(false);
					txtBookPrice.setEditable(false);
					comboBoxCateID.setEnabled(false);
				}
				txtBookID.setText("");
				txtBookTitle.setText("");
				txtBookPrice.setText("");
				comboBoxCateID.setSelectedIndex(0);
			}
		});
		//Update book
		btnUpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sqlUpdateBook = "update Book "
						+ "set IDbook = ?, Title = ?, Price = ?, IDcate = ? "
						+ "where IDbook = ?";
				try {
					PreparedStatement psUpdateBook = database.getConnection().prepareStatement(sqlUpdateBook);
					psUpdateBook.setInt(1, Integer.parseInt(txtBookID.getText()));
					psUpdateBook.setString(2,txtBookTitle.getText());
					psUpdateBook.setFloat(3, Integer.parseInt(txtBookPrice.getText()));
					psUpdateBook.setInt(4, (int) comboBoxCateID.getSelectedItem());
					psUpdateBook.setInt(5, idBookUpdate);
					if (idBookUpdate == Integer.parseInt(txtBookID.getText())) {
						psUpdateBook.executeUpdate();
						lblBookForUp_De.setText("Book updated successfully!!!");
					}else {
						if (!book.checkExistBook(Integer.parseInt(txtBookID.getText()))) {
							psUpdateBook.executeUpdate();
							lblBookForUp_De.setText("Book updated successfully!!!");
						}else lblBookForUp_De.setText("This Book ID does exist!!!"); 
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		//Delete Book
		btnDeleteBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!book.checkExistBook(Integer.parseInt(txtBookID.getText()))) {
					String sqlDeleteBook = "delete from Book "
										+ "where IDbook = ?";
					try {
						PreparedStatement psDeleteBook = database.getConnection().prepareStatement(sqlDeleteBook);
						psDeleteBook.setInt(1, Integer.parseInt(txtBookID.getText()));
						psDeleteBook.executeUpdate();
						lblBookForUp_De.setText("Book deldeted successfully!!!");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else lblBookForUp_De.setText("This Book ID does exist!!!"); 
			}
		});
		//Search book
		btnSearchBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (book.checkExistBook(Integer.parseInt(txtBookID.getText()))){
					String sqlSearchBook = "select Book.*, Category.Name from Book, Category "
										+ "where Category.IDcate = Book.IDcate "
										+ "and Book.IDbook = ?";
					try {
						PreparedStatement psSearchBook = database.getConnection().prepareStatement(sqlSearchBook);
						psSearchBook.setInt(1, Integer.parseInt(txtBookID.getText()));
						ResultSet rsSearchBook = psSearchBook.executeQuery();
						if (rsSearchBook.next()) {
							txtAreaBook.setText("Category : " + rsSearchBook.getString(5) + "\n"
									+ "\t" + rsSearchBook.getInt(1) + "- "
									+ rsSearchBook.getString(2) + " - "
									+ rsSearchBook.getFloat(3) + " - "
									+ rsSearchBook.getInt(4) + "\n");
						}
						lblBookForUp_De.setText("Book does exist!!!");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else lblBookForUp_De.setText("Book does not exist!!!");
			}
		});
		setVisible(true);
	}
	
	public void takeIDCate() {
		//Take id cate
				String sqlTakeIDcate = "select IDcate from Category";
				try {
					PreparedStatement psTakeIDCate = database.getConnection().prepareStatement(sqlTakeIDcate);
					ResultSet rsTakeIDCate = psTakeIDCate.executeQuery();
					while (rsTakeIDCate.next()) {
						comboBoxCateID.addItem(rsTakeIDCate.getInt(1));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
	}
}
