package swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import Handle.Database;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame{

	private JPanel contentPane;
	private JTextField txtUserName;
	private JPasswordField txtPass;
	Database database;
	private boolean checkLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login JFramelogin = new Login();
/*					JFramelogin.setVisible(true);
					while (!JFramelogin.getLoginState()) {*/
						JFramelogin.setVisible(true);
/*					}
					JFramelogin.setVisible(false);*/
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		database = new Database();
		checkLogin = false;
		
		setTitle("Library");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1166, 507);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		
		setContentPane(contentPane);
		
		contentPane.setLayout(null);
		
		JLabel lblMichaelsLibrary = new JLabel("Michael's Library");
		lblMichaelsLibrary.setHorizontalAlignment(SwingConstants.CENTER);
		lblMichaelsLibrary.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 30));
		lblMichaelsLibrary.setBounds(10, 11, 1130, 63);
		contentPane.add(lblMichaelsLibrary);
		
		JLabel lblTitleLogin = new JLabel("Log in");
		lblTitleLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitleLogin.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblTitleLogin.setBounds(0, 85, 1140, 36);
		contentPane.add(lblTitleLogin);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUserName.setBounds(444, 164, 89, 25);
		contentPane.add(lblUserName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(444, 233, 89, 14);
		contentPane.add(lblPassword);
		
		txtUserName = new JTextField();
		txtUserName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtUserName.setBounds(543, 168, 164, 20);
		contentPane.add(txtUserName);
		txtUserName.setColumns(10);
		
		txtPass = new JPasswordField();
		txtPass.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPass.setBounds(543, 232, 164, 20);
		contentPane.add(txtPass);
		txtPass.setColumns(10);
		
		JLabel lblLoginInfo = new JLabel("");
		lblLoginInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoginInfo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblLoginInfo.setBounds(444, 393, 263, 25);
		contentPane.add(lblLoginInfo);
		
		JButton btnLogin = new JButton("Log in");
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnLogin.setBounds(444, 294, 263, 43);
		contentPane.add(btnLogin);
		setVisible(true);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (database.checkConnectionState()) {
					if (database.checkLogInState(txtUserName.getText(), new String (txtPass.getPassword()))) {
							lblLoginInfo.setForeground(Color.BLUE);
							lblLoginInfo.setText("Log in successfully!!!");
							setVisible(false);
							Library li = new Library();
							li.setVisible(true);
					}else {
						lblLoginInfo.setForeground(Color.RED);
						lblLoginInfo.setText("Log in failed!!!");
					}
				}
			}
		});
	}
	
	public boolean getLoginState() {
		return this.checkLogin;
	}
}
