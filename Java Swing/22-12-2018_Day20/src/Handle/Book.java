package Handle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import MyConnect.MyConnect;

public class Book extends Database{
	private String sqlSyntax;
	private PreparedStatement ps;
	private ResultSet rs;
	private Category category;
	
	public Book() {
		category = new Category();
		ps = null;
		rs = null;
		sqlSyntax = "";
	}
	
	public int countBook(int idCate) {
		int nums = 0;
		sqlSyntax = "select count(Book.Title) from Book, Category"
				+ " where Category.IDcate like Book.IDcate "
				+ "and Category.IDcate = ?";
		try {
			ps = super.getConnection().prepareStatement(sqlSyntax);
			ps.setInt(1, idCate);
			rs = ps.executeQuery();
			if (rs.next())
				nums = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nums;
	}
	
	public void showAllBooks() {
		int count = 0;
		category.showAllCategoriesSupportBook();
		ResultSet rsCategory;
		sqlSyntax = "select * from Book where Book.IDcate = ?";
		try {
			rsCategory = category.getResultSet();
			while (rsCategory.next()) {
				ps = super.getConnection().prepareStatement(sqlSyntax);
				ps.setInt(1, rsCategory.getInt(1));
				rs = ps.executeQuery();
				System.out.println("\nCategory Name : " + rsCategory.getString(2));
				while (rs.next()) {
					System.out.printf("\t%d - %s - %f\n", rs.getInt(1), rs.getString(2), rs.getFloat(3));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void insertBook(int idBook, String titile, float price, int idCate) {
		sqlSyntax = "insert into Book "
				+ "values(?, ?, ?, ?)";
		try {
			if (category.checkExistCategory(idCate)) {
				ps = super.getConnection().prepareStatement(sqlSyntax);
				ps.setInt(1, idBook);
				ps.setString(2, titile);
				ps.setFloat(3, price);
				ps.setInt(4, idCate);
				ps.executeUpdate();
				System.out.println("\nBook inserted successfully!!!\n");
			}else System.out.println("\nThis category does not exist!!!\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteBook(int idBook) {
		/*sqlSyntax = "delete from Book where IDbook = ?";
		try {
			if (this.checkExistBook(idBook)) {
				ps = super.getConnection().prepareStatement(sqlSyntax);
				ps.setInt(1, idBook);
				if(ps.executeUpdate() != 0);
					System.out.println("\nBook deleted successfully!!!\n");
			}else System.out.println("\nThis book does not exist!!!\n");
		} catch (SQLException e) {
			System.out.println("bug");
			System.out.println(e.getMessage());
		}*/
		if (this.checkExistBook(idBook)) {
			try {
					sqlSyntax = "delete from Book where IDbook = ?";
					ps = super.getConnection().prepareStatement(sqlSyntax);
					ps.setInt(1, idBook);
					if (ps.executeUpdate() != 0) System.out.println("\nBook deleted successfully\n");
			} catch (SQLException e) {
					System.out.println(e.getMessage());
			}
		}else System.out.println("\nThis book does not exist!!!\n");
	}
	
	public void updateBook(int idBook, int idBookUpdating, String title, float price, int idCate) {
		String sqlSyntaxUpdating = "update Book set IDbook = ?, Title = ? , Price = ?, IDcate = ? where IDbook = ?";
		PreparedStatement psUpdating;
		if (category.checkExistCategory(idCate)) {
			try {
				psUpdating = super.getConnection().prepareStatement(sqlSyntaxUpdating);
				psUpdating.setInt(1, idBookUpdating);
				psUpdating.setString(2, title);
				psUpdating.setFloat(3, price);
				psUpdating.setInt(4, idCate);
				psUpdating.setInt(5, idBook);
				if (this.checkExistBook(idBook)) {
					if (idBook == idBookUpdating) {
						psUpdating.executeUpdate();
						System.out.println("\nBook updated successfully!!!\n");
					}else {
						if (this.checkExistBook(idBookUpdating))
							System.out.println("\nYour id which you want to update does exist!!!\n");
						else {
							psUpdating.executeUpdate();
							System.out.println("\nBook updated successfully!!!\n");
						}
					}
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}else System.out.println("\nThis book category not exist!!!\n");
	}
	
	public boolean checkExistBook(int idBook) {
		sqlSyntax = "select * from Book where Book.IDbook = ?";
		try {
			ps = super.getConnection().prepareStatement(sqlSyntax);
			ps.setInt(1, idBook);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public void deleteBookSupportForDeleteCategory(int idBook) {
		sqlSyntax = "delete * from Book where Book.IDbook = ?";
		try {
				ps = super.getConnection().prepareStatement(sqlSyntax);
				ps.setInt(1, idBook);
				ps.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
