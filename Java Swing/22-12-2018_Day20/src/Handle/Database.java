package Handle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import MyConnect.MyConnect;

public class Database {
	private Connection cn;
	private String sqlSystax;
	private PreparedStatement ps;
	private ResultSet rs;
	
	public Database() {
		cn = new MyConnect().getcn();
	}
	
	public boolean checkConnectionState() {
		if (cn != null) return true;
		return false;
	}
	
	public boolean checkLogInState(String userName, String password) {
		sqlSystax = "select * from Admin where Username = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, userName);
			rs = ps.executeQuery();
			if (rs.next())
				if (password.equalsIgnoreCase(rs.getString("Password")) && rs.getString(3).equalsIgnoreCase("1")) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Connection getConnection() {
		return this.cn;
	}
}
