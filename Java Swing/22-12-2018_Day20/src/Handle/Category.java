package Handle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import MyConnect.MyConnect;

public class Category extends Database{
	private String sqlSystax;
	private PreparedStatement ps;
	private ResultSet rs;
	private Book book;
	
	public Category() {
	}
	
	public void showAllCategories() {
		int count = 0;
		sqlSystax = "select * from Category";
		try {
			ps = super.getConnection().prepareStatement(sqlSystax);
			rs = ps.executeQuery();
			System.out.println("No\tIDCate\tName");
			while (rs.next()) {
				count++;
				System.out.printf("%d\t%d\t%s\n", count, rs.getInt(1), rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void showAllCategoriesSupportBook() {
		sqlSystax = "select * from Category";
		try {
			ps = super.getConnection().prepareStatement(sqlSystax);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void showBooksOfACategory(int idCate) {
		int i = 0;
		book = new Book();
		String sqlSystaxTakeName = "select Category.Name from Category where IDcate = ?";
		sqlSystax = "select Book.Title from Book, Category where Book.IDcate like Category.IDcate "
				+ " and Category.IDcate = ?";
		try {
			ps = super.getConnection().prepareStatement(sqlSystaxTakeName);
			ps.setInt(1, idCate);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (book.countBook(idCate) == 1)
					System.out.println("\nThere is " + book.countBook(idCate) + " book in " + rs.getString(1)
							+ " category : ");
				else if (book.countBook(idCate) >= 2)
					System.out.println("\nThere are " + book.countBook(idCate) + " books in " + rs.getString(1)
					+ " category : ");
				else 
					System.out.println("\nThere is no book in " + rs.getString(1)
					+ " category : ");
			}
			if (book.countBook(idCate) != 0)
				ps = super.getConnection().prepareStatement(sqlSystax);
				ps.setInt(1, idCate);
				rs = ps.executeQuery();
				while (rs.next()) {
					i++;
					System.out.println(i + " - " + rs.getString(1));
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void showBooksOfACategorySupportForDelete(int idCate) {
		sqlSystax = "select Book.IDbook from Book, Category where Book.IDcate like Category.IDcate "
				+ " and Category.IDcate = ?";
		try {
			ps = super.getConnection().prepareStatement(sqlSystax);
			ps.setInt(1, idCate);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void insertACategory(int idCate, String nameCate) {
		sqlSystax = "insert into Category "
				+ "values(?, ?)";
		try {
			ps = super.getConnection().prepareStatement(sqlSystax);
			ps.setInt(1, idCate);
			ps.setString(2, nameCate);
			ps.executeUpdate();
			System.out.println("\nCategory inserted successfully");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteACategory(int idCate) {
		book = new Book();
		Scanner sc = new Scanner(System.in);
		String deleteornot = "";
		if (this.checkExistCategory(idCate)) {
			try {
				if (book.countBook(idCate) != 0) {
					System.out.println("This category still have " + book.countBook(idCate) + " book/s!!!");
					do {
						System.out.print("Do you still want to delete? (Y/N) : ");
						sc = new Scanner(System.in);
						deleteornot = sc.nextLine();

						switch (deleteornot) {
						case "Y" :
						case "y" :
							this.showBooksOfACategorySupportForDelete(idCate);
							while (rs.next()) {
								book.deleteBookSupportForDeleteCategory(rs.getInt(1));
							}
							sqlSystax = "delete from Category where IDcate = ?";
							ps = super.getConnection().prepareStatement(sqlSystax);
							ps.setInt(1, idCate);
							ps.executeUpdate();
							System.out.println("\nCategory deleted successfully!!!\n");
							break;
						case "N" :
						case "n" :
							break;
						default:
							System.out.println("\nInput must be y/n character!!!\n");
							continue;
						}
						break;
					}while (true);
				}else {
					sqlSystax = "delete from Category where IDcate = ?";
					ps = super.getConnection().prepareStatement(sqlSystax);
					ps.setInt(1, idCate);
					ps.executeUpdate();
					System.out.println("\nCategory deleted successfully!!!\n");
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}else System.out.println("\nThis category does not exist!!!\n");
	}
	
	public boolean checkExistCategory(int idCate) {
		sqlSystax = "select * from Category where IDcate = ?";
		try {
			ps = super.getConnection().prepareStatement(sqlSystax);
			ps.setInt(1, idCate);
			rs = ps.executeQuery();
			if (rs.next()) return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public void updateCategory(int idCate, int idCateUpdating, String nameCateUpdating) {
		if (this.checkExistCategory(idCate)) {
			try {
				sqlSystax = "update Category set Category.IDcate = ?, Category.Name = ? where Category.IDcate = ?";
				ps = super.getConnection().prepareStatement(sqlSystax);
				ps.setInt(1, idCateUpdating);
				ps.setString(2, nameCateUpdating);
				ps.setInt(3, idCate);
				if (idCateUpdating == idCate) {
					ps.executeUpdate();
					System.out.println("\nCategory updated successfully!!!\n");
				}else {
					if (this.checkExistCategory(idCateUpdating))
						System.out.println("\nYour id which you want to update does exist!!!\n");
					else {
						
						ps.executeUpdate();
						System.out.println("\nCategory updated successfully!!!\n");
					}
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}else System.out.println("\nThis category does not exist!!!\n");
	}

	public ResultSet getResultSet() {
		return this.rs;
	}
}
