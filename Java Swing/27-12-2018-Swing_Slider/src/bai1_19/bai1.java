package bai1_19;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class bai1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtValueSilder;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai1 frame = new bai1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 910, 318);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JSlider slider = new JSlider();
		slider.setBounds(125, 89, 652, 42);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		//Step
		slider.setLabelTable(slider.createStandardLabels(10));
		contentPane.setLayout(null);
		contentPane.add(slider);
		
		JLabel lblSliderValue = new JLabel("The value");
		lblSliderValue.setBounds(136, 36, 63, 22);
		contentPane.add(lblSliderValue);
		
		txtValueSilder = new JTextField();
		txtValueSilder.setBounds(209, 36, 200, 22);
		contentPane.add(txtValueSilder);
		txtValueSilder.setColumns(10);
		
		//Catch stateChanged event
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				txtValueSilder.setText(""+slider.getValue());
			}
		});
	}
}
