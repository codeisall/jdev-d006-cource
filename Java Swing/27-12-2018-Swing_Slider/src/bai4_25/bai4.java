package bai4_25;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Font;

public class bai4 extends JFrame {

	private JPanel contentPane;
	private JTextField txtShow;
	private JLabel lblNewLabel_1;
	private JSlider sliderMonth;
	private JLabel lblNewLabel_2;
	private JSlider sliderDay;
	private ArrayList<Integer>listDays;
	private Hashtable<Integer, JLabel> days;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai4 frame = new bai4();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai4() {
		this.listDays = new ArrayList<>();
		this.days = new Hashtable<>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 634, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtShow = new JTextField();
		txtShow.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 34));
		txtShow.setBounds(10, 10, 598, 50);
		contentPane.add(txtShow);
		txtShow.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Year");
		lblNewLabel.setBounds(10, 66, 42, 26);
		contentPane.add(lblNewLabel);
		
		JSlider sliderYear = new JSlider();
		sliderYear.setMaximum(2019);
		sliderYear.setMinimum(1999);
		sliderYear.setValue(5);
		sliderYear.setSnapToTicks(true);
		sliderYear.setPaintTicks(true);
		sliderYear.setPaintLabels(true);
		sliderYear.setBounds(10, 103, 598, 50);
		sliderYear.setLabelTable(sliderYear.createStandardLabels(5));
		contentPane.add(sliderYear);
		
		lblNewLabel_1 = new JLabel("Month");
		lblNewLabel_1.setBounds(10, 164, 42, 26);
		contentPane.add(lblNewLabel_1);
		
		sliderMonth = new JSlider();
		sliderMonth.setMaximum(12);
		sliderMonth.setValue(1);
		sliderMonth.setMinimum(1);
		sliderMonth.setSnapToTicks(true);
		sliderMonth.setPaintTicks(true);
		sliderMonth.setPaintLabels(true);
		sliderMonth.setBounds(10, 201, 598, 50);
		sliderMonth.setLabelTable(sliderMonth.createStandardLabels(1));
		contentPane.add(sliderMonth);
		
		lblNewLabel_2 = new JLabel("Day");
		lblNewLabel_2.setBounds(10, 257, 42, 26);
		contentPane.add(lblNewLabel_2);
		
		sliderDay = new JSlider();
		sliderDay.setMinorTickSpacing(1);
		sliderDay.setSnapToTicks(true);
		sliderDay.setPaintTicks(true);
		sliderDay.setPaintLabels(true);
		sliderDay.setBounds(10, 294, 598, 50);
		sliderDay.setLabelTable(sliderDay.createStandardLabels(1));
		contentPane.add(sliderDay);
		
		//Luc dau
		YearMonth yearMonthObject = YearMonth.of(1999, 1);
		int daysInMonth = yearMonthObject.lengthOfMonth();
		System.out.println(daysInMonth);
		for (int i = 1; i <= daysInMonth; i++){
			days.put(i, new JLabel(""+i));
		}
		sliderDay.setMaximum(daysInMonth);
		sliderDay.setMinimum(1);
		sliderDay.setLabelTable(days);
		
		sliderMonth.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				YearMonth yearMonthObject = YearMonth.of(sliderYear.getValue(), sliderMonth.getValue());
				int daysInMonth = yearMonthObject.lengthOfMonth();
				for (int i = 1; i <= daysInMonth; i++){
					days.put(i, new JLabel(""+i));
				}
				sliderDay.setMaximum(daysInMonth);
				sliderDay.setMinimum(1);
				sliderDay.setLabelTable(days);
				Calendar c = Calendar.getInstance();
				c.set(sliderYear.getValue(), sliderMonth.getValue()-1, sliderDay.getValue()+1);
				SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd yyyy");
				txtShow.setText(format.format(c.getTime()));
			}
		});
		
		sliderDay.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Calendar c = Calendar.getInstance();
				c.set(sliderYear.getValue(), sliderMonth.getValue(), sliderDay.getValue());
				SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd yyyy");
				txtShow.setText(format.format(c.getTime()));
			}
		});
		
		sliderYear.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Calendar c = Calendar.getInstance();
				c.set(sliderYear.getValue(), sliderMonth.getValue(), sliderDay.getValue()+1);
				SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd yyyy");
				txtShow.setText(format.format(c.getTime()));
			}
		});
	}

}
