package bai2_22;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Hashtable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class bai2 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai2 frame = new bai2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 931, 376);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSlider slider = new JSlider();
		slider.setMaximum(3);
		slider.setValue(3);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(225, 134, 374, 44);
		slider.setLabelTable(slider.createStandardLabels(1));
		//chen mau len slider
		String[] colors = {"Yellow", "Red", "Blue", "Black"};
		Hashtable<Integer, JLabel> listColors = new Hashtable<>();
		for (int i = 0; i < colors.length; i++){
			listColors.put(i, new JLabel(colors[i]));
		}
		slider.setLabelTable(listColors);
		slider.setValue(0);
		contentPane.add(slider);
		
		JLabel lblNewLabel = new JLabel("Color");
		lblNewLabel.setBounds(226, 59, 63, 23);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(278, 60, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				textField.setText(""+listColors.get(slider.getValue()).getText());
			}
		});
	}

}
