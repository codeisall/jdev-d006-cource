function validate(){
	var mailPattern = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,5}(\.[a-z]{2,3}){1,2}$/;
	var urlPattern = /^http:(\/){2}www\.[a-zA0-9]+(\.[a-z]{2,3}){1,2}$/;

	if (mailPattern.test(document.getElementById("Email").value)){
		document.getElementById("emailNotification").innerHTML = "Valid";
		document.getElementById("emailNotification").style.color = "blue";
	}
	else {
		document.getElementById("emailNotification").innerHTML = "Invalid";
		document.getElementById("emailNotification").style.color = "red";
	}

	if (urlPattern.test(document.getElementById("url").value)){
		document.getElementById("urlNotification").innerHTML = "Valid";
		document.getElementById("urlNotification").style.color = "blue";
	}
	else {
		document.getElementById("urlNotification").innerHTML = "Invalid";
		document.getElementById("urlNotification").style.color = "red";
	}
}