import java.util.Scanner;


public class Array {
	
	public static void sortIncreasing(int arr[]){
		int temp = 0;
		for (int i = 0; i < arr.length; i++){
			for (int j = i+1; j < arr.length; j++){
				if (arr[i] > arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
	
	public static void sortDecreasing(int arr[]){
		int temp = 0;
		for (int i = 0; i < arr.length; i++){
			for (int j = i+1; j < arr.length; j++){
				if (arr[i] < arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
	
	public static int tong(int arr[]){
		int sum = 0;
		for (int i = 0; i < arr.length; i++)
			sum += arr[i];
		return sum;
	}
	
	public static int Min(int arr[]){
		int min = arr[0];
		for (int i = 1 ; i < arr.length; i++){
			if (arr[i] < min) min = arr[i];
		}
		return min;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = 0 ;
		System.out.print("Input the number of element : ");
		n = sc.nextInt();
		int arr[] = new int[n];
		for (int i = 0; i < n; i++){
			System.out.printf("Input the value of element %d : ", i+1);
			arr[i] = sc.nextInt();
		}
		
		sortIncreasing(arr);
		System.out.println("Xuat mang tang dan: ");
		for (int i = 0; i < arr.length; i++){
			if (i != arr.length - 1) System.out.print(arr[i] + ", ");
			else System.out.print(arr[i]);
		}
		
		sortDecreasing(arr);
		System.out.println("\nXuat mang giam dan: ");
		for (int i = 0; i < arr.length; i++){
			if (i != arr.length - 1) System.out.print(arr[i] + ", ");
			else System.out.print(arr[i]);
		}
		
		System.out.printf("\nTong la : %d", tong(arr));
		System.out.printf("\nGia tri nho nhat trong mang la : %d\n", Min(arr));
	}
	

}
