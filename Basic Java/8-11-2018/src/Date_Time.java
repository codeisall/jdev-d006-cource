import java.text.SimpleDateFormat;
import java.util.Date;

public class Date_Time {

	public static void main(String[] args) {
		Date date = new Date();
		//Hien thi ngay gio he thong theo format san co
		System.out.println(date.toString());
		
		//Theo dinh dang cua nguoi dung
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("EEEE yyyy.MM.dd 'at' hh:mm:ss a zzz");
		//Xuat theo dinh dang nguoi dung
		System.out.println("Date hien tai : " + ft.format(dNow));
		System.out.println(dNow.getDate());
		
	}
}
