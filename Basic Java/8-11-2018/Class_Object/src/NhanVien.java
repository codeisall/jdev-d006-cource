import java.util.Scanner;

public class NhanVien {
	Scanner sc = new Scanner(System.in);
	
	int manv;
	String tennv;
	float luong;
	
	//default constructor
	public NhanVien() {
		this.manv = 0;
		this.tennv = "";
		this.luong = 0;
	}
	
	//parameterize constructor
	public NhanVien(int ma, String ten, float salary){
		this.manv = ma;
		this.tennv = ten;
		this.luong = salary;
	}
	
	public void xuatNV(){
		System.out.println("Thong tin nhan vien : ");
		System.out.println("Ma : " + this.manv);
		System.out.println("Ten : " + this.tennv);
		System.out.println("Luong : " + this.luong);
	}
	
	public void nhapNV(){
		System.out.print("Nhap ma nhan vien : ");
		this.manv = sc.nextInt();
		
		System.out.print("Nhap ten nhan vien : ");
		sc = new Scanner(System.in);
		this.tennv = sc.nextLine();
		
		System.out.print("Nhap luong nhan vien : ");
		this.luong = sc.nextFloat();
	}
	
	public float tinhLuong(){
		float tongluong = 0;
		tongluong = this.luong * 2000;
		return tongluong;
		
	}
	

}
