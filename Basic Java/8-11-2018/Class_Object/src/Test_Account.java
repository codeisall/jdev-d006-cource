import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Test_Account {
	Scanner sc = new Scanner(System.in);
	double amount;
	int countTime = 0;
	String accountNo;
	String setAccount = "bankisyourfriend";
	
	public void testAccount(){
		Account myAccount = new Account();
		int choice;
		do {
			System.out.print("Please enter your account number : ");
			sc = new Scanner(System.in);
			accountNo = sc.nextLine();
			if (!accountNo.equals(setAccount)) {
				System.out.println("You inputed the account number is incorrect!!!");
				countTime++;
			}else {
				System.out.println("You logged in successfully!!!");
				do {
					System.out.println("==========FEATURES==========");
					System.out.println("1. Deposit");
					System.out.println("2. Withdraw");
					System.out.println("3. Show the current balance");
					System.out.println("4. Exit");
					System.out.print("\nInput your choice here : ");
					choice = sc.nextInt();
					
					switch (choice) {
					case 1 : 
						do {
							System.out.print("\nPlease input the amount of money you want to deposit to your account : ");
							amount = sc.nextDouble();
							if (amount < 0) System.out.println("The amount of money must be grater than zero!!!");
						}while (amount < 0);
						myAccount.deposit(amount);
						System.out.println("You deposited successfully!!!");
						break;
					case 2 :
						do {
							System.out.print("\nPlease input the amount of money you want to withdraw from your account : ");
							amount = sc.nextDouble();
							if (amount > myAccount.getBalance()) System.out.println("The amount of money must be less than your account's balance!!!");
						}while (amount > myAccount.getBalance());
						myAccount.withdraw(amount);
						System.out.println("You withdrawed successfully!!!");
						System.out.println("Your current balance is : " + myAccount.getBalance());
						break;
					case 3 :
						System.out.println("\nYour current balance is : " + myAccount.getBalance());
						break;
					case 4 :
						System.out.println("Thank for your using!!!");
						break;
						default :
							System.out.println("\nYour choice is invalid!!!");
							break;
					}
				}while (choice != 4);
			}
			if (countTime == 3) {
				System.out.println("\n\nYou inputed over the permitted time!!!");
				break;
			}
		}while (!accountNo.equals(setAccount));
	}
	
	public static void main(String[] args) {
		Date date = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("EEEE dd-MM-YYY 'at' hh:mm:ss a");
		System.out.println("The current time : " +  ft.format(date));
		Test_Account test = new Test_Account();
		test.testAccount();
		
	}

}
