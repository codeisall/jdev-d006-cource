import java.util.Scanner;

public class Student {
	Scanner sc = new Scanner(System.in);
	int id;
	String name;
	float mathScore;
	float englishScore;
	float letureScore;
	float averageScore;
	
	public Student(){
		this.id = 0;
		this.name = "";
		this.mathScore = 0;
		this.letureScore = 0;
		this.englishScore = 0;
	}
	
	public Student(int id, String name, float math, float leture, float english) {
		this.id = id;
		this.name = name;
		this.mathScore = math;
		this.letureScore = leture;
		this.englishScore = english;
	}
	
	public void inputStudent() {
		System.out.println("Input infomation for student : ");
		System.out.print("Input student's id : ");
		this.id = sc.nextInt();
		System.out.print("Input student's name : ");
		sc = new Scanner(System.in);
		this.name = sc.nextLine();
		System.out.print("Input student's Math score : ");
		this.mathScore = sc.nextFloat();
		System.out.print("Input student's Leture score : ");
		this.letureScore = sc.nextFloat();
		System.out.print("Input student's English score : ");
		this.englishScore = sc.nextFloat();
	}
	
	public void outputStudent() {
		System.out.println("\nInformation of student : ");
		System.out.println("Id : " +  this.id);
		System.out.println("Name : " + this.name);
		System.out.println("Math mark : " + this.mathScore);
		System.out.println("Leture mark : " + this.letureScore);
		System.out.println("English mark : " + this.englishScore);
		System.out.println("Average mark : " + this.averageScore);
	}
	
	public void calculateAverage() {
		this.averageScore = (this.mathScore+this.letureScore+this.englishScore)/3;
	}
	
	public static void main(String[] args) {
		Student st1 = new Student();
		st1.inputStudent();
		st1.calculateAverage();
		st1.outputStudent();
		
		Student st2 = new Student(17038, "Nguyen Lam Thanh", 1.1f, 2.2f, 3.3f);
		st2.calculateAverage();
		st2.outputStudent();
	}
}
