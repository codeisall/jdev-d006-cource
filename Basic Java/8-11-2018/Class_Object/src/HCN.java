import java.util.Scanner;

public class HCN {
	Scanner sc = new Scanner(System.in);
	int width;
	int height;
	
	public HCN(){
		this.width = 0;
		this.height = 0;
	}
	
	public HCN(int w, int h){
		this.width = w;
		this.height = h;
	}
	
	public void display(){
		System.out.printf("\nRectangle has width : %d and height : %d", this.width, this.height);
	}
	
	public void calculatePere(){
		System.out.printf("\nPeremeter of the rectangle is : %d", (this.width+this.height)*2);
	}
	
	public int calculateArea(){
		return this.width*this.height;
	}
	
	public void nhapHCN(){
		System.out.print("Input width for the rectangle : ");
		this.width = sc.nextInt();
		System.out.print("Input height for the rectangle : ");
		this.height = sc.nextInt();
	}
}
