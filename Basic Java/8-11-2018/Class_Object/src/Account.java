
public class Account {
	double amount;
	double balance;
	
	public Account() {
		this.amount = 0;
		this.balance = 0;
	}
	
	public void deposit(double amount) {
		this.amount = amount;
		this.balance += this.amount;
	}
	
	public void withdraw(double amount) {
		this.amount = amount;
		this.balance -= this.amount;
	}
	
	public double getBalance() {
		return this.balance;
	}
}
