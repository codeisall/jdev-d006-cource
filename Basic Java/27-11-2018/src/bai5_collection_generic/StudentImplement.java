package bai5_collection_generic;

import java.util.HashMap;

public class StudentImplement<K, V> {
	private HashMap<K, V> listObject;
	
	public StudentImplement() {
		listObject = new HashMap<>();
	}
	
	public void addValue(K key, V value){
		listObject.put(key, value);
		System.out.println("\nAdded\n");
	}
	
	public void displayAll(){
		System.out.println("=========================");
		System.out.println("List of student : ");
		for (K key : listObject.keySet()){
			System.out.println(listObject.get(key));
		}
		System.out.println("\nToltal of student is : " + listObject.size());
		System.out.println("=========================");
	}
}
