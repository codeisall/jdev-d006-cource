package bai5_collection_generic;

public class Student {
	private String stuID;
	private String stuName;
	private String phoneNum;
	private String adddress;
	
	public Student() {
		this.stuID = "";
		this.stuName = "";
		this.phoneNum = "";
		this.adddress = "";
	}
	
/*	public Student(String id, String name, String phone, String address) {
		this.stuID = id;
		this.stuName = name;
		this.phoneNum = phone;
		this.adddress = address;
	}*/
	
	@Override
	public String toString() {
		return "[[[[[STUDENT]]]]]"
				+"\nName : " + this.stuName
				+"\nID : " + this.stuID
				+ "\nPhone number : " + this.phoneNum
				+"\nAddress : " + this.adddress;
	}

	public String getStuID() {
		return stuID;
	}

	public void setStuID(String stuID) {
		this.stuID = stuID;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getAdddress() {
		return adddress;
	}

	public void setAdddress(String adddress) {
		this.adddress = adddress;
	} 
	
}
