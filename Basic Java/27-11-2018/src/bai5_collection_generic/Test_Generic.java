package bai5_collection_generic;

import java.util.Scanner;

public class Test_Generic {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		String name, id, phoneNum, address;
		Student st;
		StudentImplement<String, Student> m = new StudentImplement();
		
		do {
			System.out.println("1. Add student");
			System.out.println("2. Display all student");
			System.out.println("3. Save list of student");
			System.out.println("4. Exit");
			System.out.println("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				/*m = new StudentImplement<String, Student>();*/
				System.out.print("Input name for student : ");
				sc = new Scanner(System.in);
				name = sc.nextLine();
				System.out.print("Input id for student : ");
				sc = new Scanner(System.in);
				id = sc.nextLine();
				System.out.print("Input phone number for student : ");
				sc = new Scanner(System.in);
				phoneNum = sc.nextLine();
				System.out.print("Input address for student : ");
				sc = new Scanner(System.in);
				address = sc.nextLine();
				st = new Student();
				st.setStuID(id);
				st.setStuName(name);
				st.setPhoneNum(phoneNum);
				st.setAdddress(address);
				m.addValue(id, st);
				break;
			case 2 :
				m.displayAll();
				break;
			case 3 :
				System.out.println("Saved!!!!");
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!");
				break;
			}
		}while (choice != 4);
	}

}
