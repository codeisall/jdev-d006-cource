package bai2;

public class InvalidFormatException extends Exception {
	public InvalidFormatException(String error) {
		super(error);
	}
}
