package bai2;

import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bai1.Product;

public class MobileTest {
	private HashSet<Mobiles> listMobiles;
	private String mauID = "^(SS|SM|NK|MT)[0-9]{4}$";
	
	public MobileTest() {
		listMobiles = new HashSet<Mobiles>();
	}
	
	public void addMobile(){
		Mobiles mobile = new Mobiles();
		Scanner sc = new Scanner(System.in);
		
		Pattern pt = Pattern.compile(mauID);
		Matcher mt ;
		boolean check;
		do {
			do {
				System.out.print("Input mobile id : ");
				mobile.setMobile_ID(sc.nextLine());
				mt = pt.matcher(mobile.getMobile_ID());
				if (!mt.matches()) System.out.println("ID is invalid!!!\nInput agian.");
			}while (!mt.matches());
			 check = listMobiles.add(mobile);
			if (!check) System.out.println("This ID does exist!!!\nInput again");
		}while (!check);
		
		mobile.setModel(this.takeModel(mobile.getMobile_ID()));
		mobile.setColor("1tri5");
		
		do {
			System.out.print("Input price for this mobile : ");
			mobile.setPrice(sc.nextFloat());
			if (mobile.getPrice() < 0 || mobile.getPrice() >= 20000000)
				System.out.println("Price must be in interval [0, 20000000)");
		}while (mobile.getPrice() < 0 || mobile.getPrice() >= 20000000);
		System.out.println("\nAdded\n");
	}
	
	public String takeModel(String str){
		String model = "";
		String mau1 = "^SS";
		String mau2 = "^SM";
		String mau3 = "^NK";
		String mau4 = "^MT";
		Pattern pt1 = Pattern.compile(mau1);
		Matcher mt = pt1.matcher(str);
		if (mt.find()){
			model = mt.replaceAll("Seimen ");
		}
		pt1 = Pattern.compile(mau2);
		mt = pt1.matcher(str);
		if (mt.find()){
			model = mt.replaceAll("Samsung ");
		}
		pt1 = Pattern.compile(mau3);
		mt = pt1.matcher(str);
		if (mt.find()){
			model = mt.replaceAll("Motorola ");
		}
		pt1 = Pattern.compile(mau4);
		mt = pt1.matcher(str);
		if (mt.find()){
			model = mt.replaceAll("Nokia ");
		}
		return model;
	}
	
	public void displayAll(){
		if (listMobiles.size() != 0){
			System.out.println("==========================");
			System.out.println("All mobile : ");
			System.out.println("Model\t\tID\t\tColor\t\tPrice");
			for (Mobiles mo : listMobiles){
				System.out.println(mo);
			}
			System.out.println("==========================");
		}else
			System.out.println("\nEmpty\n");
	}
	
	public void deleteMobile(String str) throws InvalidFormatException{
		Scanner sc = new Scanner(System.in);
		if (this.searchMobile(str)){
			System.out.println("Have this device!!!");
			System.out.print("\nDo you really want to delete this device?\n"
					+ "Input y/n : ");
			sc = new Scanner(System.in);
			String check = sc.nextLine();
			if (check.equals("y") || check.equals("Y")) {
				for (Mobiles mo : listMobiles){
					if (mo.getMobile_ID().equals(str)){
						listMobiles.remove(mo);
						System.out.println("Deleted");
						return;
					}
				}
			}
		}else {
			System.out.println("Cannot find this device to remove!!!");
		}
		
	}
	
	public boolean searchMobile(String str) throws InvalidFormatException{
		Pattern pt = Pattern.compile(mauID);
		Matcher mt = pt.matcher(str);
		if (!mt.matches())
			throw new InvalidFormatException("ID is invalid!!!\nInput agian.");
		for (Mobiles mo : listMobiles){
			if (mo.getMobile_ID().equals(str)){
				return true;
			}
		}
		return false;
	}
}
