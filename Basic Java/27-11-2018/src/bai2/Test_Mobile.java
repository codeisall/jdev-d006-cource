package bai2;

import java.util.InputMismatchException;
import java.util.MissingFormatArgumentException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test_Mobile {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		String id;
		boolean search;
		MobileTest m = new MobileTest();
		
		do {
			System.out.println("*******************************");
			System.out.println("* 1. Add mobile               *");
			System.out.println("* 2. Search mobile by id      *");
			System.out.println("* 3. Delete mobile            *");
			System.out.println("* 4. Show all mobile          *");
			System.out.println("* 5. Exit                     *");
			System.out.println("*******************************");
			do {
				try {
					System.out.println("\nInput your choice here : ");
					sc = new Scanner(System.in);
					choice = sc.nextInt();
					break;
				} catch (InputMismatchException e) {
					System.out.println("*******************************");
					System.out.println("* 1. Add mobile               *");
					System.out.println("* 2. Search mobile by id      *");
					System.out.println("* 3. Delete mobile            *");
					System.out.println("* 4. Show all mobile          *");
					System.out.println("* 5. Exit                     *");
					System.out.println("*******************************");
					System.out.print("\nInvalid input, must be an integer number [1, 5]\nInput again.");
				}
			}while (true);
			
			
			switch (choice){
			case 1 :
				m.addMobile();
				break;
			case 2 :
				do {
					try {
						System.out.print("Input id for searching : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
						search = m.searchMobile(id);
						if(search) System.out.println("Device does exist!!!");
						else System.out.println("Device does not exist!!!");
						break;
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}while(true);
				break;
			case 3 :
				
				do {
					try {
						System.out.print("Input id for deleting : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
						m.deleteMobile(id);
						break;
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}while(true);
				break;
			case 4 :
				m.displayAll();
				break;
			case 5 :
				break;
			default :
				System.out.println("\nYour choice is invalid\n");
				break;
			}
		}while (choice != 5);
	}

}
