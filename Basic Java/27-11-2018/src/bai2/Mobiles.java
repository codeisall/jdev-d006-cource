package bai2;

public class Mobiles {
	private String mobile_ID;
	private String model;
	private String color;
	private float price;
	
	public Mobiles() {
		this.mobile_ID = "";
		this.model = "";
		this.color = "";
	}

	public String getMobile_ID() {
		return mobile_ID;
	}

	public void setMobile_ID(String mobile_ID) {
		this.mobile_ID = mobile_ID;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return this.model+"\t"+this.mobile_ID+"\t"+this.color+"\t"+this.price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mobile_ID == null) ? 0 : mobile_ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mobiles other = (Mobiles) obj;
		if (mobile_ID == null) {
			if (other.mobile_ID != null)
				return false;
		} else if (!mobile_ID.equals(other.mobile_ID))
			return false;
		return true;
	}
	
	
}
