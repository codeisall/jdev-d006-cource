package bai1;

import java.util.Scanner;

public class Test_Product {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		Management m = new Management();
		
		do {
			System.out.println("===================");
			System.out.println("1. Add product");
			System.out.println("2. Search product by id");
			System.out.println("3. Show all prodcut have price over 500$");
			System.out.println("4. Show all product");
			System.out.println("5. Exit");
			System.out.println("===================");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				m.addProduct();
				break;
			case 2 :
				System.out.print("Input the id to search : ");
				sc = new Scanner(System.in);
				String id = sc.nextLine();
				if (m.searchByDI(id))
					System.out.println("The product has id " + id +" does exist");
				else 
					System.out.println("The product has id " + id +" does not exist");
				break;
			case 3 :
				m.displayHighValue();
				break;
			case 4 :
				m.displayAll();
				break;
			case 5 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 5);
	}

}
