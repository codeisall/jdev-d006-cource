package bai1_Vector;

import java.util.Vector;

public class Management {
	private Vector<Product> listProduct;
	
	public Management(){
		listProduct = new Vector<Product>();
	}
	
	public void addProduct(){
		Product product = new Product();
		do {
			product.addProduct();
			if (this.searchByDI(product.getId()))
				System.out.println("This product existed!!!\nInput another product : ");
		}while (this.searchByDI(product.getId()));
		
		if (listProduct.add(product))
			System.out.println("Added!!!");
		else 
			System.out.println("Had not added yet");
	}
	
	public boolean searchByDI(String id){
		for (Product pro : listProduct){
			if (pro.getId().equals(id))
				return true;
		}
		return false;
	}
	
	public void displayAll(){
		System.out.println("===========================");
		System.out.println("All procduct : ");
		System.out.println("ID\tNAME\tPRICE");
		for (Product pro : listProduct){
			System.out.println(pro);
		}
		System.out.println("===========================");
	}
	
	public void displayHighValue(){
		System.out.println("===========================");
		System.out.println("List of products have price over 500$ ; ");
		System.out.println("ID\tNAME\tPRICE");
		for (Product pro : listProduct){
			if (pro.getUniPrice() > 500)
				System.out.println(pro);
		}
		System.out.println("===========================");
	}
}
