package bai1_Vector;

import java.util.Scanner;

public class Product {
	private String id;
	private String name;
	private int uniPrice;

	public Product() {
		this.id = "";
		this.name = "";
		this.uniPrice = 0;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id+"\t"+this.name+"\t"+this.uniPrice+"\n";
	}
	
	public void addProduct(){
		Scanner sc= new Scanner(System.in);
		System.out.print("Input name of product : ");
		sc= new Scanner(System.in);
		this.name = sc.nextLine();
		System.out.print("Input id of product : ");
		sc= new Scanner(System.in);
		this.id = sc.nextLine();
		System.out.print("Input the unit price : ");
		this.uniPrice = sc.nextInt();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUniPrice() {
		return uniPrice;
	}

	public void setUniPrice(int uniPrice) {
		this.uniPrice = uniPrice;
	}
}
