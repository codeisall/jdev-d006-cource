package bai4_HashMap;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test_Doctor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		String id;
		String name;
		int avaihours;
		DoctorManager m = new DoctorManager();
		Doctor doc;
		do {
			System.out.println("*****************************");
			System.out.println("*1. Add doctor              *");
			System.out.println("*2. Search for a doctor     *");
			System.out.println("*3. Display all doctor      *");
			System.out.println("*4. Exit                    *");
			System.out.println("*****************************");
			do {
				try {
					System.out.print("\nInput your choice here : ");
					sc = new Scanner(System.in);
					choice = sc.nextInt();
					break;
				} catch (InputMismatchException e) {
					System.out.println("Input must be an integer number!!!\nInput again.");
				}
			}while (true);
			
			switch (choice) {
			case 1 :
				do {
					try {
						System.out.print("Input name for doctor : ");
						sc = new Scanner(System.in);
						name = sc.nextLine();
						System.out.print("Input id for doctor : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
						System.out.print("Input available hours : ");
						sc = new Scanner(System.in);
						avaihours = sc.nextInt();
						doc = new Doctor();
						doc.setName(name);
						doc.setId(id);
						doc.setAvaihours(avaihours);
						m.addDoctor(doc.getId(), doc);
						break;
					} catch (ExistIdException e) {
						System.out.println(e.getMessage());
					}
				}while (true);
				break;
			case 2 :
				do {
					System.out.print("Input id for doctor : ");
					sc = new Scanner(System.in);
					id = sc.nextLine();
					m.searchDoctor(id);
					break;
				}while (true);
				break;
			case 3 :
				m.displayAll();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 4);
	}

}
