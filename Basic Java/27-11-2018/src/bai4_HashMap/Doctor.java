package bai4_HashMap;

public class Doctor {
	private String id;
	private String name;
	private int avaihours;
	
	public Doctor() {
		this.id = "";
		this.name = "";
		this.avaihours = 0;
	}
	
	@Override
	public String toString() {
		return "Doctor : \n"
			+ "Name : " + this.name
			+ "\nID : " + this.id
			+ "\nAvailable hours : " + this.avaihours + "\n";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAvaihours() {
		return avaihours;
	}

	public void setAvaihours(int avaihours) {
		this.avaihours = avaihours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doctor other = (Doctor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
