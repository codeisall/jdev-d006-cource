package bai4_HashMap;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

public class SortByName implements Comparator<Map.Entry<String,Doctor>>{

	@Override
	public int compare(Entry<String, Doctor> o1, Entry<String, Doctor> o2) {
		return o1.getValue().getName().compareTo(o2.getValue().getName());
	}


}
