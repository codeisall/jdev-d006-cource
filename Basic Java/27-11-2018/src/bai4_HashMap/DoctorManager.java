package bai4_HashMap;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Comparator;


public class DoctorManager {
	private TreeMap<String, Doctor> listDoctor;
	private String checkEmpty;
	
	public DoctorManager() {
		this.listDoctor = new TreeMap<>();
		this.checkEmpty = "[^\\s]";
	}
	
	public void addDoctor(String id, Doctor doc) throws ExistIdException {
		Doctor docAdd = new Doctor();
		docAdd = doc;
		Pattern pt = Pattern.compile(checkEmpty);
		Matcher mc = pt.matcher(doc.getName());
		if (!mc.find() || doc.getName().length() == 0) {
			throw new ExistIdException("Name must have some characters!!!\nInput again.");
		}else {
			if (listDoctor.containsKey(id))
				throw new ExistIdException("This id existed!!!\nInput again.");
			else {
				listDoctor.put(id, docAdd);
				System.out.println("\nAdded\n");
			}
		}
/*		Map<String, Integer> result2 = new LinkedHashMap<>();
		listDoctor.entrySet().stream()
		.sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEachOrdered(x -> result2.put(x.getKey(), x.getValue()));*/
/*		Map<String, Integer> sorted = listDoctor
				        .entrySet()
				        .stream()
				        .sorted(comparingByValue())
				      	.collect(
				            toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
				                LinkedHashMap::new));
				    System.out.println("map after sorting by values: " + sorted);
*/
	}
	
	public void displayAll() {
		System.out.println("========================================");
		System.out.println("List of doctor : ");
/*		for (Doctor doctor : listDoctor.values()) {
			System.out.println(doctor);
		}*/
		for (String key : listDoctor.keySet()) {
			System.out.println(listDoctor.get(key));
		}
		System.out.println("========================================");
	}
	
	public void searchDoctor(String id) {
		if (listDoctor.containsKey(id)) {
			System.out.println("This doctor does exist!!!");
			System.out.println(listDoctor.get(id));
		}else {
			System.out.println("This doctor does not exist!!!");
		}
	}
	
	
}
