package bai6_collection_generic;

import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookManager<K, V> {
	private TreeMap<K, V> bookTreeMap;
	private int countBook;
			
	public BookManager() {
		bookTreeMap = new TreeMap<>();
		this.countBook = 0;
	}
	
	public void addValue(K key, V value) {
		bookTreeMap.put(key, value);
		System.out.println("\nAdded\n");
	}
	
	public void displayAll(){
		System.out.println("==========================");
		System.out.println("List of book : ");
		for (V obj : bookTreeMap.values()){
			System.out.println(obj);
		}
		System.out.println("==========================");
	}

	public int getCountBook() {
		return this.bookTreeMap.size();
	}
	
	
	
	/*public boolean checkID(K id) throws InvalidFormatException{
		Pattern pt = Pattern.compile(mauID);
		Matcher mc = pt.matcher((CharSequence) id);
		if (mc.matches()) return true;
		return false;
	}
	
	public boolean checkEmpty (K str){
		Pattern pt = Pattern.compile(checkEmpty);
		Matcher mc = pt.matcher((CharSequence) str);
		if (mc.find() || ((String) str).length() == 0)
			return false;
		return true;
	}*/
}
