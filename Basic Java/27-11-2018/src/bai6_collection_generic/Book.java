package bai6_collection_generic;

public class Book {
	private String bookID;
	private String type;
	private String title;
	private String artist;
	private double price;
	
	public Book(){
		this.bookID = "";
		this.type = "";
		this.title = "";
		this.artist = "";
		this.price = 0.0;
	}
	
	@Override
	public String toString() {
		return "\n[Book] : " + "ID : " + this.bookID
				+"-Type : " + this.type
				+"-Tile : " + this.title
				+"-Artist : " + this.artist
				+"-Price : " + this.price;
	}

	public String getBookID() {
		return bookID;
	}

	public void setBookID(String bookID) {
		this.bookID = bookID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}  
	
}
