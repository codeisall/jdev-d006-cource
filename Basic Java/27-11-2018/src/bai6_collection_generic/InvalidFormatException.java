package bai6_collection_generic;

public class InvalidFormatException extends Exception {
	public InvalidFormatException(String error) {
		super(error);
	}
}
