package bai6_collection_generic;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test_Book {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		String mauID = "^B[0-9]{4} - A[0-9]{2}$";
		String checkEmpty = "[^\\s]";
		String bookID;
		String type;
		String title;
		String artist;
		double price;
		Pattern pt;
		Matcher mc;
		Book book;
		BookManager<String, Book> m = new BookManager<>();
		
		do {
			System.out.println("1. Add a book");
			System.out.println("2. Display all book");
			System.out.println("3. Count the current number of book");
			System.out.println("4. Exit");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				pt = Pattern.compile(mauID);
				do{
					try {
						System.out.print("Input id : ");
						sc = new Scanner(System.in);
						bookID = sc.nextLine();
						mc = pt.matcher(bookID);
						if (!mc.matches())
							throw new InvalidFormatException("\nThis ID is invalid!!!\nInput again.");
						else break;
					} catch (InvalidFormatException e) {
						System.out.println(e.getMessage());
					}
				}while (true);
				
				pt = Pattern.compile(checkEmpty);
				do{
					try {
						System.out.print("Input title : ");
						sc = new Scanner(System.in);
						title = sc.nextLine();
						mc = pt.matcher(title);
						if (!mc.find())
							throw new InvalidFormatException("\nTile must be have some characters!!!\nInput again.");
						else break;
					} catch (InvalidFormatException e) {
						System.out.println(e.getMessage());
					}
				}while (true);
				
				System.out.print("Input type : ");
				sc = new Scanner(System.in);
				type = sc.nextLine();
				
				do{
					try {
						System.out.print("Input artist : ");
						sc = new Scanner(System.in);
						artist = sc.nextLine();
						mc = pt.matcher(artist);
						if (!mc.find())
							throw new InvalidFormatException("\nArtist must be have some characters!!!\nInput again.");
						else break;
					} catch (InvalidFormatException e) {
						System.out.println(e.getMessage());
					}
				}while (true);

				System.out.print("Input price : ");
				sc = new Scanner(System.in);
				price = sc.nextDouble();
				
				book = new Book();
				book.setBookID(bookID);
				book.setTitle(title);
				book.setArtist(artist);
				book.setType(type);
				book.setPrice(price);
				m.addValue(book.getBookID(), book);
				break;
			case 2 :
				m.displayAll();
				break;
			case 3 :
				System.out.print("Total of book : " + m.getCountBook());
				
				System.out.println();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!");
				break;
			}
		}while (choice != 4);
	}

}
