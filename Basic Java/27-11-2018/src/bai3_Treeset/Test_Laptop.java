package bai3_Treeset;

import java.util.Scanner;

import bai2.InvalidFormatException;

public class Test_Laptop {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice  = 0;
		String search = "";
		LaptopManagement m = new LaptopManagement();
		
		do {
			System.out.println("**************************************");
			System.out.println("* 1. Add Laptop                      *");
			System.out.println("* 2. Search for a laptop             *");
			System.out.println("* 3. Display all laptop              *");
			System.out.println("* 4. Exit                            *");
			System.out.println("**************************************");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice) {
			case 1 :
				m.addLaptop();
				break;
			case 2 :
				do {
					try {
						System.out.print("Input a id for searching : ");
						sc = new Scanner(System.in);
						search = sc.nextLine();
						if (m.searchLaptop(search))
							System.out.println("This laptop does exist!!!");
						else
							System.out.println("This laptop does not exist!!!");
						break;
					} catch (InvalidFormatException e) {
						System.out.println(e.getMessage());
					}
				}while (true);
				break;
			case 3 :
				m.displayAll();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 4);
	}

}
