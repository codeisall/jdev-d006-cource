package bai3_Treeset;

import java.util.Scanner;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bai2.InvalidFormatException;

public class LaptopManagement {
	private TreeSet<Laptop> listLaptop;
	private String mauID;
	private String checkEmpty;
	
	public LaptopManagement() {
		super();
		this.listLaptop = new TreeSet<>(new AscentlyByID());
		mauID = "^[LN][0-9]{3}(\\[)[0-9]{2}(\\])$";
		checkEmpty = "[^\\s]";
	}
	
	public void addLaptop() {
		String id, name, country;
		double price;
		Laptop laptop = new Laptop();
		Scanner sc = new Scanner(System.in);
		Pattern pt;
		Matcher mc;
		do {
			try {
				System.out.print("Input id for laptop : ");
				sc = new Scanner(System.in);
				id = sc.nextLine();
				pt = Pattern.compile(mauID);
				mc = pt.matcher(id);
				if (!mc.matches()) {
					throw new InvalidFormatException("ID is invalid!!!\nInput agian.");
				}
				break;
			} catch (InvalidFormatException e) {
				System.out.println(e.getMessage());
			}
		}while (true);
		
		do {
			try {
				System.out.print("Input name for laptop : ");
				sc = new Scanner(System.in);
				name = sc.nextLine();
				pt = Pattern.compile(checkEmpty);
				mc = pt.matcher(name);
				if (!mc.find() || name.length() == 0) {
					throw new InvalidFormatException("Name must have some characters!!!\nInput again.");
				}
				break;
			} catch (InvalidFormatException e) {
				System.out.println(e.getMessage());
			}
		}while (true);
		
		do {
			try {
				System.out.print("Input country for laptop : ");
				sc = new Scanner(System.in);
				country = sc.nextLine();
				pt = Pattern.compile(checkEmpty);
				mc = pt.matcher(country);
				if (!mc.find() || country.length() == 0) {
					throw new InvalidFormatException("Country must have some characters!!!\nInput again.");
				}
				break;
			} catch (InvalidFormatException e) {
				System.out.println(e.getMessage());
			}
		}while (true);
		
		do {
			System.out.print("Input price for laptop : ");
			sc = new Scanner(System.in);
			price = sc.nextDouble();
			if (price <= 0)
				System.out.println("Price must be grater than zero!!!\nInput again.");
		}while (price <= 0);
		laptop.accept(id, name, country, price);
		if (listLaptop.add(laptop))
			System.out.println("\nAdded\n");
		else System.out.println("This laptop has this id does exist!!!\n"
				+ "So, Cannot add!!!");
	}
	
	public boolean searchLaptop(String id) throws InvalidFormatException {
		Pattern pt = Pattern.compile(mauID);
		Matcher mc = pt.matcher(id);
		if (mc.matches()) {
			for (Laptop laptop : listLaptop) {
				if (laptop.getId().equals(id))
					laptop.ouputLaptop();
					return true;
			}
		}else {
			throw new InvalidFormatException("ID is invalid!!!\\\\nInput agian.");
		}
		return false;
	}
	
	public void displayAll() {
		System.out.println("========================================");
		System.out.println("List of Laptops : ");
		for (Laptop laptop : listLaptop) {
				laptop.ouputLaptop();
		}
		System.out.println("========================================");
	}
}
