package bai3_Treeset;

import java.util.Comparator;

public class AscentlyByID implements Comparator<Laptop>{

	@Override
	public int compare(Laptop o1, Laptop o2) {
		return o1.getId().compareTo(o2.getId());
	}
}
