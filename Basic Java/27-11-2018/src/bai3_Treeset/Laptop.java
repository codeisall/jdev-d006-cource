package bai3_Treeset;

import org.omg.CORBA.CODESET_INCOMPATIBLE;

public class Laptop {
	private String id;
	private String name;
	private String country;
	private double price;
	
	public Laptop() {
		this.id = "";
		this.name = "";
		this.country = "";
		this.price = 0.0;
	}
	
	public void accept(String id, String name, String country, double price) {
		this.id = id;
		this.name = name;
		this.country = this.standardizeCountry(country);
		this.price = price;
	}
	
	public void ouputLaptop() {
		System.out.println("Name : " + this.name);
		System.out.println("ID : " + this.id);
		System.out.println("Country : " + this.country);
		System.out.println("Price : " + this.price);
	}
	
	public String standardizeCountry(String country) {
		if (country.equals("VN")) country = "Vietnam";
		else if (country.equals("CHN")) country = "China";
		return country;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Laptop other = (Laptop) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
