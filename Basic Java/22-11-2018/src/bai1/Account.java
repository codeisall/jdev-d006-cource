package bai1;

import java.util.Scanner;

public class Account {
	private String customerName;
	private String accountNumber;
	private int accountBalance;
	
	public Account() {
		this.customerName = "";
		this.accountNumber = "";
		this.accountBalance = 0;
	}
	
	public Account(String name, String number, int balance){
		this.customerName = name;
		this.accountNumber = number;
		this.accountBalance = balance;
	}
	
	public void createAccount() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input name : ");
		sc = new Scanner(System.in);
		this.customerName = sc.nextLine();
		System.out.print("Input account number : ");
		sc = new Scanner(System.in);
		this.accountNumber = sc.nextLine();
		do {
			try {
				System.out.print("Input your balance : ");
				this.accountBalance = sc.nextInt();
				if (this.accountBalance < 100)
				throw new InsufficientFundsException("Balance must be grater than or equal 100");
				break;
			} catch (InsufficientFundsException e) {
				System.out.println(e.getMessage());
			}
		}while (true);
		
	}
	
	public void displayAccountDetail(Account acc){
		System.out.println("Name : " + acc.customerName);
		System.out.println("Account number : " + acc.accountNumber);
		System.out.println("Account balance : " + acc.accountBalance);
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(int accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	
	
	public boolean checkExist(){
		return false;
	}
}
