package bai1;

import java.util.Scanner;

public class Bank extends Account{
	Account[] accList;
	private int accMax;
	private int nextAccount;
	private int index;
	public Bank() {
		super();
		accList = new Account[0];
	}
	
	public void createAccount(){
		Account acc = new Account();
		acc.createAccount();
		Account[] accAdd = new Account[accList.length+1];
		for (int i = 0; i < accList.length; i++){
			for (int j = i; j <= i; j++){
				accAdd[j] = accList[i];
			}
		}
		accAdd[accList.length] = acc;
		accList = accAdd;
	}
	
	public void displayAccountDetail(Account acc){
		super.displayAccountDetail(acc);
	}
	
	public void disposite() {
		String accNum;
		int money;
		Scanner sc = new Scanner(System.in);	
		do {
			try {
				System.out.print("Input your account number  : ");
				sc = new Scanner(System.in);
				accNum = sc.nextLine();
				if (!this.checkExist(accNum)) throw new InsufficientFundsException("This account does not exist");
				break;
			} catch (InsufficientFundsException e){
				System.out.println(e.getMessage());
			}
		}while (true);
		
		
		do {
			try {
				System.out.print("Input a mount of money you want to desposit : ");
				money = sc.nextInt();
				if (money < 0) throw new InsufficientFundsException("A mount of money to depsosite must be grater than 0");
				accList[index].setAccountBalance(accList[index].getAccountBalance()+money);
				break;
			} catch (InsufficientFundsException e){
				System.out.println(e.getMessage());
			}
		}while (true);
		this.displayAccountDetail(accList[index]);
	}
	
	public void withdraw() {
		String accNum;
		int money;
		Scanner sc = new Scanner(System.in);
		do {
			try {
				System.out.print("Input your account number  : ");
				sc = new Scanner(System.in);
				accNum = sc.nextLine();
				if (!this.checkExist(accNum)) throw new InsufficientFundsException("This account does not exist");
				break;
			} catch (InsufficientFundsException e){
				System.out.println(e.getMessage());
			}
		}while (true);
		
		
		do {
			try {
				System.out.print("Input a mount of money you want to withdraw : ");
				money = sc.nextInt();
				if (money < 0 || money > accList[index].getAccountBalance()) throw new InsufficientFundsException("A mount of money to depsosite must be grater than 0 or less than your current balance");
				accList[index].setAccountBalance(accList[index].getAccountBalance()-money);
				break;
			} catch (InsufficientFundsException e){
				System.out.println(e.getMessage());
			}
		}while (true);
		this.displayAccountDetail(accList[index]);
	}
	
	public boolean checkExist(String acc){
		for (int i = 0; i < accList.length; i++){
			if (accList[i].getAccountNumber().equals(acc)){
				this.index = i;
				return true;
			}
		}
		return false;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	
}
