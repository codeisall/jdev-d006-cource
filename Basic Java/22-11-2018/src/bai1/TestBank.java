package bai1;

import java.util.Scanner;

public class TestBank {

	public static void main(String[] args) {
		Bank bank = new Bank();
		Scanner sc = new Scanner(System.in);
		int choice;
		do {
			System.out.println("1. Create a new account");
			System.out.println("2. Withdraw");
			System.out.println("3. Disposite");
			System.out.println("4. Exit");
			System.out.println("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				bank.createAccount();
				break;
			case 2 :
				bank.withdraw();
				break;
			case 3 :
				bank.disposite();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid\n");
				break;
			}
		}while (choice != 4);
	}

}
