package bai1_fix;

import java.util.Scanner;

public class Account {
	private String accountName;
	private String accountNumber;
	private int accountBalance;
	
	public Account() {
		this.accountName = "";
		this.accountNumber = "";
		this.accountBalance = 0;
	}
	
	public void createAccount() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input name : ");
		sc = new Scanner(System.in);
		this.accountName = sc.nextLine();
		System.out.print("Input account number : ");
		sc = new Scanner(System.in);
		this.accountNumber = sc.nextLine();
		do {
			try {
				System.out.print("Input balance : ");
				this.accountBalance = sc.nextInt();
				if (this.accountBalance < 100) 
					throw new InsufficienFundsException("\nYour balance mus be grater than or equal 100\n");
				break;
			} catch (InsufficienFundsException e) {
				System.out.println(e.getMessage());
			}
		}while (true);
		System.out.println("Created!!!");
	}
	
	public void displayAccountDetail(Account acc) {
		System.out.println("\n<<<<<<<<<<THE CURRENT INOF OF YOUR ACCOUNT>>>>>>>>>>");
		System.out.println("Name : " + acc.accountName);
		System.out.println("Account number : " + acc.accountNumber);
		System.out.println("Account balance : " + acc.accountBalance);
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(int accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	
}
