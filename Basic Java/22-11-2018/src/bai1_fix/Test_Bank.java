package bai1_fix;

import java.util.Scanner;

public class Test_Bank {

	public static void main(String[] args) {
		ManageBank mange = new ManageBank();
		Scanner sc = new Scanner(System.in);
		int choice;
		
		do {
			System.out.println("==========AGRIBANK==========");
			System.out.println("1. Create a new account");
			System.out.println("2. Desposite");
			System.out.println("3. Withdraw");
			System.out.println("4. Exit");
			System.out.println("\nInput  your choice here : ");
			choice = sc.nextInt();
			
			switch (choice) {
			case 1 :
				mange.createAccount();
				break;
			case 2 :
				mange.desposite();
				break;
			case 3 :
				mange.withdraw();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid\n");
				break;
			}
		}while (choice != 4);
	}

}
