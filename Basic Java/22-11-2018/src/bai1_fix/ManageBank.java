package bai1_fix;

public class ManageBank {
	Account[] accList;
	Bank bank;
	
	public ManageBank() {
		accList = new Account[0];
	}
	
	public void createAccount() {
		Account acc = new Account();
		Account[] accListAdd = new Account[accList.length+1];
		acc.createAccount();
		for (int i = 0; i < accList.length; i++) {
			for (int j = i; j <= i; j++) {
				accListAdd[j] = accList[i];
			}
		}
		accListAdd[accList.length] = acc;
		accList = accListAdd;
	}
	
	public void desposite() {
		bank = new Bank();
		bank.deposite(accList);
	}
	
	public void withdraw() {
		bank = new Bank();
		bank.withdraw(accList);
	}
}
