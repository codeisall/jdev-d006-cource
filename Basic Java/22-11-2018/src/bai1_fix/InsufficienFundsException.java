package bai1_fix;

public class InsufficienFundsException extends Exception {
	public InsufficienFundsException(String error) {
		super(error);
	}
}
