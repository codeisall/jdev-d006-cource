package bai1_fix;

import java.util.Scanner;

public class Bank {
	private int indexAccount;
	
	public void deposite(Account[] list) {
		String accNum;
		int money;
		Scanner sc = new Scanner(System.in);
		do {
			try {
				System.out.print("Input your account number : ");
				sc = new Scanner(System.in);
				accNum = sc.nextLine();
				if (!this.checkExits(list, accNum))
					throw new InsufficienFundsException("\nThis account does not exist!!!\n");
				break;
			} catch (InsufficienFundsException e) {
				System.out.println(e.getMessage());
				System.out.println("Please, input your account number again !!!");
			}
		}while (true);
		
		do {
			try {
				System.out.print("Input amount of money to desposite : ");
				money = sc.nextInt();
				if (money < 100)
					throw new InsufficienFundsException("\nAt least 100 to desposite!!!\n");
				list[indexAccount].setAccountBalance(list[indexAccount].getAccountBalance()+money);
				break;
			} catch (InsufficienFundsException e) {
				System.out.println(e.getMessage());
				System.out.println("Please, input amount of money to desposite again !!!");
			}
		}while (true);
		list[indexAccount].displayAccountDetail(list[indexAccount]);
	}
	
	public void withdraw(Account[] list) {
		String accNum;
		int money;
		Scanner sc = new Scanner(System.in);
		do {
			try {
				System.out.print("Input your account number : ");
				sc = new Scanner(System.in);
				accNum = sc.nextLine();
				if (!this.checkExits(list, accNum))
					throw new InsufficienFundsException("\nThis account does not exist!!!\n");
				break;
			} catch (InsufficienFundsException e) {
				System.out.println(e.getMessage());
				System.out.println("Please, input your account number again !!!");
			}
		}while (true);
		
		do {
			try {
				System.out.print("Input amount of money to wthdraw : ");
				money = sc.nextInt();
				if (money < 0 || money > list[indexAccount].getAccountBalance())
					throw new InsufficienFundsException("\nAt least 1 or less than your current balance to wthdraw!!!\n");
				list[indexAccount].setAccountBalance(list[indexAccount].getAccountBalance()-money);
				break;
			} catch (InsufficienFundsException e) {
				System.out.println(e.getMessage());
				System.out.println("Please, input amount of money to withdraw again !!!");
			}
		}while (true);
		list[indexAccount].displayAccountDetail(list[indexAccount]);
	}
	
	public boolean checkExits(Account[] accList, String accNum) {
		for (int i = 0; i < accList.length; i++) {
			if (accList[i].getAccountNumber().equals(accNum)) {
				this.indexAccount = i;
				return true;
			}
		}
		return false;
	}
}
