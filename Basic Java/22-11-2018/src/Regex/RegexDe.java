package Regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDe {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String user;
		//Email : 1 den vo cung cac ky tu @ 1 den vo cung ky tu (. 2 den 4 lan ky tu a-z) xaut hien 1 or 2 lan
		String mauMaSV = "^[A-Z]{6}[0-9]{5}$";
		String mauEmail = "^[0-9a-zA-Z_]{1,}@[0-9a-zA-Z]{1,}(\\.[a-zA-Z]{2,4}){1,2}$";
		//http:\\www.mp3.com
		String mauHTTP = "^http:[\\\\]{2}(www\\.){0,1}[a-zA-Z0-9]{1,}\\.[a-zA-Z]{2,4}$";
		Pattern pt = Pattern.compile(mauHTTP, Pattern.CASE_INSENSITIVE);
		System.out.print("Input your email here : ");
		sc = new Scanner(System.in);
		user = sc.nextLine();
		Matcher m = pt.matcher(user);

		if (m.matches()) System.out.println("Your mail is valid");
		else System.out.println("Your mail is invalid");
	}

}  
