 package bai2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Employee {
	private String name;
	private String address;
	private String phoneNumber;
	
	public Employee() {
		this.name = "";
		this.address = "";
		this.phoneNumber = "";
	}
	
	public Employee(String name, String address, String phoneNumer) {
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	
	public String standardizeAddress(){
		String mau = " Drive";
		Pattern pt1 = Pattern.compile(mau, Pattern.CASE_INSENSITIVE);
		Matcher mt1 = pt1.matcher(this.address);
		this.address = mt1.replaceAll("D");
		mau = " Street";
		Pattern pt2 = Pattern.compile(mau, Pattern.CASE_INSENSITIVE);
		Matcher mt2 = pt2.matcher(this.address);
		this.address = mt2.replaceAll("S");
		mau = " Road";
		Pattern pt3 = Pattern.compile(mau, Pattern.CASE_INSENSITIVE);
		Matcher mt3 = pt3.matcher(this.address);
		this.address = mt3.replaceAll("R");
		return this.address;
	}
	
	public String standardizePhone(){
		String mau = "[\\s.]{1,}";
		Pattern pt1 = Pattern.compile(mau, Pattern.CASE_INSENSITIVE);
		Matcher mt1 = pt1.matcher(this.phoneNumber);
		this.phoneNumber = mt1.replaceAll("-");
		return this.phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
}
