import java.util.Scanner;

public class Employee {
	private String id;
	private int numOfProduct;
	
	public Employee() {
		this.id = "";
		this.numOfProduct = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getNumOfProduct() {
		return numOfProduct;
	}

	public void setNumOfProduct(int numOfProduct) {
		this.numOfProduct = numOfProduct;
	}
	
	public boolean overDefault(){
		if (this.numOfProduct > 500) return true;
		return false;
	}                                                                                                                                                                                
	
	public String getResult(){
		if (this.overDefault()) return "Pass!!!";
			return "Fail!!!";
	}
	
	public double getSalary(){
		if (this.overDefault()) {
			return 500*20000 + (this.numOfProduct-500)*30000;
		}
		return 500*20000;
	}
	
	public static void showTitle(){
		System.out.printf("%s%30s%30s%30s\n", "ID", "Product", "Salary", "Final");
	}
	
	@Override
	public String toString() {
		return String.format("%s", this.id) + String.format("%30d", this.numOfProduct) + String.format("%30f", this.getSalary())
		+ String.format("%30s", this.getResult());
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String id;
		int numofproduct;
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		
		System.out.println("Input some attributes for employee 1 : ");
		System.out.print("Input employee's id : ");
		sc = new Scanner(System.in);
		id = sc.nextLine();
		e1.setId(id);
		System.out.print("Input the number of product : ");
		numofproduct = sc.nextInt();
		e1.setNumOfProduct(numofproduct);
		Employee.showTitle();
		System.out.println(e1.toString());
		
		System.out.println("\nInput some attributes for employee 2 : ");
		System.out.print("Input employee's id : ");
		sc = new Scanner(System.in);
		id = sc.nextLine();
		e2.setId(id);
		System.out.print("Input the number of product : ");
		numofproduct = sc.nextInt();
		e2.setNumOfProduct(numofproduct);
		Employee.showTitle();
		System.out.println(e2.toString());
			
	}
}
