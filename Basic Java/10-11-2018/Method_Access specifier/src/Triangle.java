import java.util.Scanner;

public class Triangle {
	private int ma;
	private int mb;
	private int mc;
	
	public Triangle(){
		this.ma = 0;
		this.mb = 0;
		this.mc = 0;
	}
	
	public int getMa(){
		return this.ma;
	}
	
	public int getMb(){
		return this.mb;
	}
	
	public int getMc(){
		return this.mc;
	}
	
	public void setMa(int side){
		if (side < 0) this.ma = 0;
		else this.ma = side;
	}
	
	public void setMb(int side){
		if (side < 0) this.mb = 0;
		else this.mb = side;
	}
	
	public void setMc(int side){
		if (side < 0) this.mc = 0;
		else this.mc = side;
	}
	
	public int getPere(){
		return this.ma+this.mb+this.mc;
	}
	
	public float getArea(){
		float p = (this.ma+this.mb+this.mc)/2;
		return (float) Math.sqrt(p*(p-this.ma)*(p-this.mb)*(p-this.mc));
	}
	
	public boolean checkTriangle(){
		if (this.ma + this.mb > this.mc && this.ma + this.mc > this.mb && this.mc + this.mb > this.ma)
			return true;  
		return false;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int sideA;
		int sideB;
		int sideC;
		Triangle tr1 = new Triangle();
		System.out.println("Input three sides for the triagle : ");
		System.out.print("Input the length of side a : ");
		sideA = sc.nextInt();
		tr1.setMa(sideA);
		System.out.print("Input the length of side b : ");
		sideB = sc.nextInt();
		tr1.setMb(sideB);
		System.out.print("Input the length of side c : ");
		sideC = sc.nextInt();
		tr1.setMc(sideC);
		
		if (tr1.checkTriangle()){
			System.out.println("The peremeter of the triangle : " + tr1.getPere());
			System.out.println("The area of the triangle : " + tr1.getArea());
		}else {
			System.out.println("Cannot create a completed triangle!!!");
		}
	}
}
