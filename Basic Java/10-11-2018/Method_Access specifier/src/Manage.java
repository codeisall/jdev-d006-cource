import java.util.Scanner;

public class Manage {
	Scanner sc = new Scanner(System.in);
	
	public void inputStudent(Student st){
		int id;
		String name;
		float theory;
		float practice;
		System.out.println("Input the information for student : ");
		System.out.println("Input student's id : ");
		id = sc.nextInt();
		st.setId(id);
		System.out.println("Input student's name : ");
		sc = new Scanner(System.in);
		name = sc.nextLine();
		st.setNameStudent(name);
		System.out.println("Input the theory mark : ");
		theory = sc.nextFloat();
		st.setTheoryMark(theory);
		System.out.println("Input the practice mark : ");
		practice = sc.nextFloat();
		st.setPracticeMark(practice);
	}
	
	public float averageMark(Student st){
		return (st.getTheoryMark()+st.getPracticeMark())/2;
	}
	
	public void outputStudent(Student st){
		System.out.println("\nInformation of student : ");
		System.out.println("ID is : " + st.getId());
		System.out.println("Name is : " + st.getNameStudent());
		System.out.println("Theory mark is : " + st.getTheoryMark());
		System.out.println("Practice mark is : " + st.getPracticeMark());
		System.out.println("Average mark is : " + this.averageMark(st));
	}
}
