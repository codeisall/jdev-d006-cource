   
public class Student {
	private int id;
	private String nameStudent;
	private float theoryMark;
	private float practiceMark;
	
	public Student(){
		this.id = 0;
		this.nameStudent = "";
		this.theoryMark = 0;
		this.theoryMark = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameStudent() {
		return nameStudent;
	}

	public void setNameStudent(String nameStudent) {
		this.nameStudent = nameStudent;
	}

	public float getTheoryMark() {
		return theoryMark;
	}

	public void setTheoryMark(float theoryMark) {
		this.theoryMark = theoryMark;
	}

	public float getPracticeMark() {
		return practiceMark;
	}

	public void setPracticeMark(float practiceMark) {
		this.practiceMark = practiceMark;
	}
}
