package bai2;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class WriteData {
	public static void main(String[] args) {
		String fileName = "D://4-12-2018//bai2.txt";
		Scanner sc = new Scanner(System.in).useDelimiter("\n");
		String str;
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(fileName);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			int n = 3;
			for (int i = 0; i < n; i++){
				System.out.println("Input a string : ");
				str = sc.next() + "\r\n";
				byte arrByte[] = str.getBytes();
				bos.write(arrByte);
				bos.flush();
			}
			fos.close();
			bos.close();
			System.out.println("Wrote file!!!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
