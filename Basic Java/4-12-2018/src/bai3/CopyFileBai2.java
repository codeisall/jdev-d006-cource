package bai3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFileBai2 {

	public static void main(String[] args) {
		String fileNameBai2 = "D://4-12-2018//bai2.txt";
		String fileName3 = "D://4-12-2018//bai3.txt";
		
		FileReader fis;
		try {
			fis = new FileReader(fileNameBai2);
			BufferedReader bis = new BufferedReader(fis);
			FileWriter fos = new FileWriter(fileName3);
			BufferedWriter bos = new BufferedWriter(fos);
			String str = "";
			
			do {
				str = bis.readLine();
				if (str != null)
					bos.write(str + "\r\n");
			}while (str != null);
			
			bos.close();
			System.out.println("Copied!!!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
