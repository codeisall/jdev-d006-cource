package bai6;

import java.util.HashSet;

public class ProductManage {
	private HashSet<Product> listProduct;
	private String fileName;
	HandleFile<Product> handle;
	
	public ProductManage() {
		this.listProduct = new HashSet<>();
		this.fileName = "Products.dat";
		handle = new HandleFile<>();
		listProduct = handle.readFile(listProduct, fileName);
	}
	
	public void addProcduct(Product pro) {
		
		if (listProduct.add(pro)) {
			System.out.println("\nAdded Product!!!\n");
		}else {
			System.out.println("\nThis id does exist!!!");
		}
		handle.writeFile(listProduct, fileName);
	}
	
	public void deleteProcduct(String id) {
		Product proDelete = new Product("","", 0.0);
		if (this.searchProductById(id)) {
			for (Product pro : listProduct) {
				if (pro.getIdProduct().equals(id)) {
					proDelete = pro;
					listProduct.remove(pro);
					break;
				}
			}
			System.out.println("\nRemoved : " + proDelete);
		}
		else {
			System.out.println("\nThis id does exist!!!\n");
		}
		handle.writeFile(listProduct, fileName);
	}
	
	public boolean searchProductById(String id) {
		for (Product pro : listProduct) {
			if (pro.getIdProduct().equals(id)) return true;
		}
		return false;
	}
	
	public boolean searchProductByName(String name) {
		for (Product pro : listProduct) {
			if (pro.getNameProduct().equals(name)) return true;
		}
		return false;
	}
	
	public void listHighestPriceProduct() {
		System.out.println("========================================");
		System.out.println("List of the highest price product : ");
		for (Product pro : listProduct) {
			if (pro.getPriceProduct() >= 500) {
				System.out.println(pro);
			}
		}
		System.out.println("========================================");
	}
	
	public void listAll() {
		System.out.println("========================================");
		System.out.println("List of all product : ");
		for (Product pro : listProduct) {
			System.out.println(pro);
		}
		System.out.println("========================================");
	}
}
