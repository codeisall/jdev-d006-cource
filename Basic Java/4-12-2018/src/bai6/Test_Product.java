package bai6;

import java.util.Scanner;

public class Test_Product {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		String id;
		String name;
		double price;
		Product product;
		ProductManage manage = new ProductManage();
		NewException exception = new NewException();
		
		do {
			System.out.println("***********************************************");
			System.out.println("* 1. Add a new product                        *");
			System.out.println("* 2. Delete a product                         *");
			System.out.println("* 3. Search for a product by ID               *");
			System.out.println("* 4. Search for a product by Name             *");
			System.out.println("* 5. List the highest cost product            *");
			System.out.println("* 6. List all product                         *");
			System.out.println("* 7. Exit                                     *");
			System.out.println("***********************************************");
			
			do {
				try {
					System.out.print("\nInput your choice here : ");
					sc = new Scanner(System.in);
					choice = sc.nextInt();
					break;
				} catch (Exception e) {
					System.out.println("\nInput must be an number!!!\n");
				}
			}while (true);
			
			switch (choice) {
				case 1 :
					do {
						System.out.print("Input name for the product : ");
						sc = new Scanner(System.in);
						name = sc.nextLine();
					}while (!exception.checkEmptyField(name));
					
					do {
						System.out.print("Input id for the product : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
					}while (!exception.checkEmptyField(id));
					
					do {
						System.out.print("Input price for the product : ");
						sc = new Scanner(System.in);
						price = sc.nextDouble();
					}while (price <= 0);
					product = new Product(id, name, price);
					manage.addProcduct(product);
					break;
				case 2 :
					do {
						System.out.print("Input id for deleting : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
					}while (!exception.checkEmptyField(id));
					if (manage.searchProductById(id))
						manage.deleteProcduct(id);
					else 
						System.out.println("\nThis id does not exist!!!\n");
					break;
				case 3 :
					do {
						System.out.print("Input id for searching by id : ");
						sc = new Scanner(System.in);
						id = sc.nextLine();
					}while (exception.checkEmptyField(id));
					if (!manage.searchProductById(id))
						System.out.println("\nThis id does exist!!!\n");
					else
						System.out.println("\nThis id does not exist!!!\n");
					break;
				case 4 :
					do {
						System.out.print("Input name for searching by name : ");
						sc = new Scanner(System.in);
						name = sc.nextLine();
					}while (exception.checkEmptyField(name));
					if (manage.searchProductByName(name))
						System.out.println("\nThis name does exist!!!\n");
					else
						System.out.println("\nThis name does not exist!!!\n");
					break;
				case 5 :
					manage.listHighestPriceProduct();
					break;
				case 6 :
					manage.listAll();
					break;
				case 7 :
					System.out.println("\nGoodBye!!!\n");
					break;
				default :
					System.out.println("\nYour choice is invalid!!!\n");
					break;
			}
		}while (choice != 7);
	}

}
