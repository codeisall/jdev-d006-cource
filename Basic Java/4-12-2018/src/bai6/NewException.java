package bai6;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewException {
	private String emptyField;
	public NewException() {
		this.emptyField = "[\\S]";
	}
	
	public boolean checkEmptyField(String str) {
		Pattern pt = Pattern.compile(emptyField);
		Matcher mc = pt.matcher(str);
		if (!mc.find() || str.length() == 0) return false;
		return true;
	}
}
