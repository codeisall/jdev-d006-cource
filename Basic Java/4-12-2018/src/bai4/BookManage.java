package bai4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class BookManage {
	private String fileName = "D://4-12-2018//book.dat";
	
	
	public void writeBook(Book book){
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(this.fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(book);
			oos.close();
			fos.close();
			System.out.println("Wrote this book!!!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Book readBook(){
		Book book = new Book();
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(this.fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			book = (Book) ois.readObject();
			ois.close();
			fis.close();
			System.out.println("Read!!!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return book;
	}
}
