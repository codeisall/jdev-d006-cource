package bai4;

public class Test_Book {

	public static void main(String[] args) {
		BookManage manage = new BookManage();
		Book book = new Book("Nguyen Lam Thanh", 15000);
		
		manage.writeBook(book);
		Book bookRead = new Book();
		bookRead = manage.readBook();
		System.out.println("Info of the book : ");
		System.out.println(bookRead);
	}

}
