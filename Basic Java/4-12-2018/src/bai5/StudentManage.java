package bai5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentManage {
	private HashSet<Student> listStudent;
	private String fileName;
	private String checkEmpty;
	private String formatDate;
	
	public StudentManage()  {
		this.formatDate = "^[0123]{0, 1}[]";
		this.fileName = "student.dat";
		this.listStudent = new HashSet<>();
		this.checkEmpty = "[\\S]";
		this.readStudent();
	}
	
	public void writeStudent() {
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(this.fileName);
			oos = new ObjectOutputStream(fos);
			
			oos.writeObject(this.listStudent);
			oos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readStudent()  {
		File file = new File(this.fileName);
		if (file.length() != 0) {
			FileInputStream fis;
			try {
				fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				this.listStudent = (HashSet) ois.readObject();
				ois.close();
				fis.close();
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}else {
			System.out.print("");
		}
	}
		
	public void addStudent(Student st) throws ExistIdException {
		if (this.listStudent.add(st)) {
			System.out.println("\nAdded!!!\n");
		}else {
			throw new ExistIdException("\nThis id does exist!!!");
		}
		this.writeStudent();
	}
	
	public boolean searchStudent(String id) {
		for (Student st : this.listStudent) {
			if (st.getStudentID().equals(id))
				return true;
		}
		return false;
	}
	
	public Student deleteStudent(String id) {
		Student student = new Student();
		for (Student st : this.listStudent) {
			if (st.getStudentID().equals(id)) {
				student = st;
				listStudent.remove(st);
				break;
			}
		}
		System.out.println("\nDelete student : ");
		return student;
	}
	
	public void editStudent (Student student, String id) {
		for (Student st : this.listStudent) {
			if (st.getStudentID().equals(id)) {
				st.setStudentID(student.getStudentID());
				st.setStudentName(student.getStudentName());
				st.setStudentDate(student.getStudentDate());
				st.setStudentAdress(student.getStudentAdress());
				st.setStudentClass(student.getStudentClass());
				break;
			}
		}
		System.out.println("\nChanged!!!\n");
	}
	
	public void displayAll() {
		System.out.println("=========================");
		System.out.println("List of student : ");
		for (Student st : this.listStudent) {
			System.out.println(st);
		}
		System.out.println("=========================");
	}
	
	public int getNumberOfStudent() {
		return this.listStudent.size();
	}
	
	public boolean emptyField(String str) {
		Pattern pt = Pattern.compile(this.checkEmpty);
		Matcher mc = pt.matcher(str);
		if (!mc.find() || str.length() == 0) return false;
		return true;
	}
}
