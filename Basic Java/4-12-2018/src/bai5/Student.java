package bai5;

import java.io.Serializable;

public class Student implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String studentID;
	private String studentName;
	private String studentDate;
	private String studentAdress;
	private String studentClass;
	
	public Student(String studentID, String studentName, String studentDate, String studentAdress, String studentClass) {
		this.studentID = studentID;
		this.studentName = studentName;
		this.studentDate = studentDate;
		this.studentAdress = studentAdress;
		this.studentClass = studentClass;
	}

	public Student() {
		this.studentID = "";
		this.studentName = "";
		this.studentDate = "";
		this.studentAdress = "";
		this.studentClass = "";
	}

	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentDate() {
		return studentDate;
	}

	public void setStudentDate(String studentDate) {
		this.studentDate = studentDate;
	}

	public String getStudentAdress() {
		return studentAdress;
	}

	public void setStudentAdress(String studentAdress) {
		this.studentAdress = studentAdress;
	}

	public String getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(String studentClass) {
		this.studentClass = studentClass;
	}

	@Override
	public String toString() {
		return "Student [studentID=" + studentID + ", studentName=" + studentName + ", studentDate=" + studentDate
				+ ", studentAdress=" + studentAdress + ", studentClass=" + studentClass + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((studentID == null) ? 0 : studentID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (studentID == null) {
			if (other.studentID != null)
				return false;
		} else if (!studentID.equals(other.studentID))
			return false;
		return true;
	}
}
