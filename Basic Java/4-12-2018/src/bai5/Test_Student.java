package bai5;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Test_Student {

	public void menuIdExist() {
		System.out.println("*************************************************");
		System.out.println("* 1. Input another ID and remain the rest info  *");
		System.out.println("* 2. Input for whole info                       *");
		System.out.println("*************************************************");
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		String id = "", name = "", date = "", address = "", className = "";
		Student st;
		boolean inputPermittion = true;
		
		StudentManage m = new StudentManage();
		
		do {
			System.out.println("******************************************");
			System.out.println("* 1. Add a new student                   *");
			System.out.println("* 2. Delete a student                    *");
			System.out.println("* 3. Edit a student                      *");
			System.out.println("* 4. Search for a student                *");
			System.out.println("* 5. Display all student                 *");
			System.out.println("* 6. Count the current number of student *");
			System.out.println("* 7. Exit                                *");
			System.out.println("******************************************");
			
			do {
				try {
					System.out.print("Input your choice here : ");
					sc = new Scanner(System.in);
					choice = sc.nextInt();
					break;
				} catch (InputMismatchException e) {
					System.out.println("\nInput must be an number!!!\nPleae, Input again.");
				}
			}while (true);
			
			switch (choice) {
			case 1 :
				do {
					if (inputPermittion) {
						do {
							System.out.print("Input name for the student : ");
							sc = new Scanner(System.in);
							name = sc.nextLine();
							if (m.emptyField(name)) break;
						}while (true);
						
						do {
							System.out.print("Input id for the student : ");
							sc = new Scanner(System.in);
							id = sc.nextLine();
							if (m.emptyField(id)) break;
						}while (true);
						
						do {
							System.out.print("Input date for student : ");
							sc = new Scanner(System.in);
							date = sc.nextLine();
							if (m.emptyField(date)) break;
						}while (true);
						
						do {
							System.out.print("Input address for student : ");
							sc = new Scanner(System.in);
							address = sc.nextLine();
							if (m.emptyField(address)) break;
						}while (true);
						
						do {
							System.out.print("Input name of class for student : ");
							sc = new Scanner(System.in);
							className = sc.nextLine();
							if (m.emptyField(className)) break;
						}while (true);
						
					}
					st = new Student(id, name, date, address, className);
					try {
						m.addStudent(st);
						break;
					} catch (ExistIdException e) {
						System.out.println(e.getMessage());
						System.out.println("*************************************************");
						System.out.println("* 1. Input another ID and remain the rest info  *");
						System.out.println("* 2. Input for whole info                       *");
						System.out.println("*************************************************");
						
						do {
							try {
								System.out.print("Input your choice here : ");
								sc = new Scanner(System.in);
								choice = sc.nextInt();
								break;
							} catch (InputMismatchException ex) {
								System.out.println("\nInput must be an number!!!\nPleae, Input again.");
							}
						}while (true);
						
						switch (choice) {
							case 1 :
								System.out.print("Inputa another id for the student : ");
								sc = new Scanner(System.in);
								id = sc.nextLine();
								inputPermittion = false;
								break;
							case 2 :
								inputPermittion = true;
								System.out.println("Please input again.");
								break;
							default :
								System.out.println("\nYour choice is invalid!!!\n");
								break;
						}
					}
				}while (true);
				break;
			case 2 :
				System.out.print("Input an id for deleting : ");
				sc = new Scanner(System.in);
				id = sc.nextLine();
				if (m.searchStudent(id)) {
					st = new Student();
					st = m.deleteStudent(id);
					System.out.println(st);
				}else {
					System.out.println("This id does not exist!!!");
				}
				break;
			case 3 :
				System.out.print("Input an id for editing : ");
				sc = new Scanner(System.in);
				id = sc.nextLine();
				if (m.searchStudent(id)) {
					System.out.print("Input name for the student : ");
					sc = new Scanner(System.in);
					name = sc.nextLine();
					/*System.out.print("Input id for the student : ");
					sc = new Scanner(System.in);
					id = sc.nextLine();*/
					System.out.print("Input date for student : ");
					sc = new Scanner(System.in);
					date = sc.nextLine();
					System.out.print("Input address for student : ");
					sc = new Scanner(System.in);
					address = sc.nextLine();
					System.out.print("Input name of class for student : ");
					sc = new Scanner(System.in);
					className = sc.nextLine();
					st = new Student(id, name, date, address, className);
					m.editStudent(st, id);
				}else {
					System.out.println("This id does not exist!!!");
				}
				break;
			case 4 :
				System.out.print("Input an id for editing : ");
				sc = new Scanner(System.in);
				id = sc.nextLine();
				if (m.searchStudent(id)) {
					System.out.println("This id doese exist!!!");
				}else {
					System.out.println("This id does not exist!!!");
				}
				break;
			case 5 :
				m.displayAll();
				break;
			case 6 :
				System.out.println("Total of the number of student is : " + m.getNumberOfStudent());
				break;
			case 7 :
				m.writeStudent();
				System.out.println("\nGoodBye!!!\n");
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 7);
	}

}
