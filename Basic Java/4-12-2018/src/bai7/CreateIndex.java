package bai7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.naming.PartialResultException;

public class CreateIndex {
	private String fileName = "Poetry.txt";
	private TreeMap<String, TreeSet> index;
	private TreeSet<Integer> lineIndex;
	private String patternSlit = "[\\ \\.,':?\n]";
	
	public CreateIndex() {
		this.index = new TreeMap<>();
		this.lineIndex = new TreeSet<>();
	}
	
	public void readFile(){
		Pattern pt = Pattern.compile(patternSlit);
		int lineNum = 0;
		String lineData = "";
		String[] words = null;
		FileReader fr;
		try {
			fr = new FileReader("Poetry.txt");
			BufferedReader br = new BufferedReader(fr);
			do {
				lineData = br.readLine();
				lineNum++;
				if (lineData != null)//The last line null, must check it
				{
					System.out.println(lineData);
					words = pt.split(lineData);
					for (int i = 0; i < words.length; i++){
						if (words[i].length() > 3 && !words[i].equals("The")){
							if (!index.containsKey(words[i])){
								lineIndex = new TreeSet<>();
								lineIndex.add(lineNum);
								index.put(words[i], lineIndex);
							}else {
								lineIndex = new TreeSet<>();
								lineIndex = index.get(words[i]);
								lineIndex.add(lineNum);
								index.put(words[i], lineIndex);
							}
						}
					}
				}
			}while (lineData != null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeFile(){
		for (String key : index.keySet()){
			System.out.println(key + "				" + index.get(key));
		}
		
		FileWriter fw;
		try {
			fw = new FileWriter("Poetry_idx.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			for (String key : index.keySet()){
				bw.write(String.format("%s %20s\r\n", key, index.get(key)));
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		CreateIndex c = new CreateIndex();
		c.readFile();
		c.writeFile();
	}
}
