package bai1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyAImage {

	public static void main(String[] args) {
		String fileName1 = "C://Users//GIT//Pictures//1.jpg";
		String fileName2 = "C://Users//GIT//Pictures//1_copy.jpg";
		
		try {
			FileInputStream fis = new FileInputStream(fileName1);
			FileOutputStream fos = new FileOutputStream(fileName2);
			int c;
			
			do {
				c = fis.read();
				fos.write(c);
			}while (c != -1);
			
			fos.close();
			System.out.println("Created a copy!!!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
