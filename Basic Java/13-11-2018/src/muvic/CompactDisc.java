package muvic;

public class CompactDisc {
	private String title;
	private String artist;
	private double price;
	private String code;
	
	public CompactDisc (String title, String artist, double price, String code){
		this.title  = title;
		this.artist = artist;
		this.price = price;
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "CompactDisc Music [title=" + title + ", artist=" + artist + ", price=" + price + ", code=" + code + "]";
	}
	
	
}
