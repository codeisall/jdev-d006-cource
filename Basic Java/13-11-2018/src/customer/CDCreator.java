package customer;

import java.util.Scanner;

import muvic.CompactDisc;

public class CDCreator {
	Scanner sc = new Scanner(System.in);
	private int nextMovie;
	private int maxMovie;
	private int nextMusic;
	private int maxMusic;
	
	muvic.CompactDisc[] music = new muvic.CompactDisc[0];
	movies.CompactDisc[] movie = new movies.CompactDisc[0];
	
	public void displayAllMusic(){
		for (int i = 0; i < music.length; i++){
			System.out.println(music[i]);
		}
	}
	
	public void displayAllMovie(){
		for (int i = 0; i < movie.length; i++){
			System.out.println(movie[i]);
		}
	}
	
	public void addMusic(){
		String title;
		String artist;
		double price;
		String code;
		int numElem = music.length;
		muvic.CompactDisc[] musicAdd = new muvic.CompactDisc[numElem+1];
		for (int i = 0; i < music.length; i++){
			for (int j = i; j <= i; j++){
				musicAdd[j] = music[i];
			}
		}
		System.out.println("Music : ");
		System.out.print("Input title : ");
		sc = new Scanner(System.in);
		title = sc.nextLine();
		System.out.print("Input artist : ");
		sc = new Scanner(System.in);
		artist = sc.nextLine();
		System.out.print("Input price ; ");
		price = sc.nextDouble();
		System.out.print("Input code : ");
		sc = new Scanner(System.in);
		code = sc.nextLine();
		muvic.CompactDisc mu = new muvic.CompactDisc(title, artist, price, code);
		musicAdd[numElem] = mu;
		music = musicAdd;
	}
	
	public void addMovie(){
		String title;
		double price;
		String code;
		int numElem = movie.length;
		movies.CompactDisc[] movieAdd = new movies.CompactDisc[numElem+1];
		for (int i = 0; i < movie.length; i++){
			for (int j = i; j <= i; j++){
				movieAdd[j] = movie[i];
			}
		}
		System.out.println("Movie : ");
		System.out.print("Input title : ");
		sc = new Scanner(System.in);
		title = sc.nextLine();
		System.out.print("Input price ; ");
		price = sc.nextDouble();
		System.out.print("Input code : ");
		sc = new Scanner(System.in);
		code = sc.nextLine();
		movies.CompactDisc mo = new movies.CompactDisc(title, price, code);
		movieAdd[numElem] = mo;
		movie = movieAdd;
	}
}
