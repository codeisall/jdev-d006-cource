package customer;

import java.util.Scanner;

public class UserInterface {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		CDCreator cd = new CDCreator();
		do {
			System.out.println("1. Add Music");
			System.out.println("2. Add Movie");
			System.out.println("3. Show all Music");
			System.out.println("4. Show all Movie");
			System.out.println("5. Exit");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				cd.addMusic();
				break;
			case 2 :
				cd.addMovie();
				break;
			case 3 :
				cd.displayAllMusic();
				break;
			case 4 :
				cd.displayAllMovie();
			case 5 :
				break;
			default :
				System.out.println("Your choice is invalid!!!");
				break;
			}
		}while (choice != 5);
	}

}
