package bai1;

import java.util.Scanner;

public class StandardStudent extends Student {
	Scanner sc = new Scanner(System.in);
	private int engMark;
	
	public StandardStudent() {
		this.engMark = 0;
	}
	
	public StandardStudent(int engMark) {
		this.engMark = engMark;
	}
	
	public void inputSt(){
		super.inputSt();
		System.out.print("Input English mark : ");
		this.engMark = sc.nextInt();
	}
	
	@Override
	public String toString() {
		return super.toString() + 
		"\nEnglish mark : " + this.engMark;
	}

	@Override
	float calculatMark() {
		float total = 0;
		total += this.engMark;
		return total;
	}
	
	
}
