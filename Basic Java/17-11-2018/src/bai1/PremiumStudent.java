package bai1;

import java.util.Scanner;

public class PremiumStudent extends StandardStudent {
	Scanner sc = new Scanner(System.in);
	private int mathMark;
	
	public PremiumStudent() {
		this.mathMark = 0;
	}
	
	public PremiumStudent(int mathMark) {
		this.mathMark = mathMark;
	}
	
	public void inputSt(){
		super.inputSt();
		System.out.println("Input Math mark : ");
		this.mathMark = sc.nextInt();
	}
	
	@Override
	public String toString() {
		return super.toString() + 
		"\nMath mark : " + this.mathMark + "\n";
	}
	
	@Override
	float calculatMark() {
		float total = 0;
		total += (this.mathMark + super.calculatMark());
		return total;
	}
}
