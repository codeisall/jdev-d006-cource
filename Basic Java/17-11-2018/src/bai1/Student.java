package bai1;

import java.util.Scanner;

public abstract class Student {
	Scanner sc = new Scanner(System.in);
	private int rollNo;
	private String name;
	
	public Student(int roll, String name) {
		this.rollNo = roll;
		this.name = name;
	}
	
	public Student() {
		this.rollNo = 0;
		this.name = "";
	}
	
	public void inputSt(){
		System.out.println("Input student's information : ");
		System.out.print("Input roll number : ");
		this.rollNo = sc.nextInt();
		System.out.print("Input name : ");
		sc =  new Scanner(System.in);
		this.name = sc.nextLine();
	}
	
	@Override
	public String toString() {
		return  "Roll number : " + this.rollNo + 
				"\nName : " + this.name;
	}
	
	abstract float calculatMark();
}
