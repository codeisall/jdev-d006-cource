package bai1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		Student std = new StandardStudent();
		Student pre = new PremiumStudent();
		do {
			System.out.println("=========MANAGE STUDENT==========");
			System.out.println("1. Input Standard Student");
			System.out.println("2. Output Standard Student");
			System.out.println("3. Input Premium Student");
			System.out.println("4. Output Premium Student");
			System.out.println("5. Exit");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt() ;
			
			switch (choice){
			case 1 :
				std.inputSt();
				break;
			case 2 :
				System.out.println("Stadard Student : \n" + std);
				break;
			case 3 :
				pre.inputSt();
				break;
			case 4 :
				System.out.println("Premium Student : \n" + pre);
				break;
			case 5 :
				break;
			default :
				System.out.println("Your choice is invalid!!!");
				break;
			}
		}while (choice != 5);
	}

}
