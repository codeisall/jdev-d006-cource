package bai2;

public abstract class Point {
	int X;
	int Y;
	
	public Point() {
		this.Y = 0;
		this.X = 0;
	}
	
	public Point (int x, int y){
		this.X = x;
		this.Y = y;
	}
	
	public void move(int dX, int dY){
		this.X += dX;
		this.Y += dY;
	}
	
	public abstract void draw();
}
