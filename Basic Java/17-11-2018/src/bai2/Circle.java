package bai2;

import java.util.Random;
import java.util.Scanner;

public class Circle extends Point implements Shape{
	Scanner sc = new Scanner(System.in);
	private double radius;
	private int color;
	
	public Circle () {
		super();
		this.radius = 0;
		this.color = 0;
	}
	
	public Circle (int x, int y, double r, int color){
		super(x, y);
		this.radius = r;
		this.color = color;
	}
	
	@Override
	public void draw() {
		System.out.println("The circle has : R = " + this.radius + " and Color : " + this.color +
				"\nIt is located at point (" + super.X + ", " + super.Y + ")");
	}
	
	@Override
	public float area() {
		return (float) (this.pi*Math.pow(this.radius, 2));
	}

	public static void main(String[] args) {
		Random rd = new Random();
		Scanner sc = new Scanner(System.in);
		Circle c = new Circle();
		int choice;
		int dX, dY;
		
		do {
			System.out.println("1. Create the circle");
			System.out.println("2. Move the circle");
			System.out.println("3. Draw the circle");
			System.out.println("4. Calculate the area of the circle");
			System.out.println("5. Create a circle with the random values");
			System.out.println("6. Exit");
			System.out.print("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice){
			case 1 :
				System.out.print("Input the value of X : ");
				c.X = sc.nextInt();
				System.out.print("Input the value of Y : ");
				c.Y = sc.nextInt();
				System.out.print("Input the radius : ");
				c.radius = sc.nextDouble();
				System.out.print("Input the color : ");
				c.color = sc.nextInt();
				c = new Circle(c.X, c.Y, c.radius, c.color);
				break;
			case 2 :
				System.out.println("Input the next value of X : ");
				dX = sc.nextInt();
				System.out.println("Input the nest value of Y : ");
				dY = sc.nextInt();
				c.move(dX, dY);
				System.out.println("The current coordinate of the circle is : ");
				c.draw();
				break;
			case 3 :
				c.draw();
				break;
			case 4 :
				System.out.println("The area of the circle is : " + c.area());
				break;
			case 5 :
				c.X = rd.nextInt(50);
				c.Y = rd.nextInt(50);
				c.radius = rd.nextInt(50) + 0.1;
				c.color = rd.nextInt(50);
				c = new Circle(c.X, c.Y, c.radius, c.color);
				System.out.println("The current coordinate of the circle is : ");
				c.draw();
				break;
			case 6 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 6);
	}

	

}

