package bai2;

public interface Shape {
	static final float pi =  3.14f;
	
	float area();
}
