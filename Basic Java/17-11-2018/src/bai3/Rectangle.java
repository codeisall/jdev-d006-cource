package bai3;

public class Rectangle implements Shape{
	private float width;
	private float length;
	
	public Rectangle() {
		this.width = 0;
		this.length = 0;
	}
	
	public Rectangle (float w, float l){
		this.width = w;
		this.length = l;
	}
	
	@Override
	public float showArea() {
		return (this.length+this.width)*2;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	@Override
	public void showShape() {
		System.out.println("The rectangle has : Width = " + this.width
				+ " and Length = " + this.length);
	}
	
	
}
