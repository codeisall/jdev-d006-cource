package bai3;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ShapeCollection shape = new ShapeCollection();
		int choice;
		do {
			System.out.println("1. Add a circle");
			System.out.println("2. Add a rectangle");
			System.out.println("3. Show all shape");
			System.out.println("4. Exit");
			System.out.println("\nInput your choice here : ");
			choice = sc.nextInt();
			
			switch (choice) {
			case 1 :
				if (shape.getCount() < shape.getMax())
					shape.AddCircle();
				else System.out.println("Full!!!");
				break;
			case 2 :
				if (shape.getCount() < shape.getMax())
					shape.AddRectangle();
				else System.out.println("Full!!!");
				break;
			case 3 :
				shape.show();
				break;
			case 4 :
				break;
			default :
				System.out.println("\nYour choice is invalid!!!\n");
				break;
			}
		}while (choice != 4);
	}

}
