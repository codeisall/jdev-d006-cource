package bai3;

import java.util.Scanner;

public class ShapeCollection {
	Scanner sc = new Scanner(System.in);
	Shape[] listShape; //Polymophism
	private int count;
	private int max;
	
	public int getCount(){
		return this.count;
	}
	
	public ShapeCollection(){
		this.max = 5;
		listShape = new Shape [this.max];
		this.count = 0;
	}
	
	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void AddCircle(){
		Circle circle = new Circle();
		System.out.print("Input the radius of the cirle : ");
		circle.setRadius(sc.nextInt());
		circle = new Circle(circle.getRadius());
		listShape[count] =  circle;
		count++;
	}
	
	public void AddRectangle(){
		Rectangle rec = new Rectangle();
		System.out.println("Input the width of the rectangle : ");
		rec.setWidth(sc.nextFloat());
		System.out.println("Input the length of the rectangle : ");
		rec.setLength(sc.nextFloat());
		rec = new Rectangle(rec.getWidth(), rec.getLength());
		listShape[count] = rec;
		count++;
	}
	
	public void show(){
		/*for (Shape s : listShape){
			if (s instanceof Circle) {
				System.out.println("The circle has : Radius = " + ((Circle) s).getRadius());
			}else {
				System.out.println("The rectangle has : Width = " + ((Rectangle) s).getWidth()
						+ " and Length = " + ((Rectangle) s).getLength());
			}
		}//for nay khong da hinh
*/		
		for (Shape s : listShape){
			s.showShape();
		}//for nay laf dung polymophism
		//no tu hieu lay ham showShap cua thang nao
	}

}
