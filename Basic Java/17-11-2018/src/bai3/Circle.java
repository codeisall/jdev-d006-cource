package bai3;

import java.util.Scanner;

public class Circle implements Shape{
	Scanner sc = new Scanner(System.in);
	private int radius;
	
	
	
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Circle() {
		this.radius = 0;
	}
	
	public Circle(int r){
		this.radius = r;
	}
	
	@Override
	public float showArea() {
		return (float) (Math.pow(radius, 2) * Math.PI);
	}

	@Override
	public void showShape() {
		System.out.println("The circle has : Radius = " + this.radius + " and Area = " + (this.radius*this.radius*Math.PI));
	}
	
}
