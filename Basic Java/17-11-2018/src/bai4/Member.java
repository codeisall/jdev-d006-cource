package bai4;

import java.util.Scanner;

public class Member extends Customer {
	Scanner sc = new Scanner(System.in);
	private float rewardRate;
	
	@Override
	public double calculateWonMoney() {
		return super.getMoneybet() * this.rewardRate;
	}

	public void addCustomer() {
		super.addCustomer();
		System.out.print("Input the reward rate : ");
		this.rewardRate = sc.nextFloat();
	}
	
	public void showCustomer(){
		super.showCustomer();
		System.out.println("The reward rate : " + this.rewardRate);
	}
}
