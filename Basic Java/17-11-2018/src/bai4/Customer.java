package bai4;

import java.util.Scanner;

public abstract class Customer {
	Scanner sc = new Scanner(System.in);
	private String password;
	private String name;
	private double moneybet;
	
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoneybet() {
		return moneybet;
	}

	public void setMoneybet(double moneybet) {
		this.moneybet = moneybet;
	}

	public void input (){
		System.out.print("Input password : ");
		sc = new Scanner(System.in);
		this.password = sc.nextLine();
		System.out.print("Input your name : ");
		this.name = sc.nextLine();
		System.out.print("Input your money bet : ");
		this.moneybet = sc.nextDouble();
	}
	
	public void output(){
		System.out.println("\nPassword : " + this.password);
		System.out.println("Name : " + this.name);
		System.out.println("Money bets : " + this.moneybet);
	}
	
	public abstract double calculateWonMoney();
	
	public void addCustomer(){
/*		Member mem = new Member();
		System.out.print("Input your name : ");
		sc = new Scanner(System.in);
		mem.setName(sc.nextLine());
		System.out.print("Input your password : ");
		sc = new Scanner(System.in);
		mem.setPassword(sc.nextLine());
		System.out.print("Input your money bets : ");
		mem.setMoneybet(sc.nextDouble());*/
		System.out.print("Input your name : ");
		sc = new Scanner(System.in);
		this.name = sc.nextLine();
		System.out.print("Input your password : ");
		sc = new Scanner(System.in);
		this.password = sc.nextLine();
		System.out.print("Input your money bets : ");
		this.moneybet = sc.nextDouble();
	}
	
	public void showCustomer() {
		System.out.println("Name : " + this.name);
		System.out.println("Password : " + this.password);
		System.out.println("Money bets : " + this.moneybet);
	}
}
