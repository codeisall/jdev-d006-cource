package bai4;

import java.util.Scanner;

public class Manage {
	Scanner sc = new Scanner(System.in);
	Customer[] client;
	private int count;
	private int max;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Manage() {
		this.max = 5;
		this.count = 0;
		client = new Customer[max];
	}
	
	public void AddMember(){
		Member mem = new Member();
		mem.addCustomer();
		client[count] = mem;
		count++;
	}
	
	public void AddUnregular(){
		UnregularCustomer un = new UnregularCustomer();
		un.addCustomer();
		client[count] = un;
		count++;
	}
	
	public void showAll(){
		for (int i = 0; i < client.length; i++){
			client[i].showCustomer();
		}
	}
}
