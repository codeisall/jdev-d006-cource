import java.util.Scanner;

public class Loop_Bai4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = 0;
		System.out.println("Input a positive number to print the multiplication table : ");
		do {
			System.out.println("Enter a number here : ");
			number = sc.nextInt();
			if (number <= 0 ) System.err.println("The multiplication must contain the number which is grater than 0 and less than 10 !!!");
			if ( number >= 10) System.err.println("The multiplication must contain the number which is grater than 0 and less than 10 !!!");
		}while (number <= 0 || number >= 10);
		
		System.out.printf("The multiplication of %d is : \n", number);
		for (int i = 1; i <= 9; i++){
			System.out.printf("%d x %d = %d\n", number, i, number*i);
		}
	}
}
