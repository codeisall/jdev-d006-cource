import java.util.Scanner;

public class For_Bai1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = 0;
		int re = 1;
		System.out.print("Input a positive integer number to calculate the factorial of that number : ");
		number = sc.nextInt();
		System.out.printf("The factorial of %d is : ", number);
		for (int i = 2; i <= number; i++){
			re *= i;
		}
		System.out.println(re);
	}
}
