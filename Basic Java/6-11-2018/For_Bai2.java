import java.util.Scanner;

public class For_Bai2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = 0;
		do {
			System.out.print("Input a number here : ");
			number = sc.nextInt();
			if (number % 5 != 0) System.out.println("Your number must be able to divide by 5 !!!");
		}while (number % 5 != 0);
		System.out.print("\nYour series is : ");
		for (int i = number; i >= 5; i -= 5){
			if (i != 5)
				System.out.print(i + ", ");
			else 
				System.out.print(i);
		}
	}

}
