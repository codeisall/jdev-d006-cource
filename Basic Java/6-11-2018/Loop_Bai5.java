import java.util.Scanner;

public class Loop_Bai5 {
	Scanner sc = new Scanner(System.in);
	private final String patternPass = "abc123";
	private String inputPass = "";
	private int countInputTime = 1;
	private boolean logIn = false;
	public void checkPassWord(){
		do {
			System.out.print("Input your password to log in : ");
			inputPass = sc.nextLine();
			if (patternPass.length() != inputPass.length()) System.out.println("Your password is incorrect!!!");
			else {
				for (int i = 0; i < patternPass.length(); i++){
					if (patternPass.substring(i, i+1).equals(inputPass.substring(i, i+1))){
						logIn = true;
						break;
					}
				}
			}
			if (logIn){
				System.out.println("You loged in !!!");
				break;
			}
			countInputTime++;
		}while (countInputTime != 4);
		if (countInputTime >= 3)
			System.out.println("\nSorry, we must lock your account !!!");
	}
	
	public static void main(String[] args) {
		Loop_Bai5 b5 = new Loop_Bai5();
		b5.checkPassWord();
	}
}
