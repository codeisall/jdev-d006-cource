import java.util.Scanner;

public class Loop_Bai7 {
	Scanner sc = new Scanner(System.in);
	private int height = 0;
	private int width = 0;
	
	public void DarwFilledReactangle(){
		System.out.println("Filled Rectangle : ");
		System.out.println("Input height : ");
		this.height = sc.nextInt();
		System.out.println("Input width : ");
		this.width = sc.nextInt();
		for (int i = 0; i < this.height; i++){
			for (int j = 0; j < this.width; j++){
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public void DrawTriangle1(){
		System.out.println("\nFilled Triangle 1 : ");
		System.out.println("Input height : ");
		this.height = sc.nextInt();
		for (int i = 0; i < this.height; i++){
			for (int j = 0; j <= i; j++){
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public void DrawTriangle2(){
		System.out.println("\nFilled Triangle 2 : ");
		System.out.println("Input height : ");
		this.height = sc.nextInt();
		for (int i = this.height; i >= 1 ; i--){
			for (int j = 0; j < i; j++){
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public void DrawEmptyRec(){
		System.out.println("\nEmpty Rectangle : ");
		System.out.println("Input height : ");
		this.height = sc.nextInt();
		System.out.println("Input width : ");
		this.width = sc.nextInt();
		for (int i = 0; i < this.height; i++){
			for (int j = 0; j < this.width; j++){
				if (i == 0 || i == this.height - 1) System.out.print("*");
				else {
					if (j == 0 || j == this.width - 1) System.out.print("*");
					else System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Loop_Bai7 b7 = new Loop_Bai7();
		b7.DarwFilledReactangle();
		b7.DrawTriangle1();
		b7.DrawTriangle2();
		b7.DrawEmptyRec();
	}

}
