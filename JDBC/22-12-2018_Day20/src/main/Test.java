package main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import Handle.Ex4_Menu;
import MyConnect.MyConnect;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String userName, password;
		String nameCategory, nameCategoryUpdating;
		int idCategrory, idBook, idBookUpdating;
		String titleBook;
		float priceBook;
		int choice = 0;
		Ex4_Menu b4 = new Ex4_Menu();
		PreparedStatement pre = null;
		ResultSet result = null;
		
		if (b4.checkConnectionState()) {
			System.out.println("Input your username : ");
			/*sc = new Scanner(System.in);*/
			userName = "aaa";
			System.out.println("Input your password : ");
			/*sc = new Scanner(System.in);*/
			password = "111";
			
			if (b4.checkLogInState(userName, password)) {
				System.out.print("Logged in successfully\n\n");
				do {
					System.out.println("**************************");
					System.out.println("*   BOOKSHOP MANAGEMENT  *");
					System.out.println("* 1. Mange Book Category *");
					System.out.println("* 2. Mange Book          *");
					System.out.println("* 3. Close Program       *");
					System.out.println("**************************");
					do {
						try {
							System.out.print("\nPlease, enter your choice : ");
							sc = new Scanner(System.in);
							choice = sc.nextInt();
							break;
						} catch (Exception e) {
							System.out.println("\nYou must enter a number!!!\n");
						}
					}while (true);
					switch (choice) {
					case 1:
						do {
							System.out.println("******************************************************************");
							System.out.println("*                         CATEGORY MANAGEMENT                    *");
							System.out.println("* 1. Show all book category                                      *");
							System.out.println("* 2. Showing books and the number of book of a specific category *");
							System.out.println("* 3. Add a new category                                          *");
							System.out.println("* 4. Delete a category                                           *");
							System.out.println("* 5. Edit a category                                             *"); 
							System.out.println("* 6. Exit                                                        *");
							System.out.println("******************************************************************");
							do {
								try {
									System.out.print("\nPlease, enter your choice : ");
									sc = new Scanner(System.in);
									choice = sc.nextInt();
									break;
								} catch (Exception e) {
									System.out.println("\nYou must enter a number!!!\n");
								}
							}while (true);
							switch (choice) {
							case 1:
								b4.showAllsCategory();
								break;
							case 2 :
								System.out.print("Input a category name : ");
								sc = new Scanner(System.in);
								nameCategory = sc.nextLine();
								b4.showBookACategory(nameCategory);
								break;
							case 3 :
								System.out.print("Input category id : ");
								sc = new Scanner(System.in);
								idCategrory = sc.nextInt();
								System.out.print("Input category name : ");
								sc = new Scanner(System.in);
								nameCategory = sc.nextLine();
								if (!b4.checkExistIDCategory(idCategrory))
									b4.addANewCategory(idCategrory, nameCategory);
								else System.out.println("\nThis category id does exist!!!\n");
								break;
							case 4 :
								System.out.print("Input a category name for deleting : ");
								sc = new Scanner(System.in);
								nameCategory = sc.nextLine();
								b4.deleteCategory(nameCategory);
								break;
							case 5 :
								System.out.print("Input a category name for updating : ");
								sc = new Scanner(System.in);
								nameCategory = sc.nextLine();
								if (b4.checkExistCategory(nameCategory)) {
									System.out.print("Input category name : ");
									sc = new Scanner(System.in);
									nameCategoryUpdating = sc.nextLine();
									System.out.print("Input category id : ");
									sc = new Scanner(System.in);
									idCategrory = sc.nextInt();
									b4.editACategory(nameCategory, idCategrory, nameCategoryUpdating);
								}
								else System.out.println("\nCategory does not exist!!!\n");
								break;
							case 6 :
								System.out.println("\nCame back main page!!\n");
								break;
							default:
								System.out.println("\nYour choice is invalid!!!\n");
								break;
							}
						}while (choice != 6);
						break;
					case 2 :
						do {
							System.out.println("**********************************");
							System.out.println("*         BOOK MANAGEMENT        *");
							System.out.println("* 1. Showing all books available *");
							System.out.println("* 2. Add a new book              *");
							System.out.println("* 3. Delete a book               *");
							System.out.println("* 4. Edit a book                 *");
							System.out.println("* 5. Exit                        *");
							System.out.println("**********************************");
							do {
								try {
									System.out.print("\nPlease, enter your choice : ");
									sc = new Scanner(System.in);
									choice = sc.nextInt();
									break;
								} catch (Exception e) {
									System.out.println("\nYou must enter a number!!!\n");
								}
							}while (true);
							
							switch (choice) {
							case 1 :
								b4.showAllBooks();
								break;
							case 2 :
								System.out.print("Input book id : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								System.out.print("Input book title : ");
								sc = new Scanner(System.in);
								titleBook = sc.nextLine();
								System.out.print("Input book cost : ");
								sc = new Scanner(System.in);
								priceBook = sc.nextFloat();
								System.out.print("Input category id or category name : ");
								sc = new Scanner(System.in);
								idCategrory = sc.nextInt();
								if (!b4.checkExitIDBook(idBook)) {
									if (b4.checkExistIDCategory(idCategrory)) {
										b4.addANewBook(idCategrory, idBook, titleBook, priceBook);
									}else System.out.println("\nThis category does not exist!!!\n");
								}else {
									System.out.println("\nThis book does exist!!!\n");
								}
								break;
							case 3 :
								System.out.print("Input book id or book name for deleting : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								if (b4.checkExitIDBook(idBook)) b4.deleteABook(idBook);
								else System.out.println("\nThis book does not exist!!!\n");
								break;
							case 4 :
								System.out.print("Input book id or book name for updating : ");
								sc = new Scanner(System.in);
								idBook = sc.nextInt();
								if (b4.checkExitIDBook(idBook)) {
									System.out.print("Input book id : ");
									sc = new Scanner(System.in);
									idBookUpdating = sc.nextInt();
									System.out.print("Input book title : ");
									sc = new Scanner(System.in);
									titleBook = sc.nextLine();
									System.out.print("Input book cost : ");
									sc = new Scanner(System.in);
									priceBook = sc.nextFloat();
									System.out.print("Input category id or category name : ");
									sc = new Scanner(System.in);
									idCategrory = sc.nextInt();
									if (b4.checkExistIDCategory(idCategrory)) {
										b4.updateABook(idBookUpdating, titleBook, priceBook, idCategrory);
									}else {
										System.out.println("\nThis category does not exist!!!\n");
									}
								}else System.out.println("\nThis book does not exist!!!\n");
								break;
							case 5 :
								System.out.println("\nCame back main page!!\n");
								break;
							default :
								System.out.println("\nYour choice is invalid!!!\n");
								break;
							}
						}while (choice != 5);
						break;
					case 3 :
						System.out.println("\nGoodBye!!\n");
						break;
					default:
						System.out.println("\nYour choice is invalid!!!\n");
						break;
					}
				}while (choice != 3);
			}else System.out.print("Login failed\nUserName does not exist or Password is incorrect\n");
		}
		else System.out.println("Cannot connect to database");
	}
}
