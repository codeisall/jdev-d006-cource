package Handle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import MyConnect.MyConnect;

public class Ex4_Menu {
	private Connection cn;
	private String sqlSystax;
	private PreparedStatement ps;
	private ResultSet rs;
	private int idCategory;
	public Ex4_Menu() {
		cn = new MyConnect().getcn();
	}
	public boolean checkConnectionState() {
		if (cn != null) return true;
		return false;
	}
	
	public boolean checkLogInState(String userName, String password) {
		sqlSystax = "select * from Admin where Username = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, userName);
			rs = ps.executeQuery();
			if (rs.next())
				if (password.equalsIgnoreCase(rs.getString("Password")) && rs.getString(3).equalsIgnoreCase("1")) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void showAllsCategory() {
		sqlSystax = "select * from Category";
		try {
			ps = cn.prepareStatement(sqlSystax);
			rs = ps.executeQuery();
			int row = 1;
			System.out.printf("No%40s%40s\n", "Category", "ID");
			while (rs.next()) {
				System.out.printf("%d%40s%40d\n", row, rs.getString(2), rs.getInt(1));
				row++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int countBook(String nameCategory) {
		int nums = 0;
		//First way
		/*sqlSystax = "select * from Category, Book where Book.IDcate like Category.IDcate and Name = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, nameCategory);
			ResultSet rsCount = ps.executeQuery();
			while (rsCount.next()) {
				nums++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		
		//Second way
		sqlSystax = "select count(Book.Title) from Category, Book where Book.IDcate like Category.IDcate and Category.Name = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, nameCategory);
			ResultSet rsCount = ps.executeQuery();
			if (rsCount.next())
				nums = rsCount.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nums;
	}
	
	public boolean checkExistCategory(String nameCategory) {
		sqlSystax = "select Category.IDcate from Category where Name = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, nameCategory);
			ResultSet rsCount = ps.executeQuery();
			if (rsCount.next()) {
				this.idCategory = rsCount.getInt(1);
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean checkExistIDCategory(int idCategory) {
		sqlSystax = "select * from Category where IDcate = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idCategory);
			ResultSet rsCount = ps.executeQuery();
			if (rsCount.next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void showBookACategory(String nameCategory) {
		sqlSystax = "select Book.Title from Category, Book where Category.IDCate like Book.IDCate and Category.Name = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, nameCategory);
			rs = ps.executeQuery();
			//Operation not allowed after ResultSet closed
			if (this.checkExistCategory(nameCategory)) {
				System.out.println("Total " + nameCategory + " book is : " + this.countBook(nameCategory));

				int row = 1;
				System.out.printf("No\t%s\n", "Title");
				while (rs.next()) {
					System.out.printf("%d\t%s\n", row, rs.getString("Title"));
					row++;
				}
			}else System.out.println("\n" + nameCategory + " category does not exist!!!\n");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addANewCategory(int id, String nameCategory){
		sqlSystax = "insert into Category values(?, ?)";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(2, nameCategory);
			ps.setInt(1, id);
			if (ps.executeUpdate() != 0) System.out.println("\nAdded successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteCategory(String nameCategory) {
		Scanner sc = new Scanner(System.in);
		String deleteCheck;
		try {
			if (this.checkExistCategory(nameCategory)) {
				if (this.countBook(nameCategory) != 0) {
					System.out.println(nameCategory + " category still have " + this.countBook(nameCategory) + " book/books");
					System.out.print("Do you still want delete this category? (Y/N) : ");
					sc = new Scanner(System.in);
					deleteCheck = sc.nextLine();
					if (deleteCheck.equalsIgnoreCase("Y")) {
						sqlSystax = "select Book.IDbook from Category, Book where Book.IDcate like Category.IDcate and Category.Name = ?";
						ps = cn.prepareStatement(sqlSystax);
						ps.setString(1, nameCategory);
						rs = ps.executeQuery();
						while (rs.next()){
							this.deleteABook(rs.getInt("IDbook"));
						}
						sqlSystax = "delete from Category where Name = ?";
						ps = cn.prepareStatement(sqlSystax);
						ps.setString(1, nameCategory);
						if (ps.executeUpdate() != 0) System.out.println("\nDeleted Successfully\n");
					}else return;
				}else {
					sqlSystax = "delete from Category where Name = ?";
					ps = cn.prepareStatement(sqlSystax);
					ps.setString(1, nameCategory);
					if (ps.executeUpdate() != 0) System.out.println("\nDeleted Successfully\n");
				}
			}else System.out.println("\n" + nameCategory + " category does not exist!!!\n");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void editACategory(String nameCategory, int idCategoryEdit, String nameCategoryEdit) {
		sqlSystax = "update Category set Category.IDcate = ?, Category.Name = ? where Category.Name = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idCategoryEdit);
			ps.setString(2, nameCategoryEdit);
			ps.setString(3, nameCategory);
			if (ps.executeUpdate() != 0) System.out.println("\nUpdated Successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public boolean checkExitIDBook(int idBook) {
		sqlSystax = "select Book.IDbook from Book where IDbook = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idBook);
			rs = ps.executeQuery();
			if (rs.next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void showAllBooks() {
		sqlSystax = "select Category.Name from Category";
		PreparedStatement psShowBook;
		ResultSet rsShowBook;
		String nameCate;
		try {
			ps = cn.prepareStatement(sqlSystax);
			rs = ps.executeQuery();
			while (rs.next()) {
				nameCate = rs.getString("Name");
				sqlSystax = "select Book.IDBook, Book.Title, Book.Price from Category, Book where Category.IDcate like Book.IDcate"
						+ " and Category.Name = ?";
				psShowBook = cn.prepareStatement(sqlSystax);
				psShowBook.setString(1, rs.getString("Name"));
				rsShowBook = psShowBook.executeQuery();
				System.out.println("Category : " + rs.getString("Name"));
				while(rsShowBook.next())
					System.out.println("\t" + rsShowBook.getInt("IDBook") + " - "
							+ rsShowBook.getString("Title") + " - "
							+ rsShowBook.getFloat("Price"));
			}
			/*while(rs.next()) {
				System.out.println("Category : " + rs.getString("Name"));
				System.out.println("\t" + rs.getInt("IDBook") + " - "
						+ rs.getString("Title") + " - "
						+ rs.getFloat("Price"));
			}*/
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addANewBook(int idCate, int idBook, String title, float price) {
		sqlSystax = "insert into Book values(?, ?, ?, ?)";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idBook);
			ps.setString(2, title);
			ps.setFloat(3, price);
			ps.setInt(4, idCate);
			if (ps.executeUpdate() != 0) System.out.println("\nBook inserted successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteABook(int idBook) {
		sqlSystax = "delete from Book where IDbook = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idBook);
			if (ps.executeUpdate() != 0) System.out.println("\nBook deleted successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void updateABook(int idBook, String title, float price, int idCate) {
		sqlSystax = "update Book set IDbook = ?, Title = ? , Price = ? , IDcate = ? where IDbook = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setInt(1, idBook);
			ps.setString(2, title);
			ps.setFloat(3, price);
			ps.setInt(4, idCate);
			ps.setInt(5, idBook);
			if (ps.executeUpdate() != 0) System.out.println("\nBook updated successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		/*sqlSystax = "update Book set Title = ? , Price = ? ,IDcate = ? where IDbook = ?";
		try {
			ps = cn.prepareStatement(sqlSystax);
			ps.setString(1, title);
			ps.setFloat(2, price);
			ps.setInt(3, idCate);
			ps.setInt(4, idBook);
			if (ps.executeUpdate() != 0) System.out.println("\nBook updated successfully\n");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}*/
	}
	
	public int getIDCate() {
		return this.idCategory;
	}
}
