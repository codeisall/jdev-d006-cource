package bai1_47;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

public class TakeData {
	
	public void takeStandardFee(Connection cn, ArrayList<String> standard, ArrayList<String> fee) {
		String sql = "select * from Standards";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			
			while (rs.next()) {
				standard.add(rs.getString(1));
				fee.add(String.valueOf(rs.getInt(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void takeStudent(Connection cn, ArrayList<String> student) {
		String sql = "select * from Student";
		int count = 0;
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			
			while (rs.next()) {
				for (int i = 0; i < 7; i++)
					student.add(rs.getString(i+1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String takeSpecificFee(Connection cn, String standard) {
		String fee = "";
		String sql = "select Standards.Fee from Student, Standards "
				+ "where Student.Standard like Standards.Standard "
				+ "and Standards.Standard like ?";
		PreparedStatement pt;
		try {
			pt = cn.prepareStatement(sql);
			pt.setString(1, standard);
			ResultSet rs = pt.executeQuery();
			if (rs.next()) fee = String.valueOf(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fee;
	}
}
