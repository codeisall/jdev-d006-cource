package bai2_61;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import Myconnect.MyConnect;
import bai1_47.CheckInput;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ButtonGroup;

public class bai2_67_fixed extends JFrame {

	private JPanel contentPane;
	private JTextField txtFullName;
	private JTextField txtContact;
	private JTextField txtUserName;
	private JPasswordField txtPass;
	private JTable table;
	
	//Extra variables
	private int rowClicked;
	private CheckInput check;
	private String passStr;
	private Connection cn;
	private String role;
	private int lock;
	private boolean rightClicked;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					/*bai2_67_fixed frame = new bai2_67_fixed();
					frame.setVisible(true);*/
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai2_67_fixed() {
		MyConnect my = new MyConnect();
		my.setName("accountdeatil");
		this.check = new CheckInput();
		this.cn = my.getcn();
		this.rightClicked = false;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 721, 454);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(368, 11, 326, 251);
		contentPane.add(panel);
		
		JLabel label = new JLabel("Account Detail");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 30));
		label.setBounds(10, 0, 306, 37);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Full Name");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_1.setBounds(10, 56, 68, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Contact");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_2.setBounds(10, 109, 68, 14);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("User Name");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setBounds(10, 162, 68, 14);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("Password");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_4.setBounds(10, 215, 68, 14);
		panel.add(label_4);
		
		txtFullName = new JTextField();
		txtFullName.setColumns(10);
		txtFullName.setBounds(88, 53, 175, 20);
		panel.add(txtFullName);
		
		txtContact = new JTextField();
		txtContact.setColumns(10);
		txtContact.setBounds(88, 106, 175, 20);
		panel.add(txtContact);
		
		txtUserName = new JTextField();
		txtUserName.setColumns(10);
		txtUserName.setBounds(88, 159, 175, 20);
		panel.add(txtUserName);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(88, 212, 175, 20);
		panel.add(txtPass);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Role", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(368, 273, 136, 115);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JRadioButton rdCall = new JRadioButton("Employees Call");
		buttonGroup.add(rdCall);
		panel_1.add(rdCall);
		
		JRadioButton rdSale = new JRadioButton("Employees Sale");
		buttonGroup.add(rdSale);
		panel_1.add(rdSale);
		
		JRadioButton rdAdmin = new JRadioButton("Admin");
		buttonGroup.add(rdAdmin);
		panel_1.add(rdAdmin);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(512, 273, 182, 115);
		contentPane.add(panel_2);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setBounds(49, 11, 83, 23);
		panel_2.add(btnCreate);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(49, 45, 83, 23);
		panel_2.add(btnReset);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(49, 79, 83, 23);
		panel_2.add(btnClose);
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBounds(10, 273, 328, 108);
		contentPane.add(panel_3);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBounds(38, 42, 77, 23);
		panel_3.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(125, 42, 77, 23);
		panel_3.add(btnDelete);
		
		JButton btnLock = new JButton("Lock");
		btnLock.setBounds(212, 42, 77, 23);
		panel_3.add(btnLock);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 326, 251);
		contentPane.add(scrollPane);
		
		//Colums Names
		DefaultTableModel tableModel = new DefaultTableModel();
		Object[] names = {"Fullname", "Username", "Role", "Lock"};
		tableModel.setColumnIdentifiers(names);
		Object[] tableData = new Object[4];
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(tableModel);
		
		//Load data from database
		String sql = "select * from account";
		PreparedStatement pt;
		try {
			pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				tableData[0] = rs.getString(1);
				tableData[1] = rs.getString(3);
				tableData[2] = rs.getString(5);
				tableData[3] = rs.getInt(6);
				tableModel.addRow(tableData);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//Reset button
		btnReset.addActionListener(event -> {
			txtFullName.setText("");
			txtContact.setText("");
			txtUserName.setText("");
			txtPass.setText("");
		});
		
		//Close button
		btnClose.addActionListener(event -> {
			System.exit(DISPOSE_ON_CLOSE);
		});
		
		//Table clicked
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				rightClicked = true;
				rowClicked = table.getSelectedRow();
				txtFullName.setText((String) tableModel.getValueAt(rowClicked, 0));
				txtContact.setText(getSpecificContact((String) tableModel.getValueAt(rowClicked, 1)));
				txtUserName.setText((String) tableModel.getValueAt(rowClicked, 1));
				lock = (int) tableModel.getValueAt(rowClicked, 3);
				//Show the role
				if (tableModel.getValueAt(rowClicked, 2).equals("call")) {
					rdCall.setSelected(true);
				}else if (tableModel.getValueAt(rowClicked, 2).equals("sale")) {
					rdSale.setSelected(true);
				}else {
					rdAdmin.setSelected(true);
				}
				
				/*if (rightClicked) {*/
					if (tableModel.getValueAt(rowClicked, 3).equals(1)) {
						btnLock.setText("Lock");
						lock = 0;
					}else {
						btnLock.setText("Unlock");
						lock = 1;
					}
				/*}else btnLock.setText("Lock");*/
			}
		});
		
		//Role radiobutton
		rdSale.addActionListener(event -> {
			role = "sale";
		});
		rdCall.addActionListener(event -> {
			role = "call";
		});
		rdAdmin.addActionListener(event -> {
			role = "admin";
		});
		
		//Create button
		btnCreate.addActionListener(event -> {
			passStr = new String(txtPass.getPassword());
			if (rdAdmin.isSelected()) role = "admin";
			else if (rdCall.isSelected()) role = "call";
			else role = "sale";
			if (rdSale.isSelected() || rdAdmin.isSelected() || rdCall.isSelected()) {
				if (this.checkCreate(txtFullName.getText(), txtContact.getText(), txtUserName.getText(), passStr)) {
					this.create(txtFullName.getText(), txtContact.getText(), txtUserName.getText(), passStr, role, 1);
					tableData[0] = txtFullName.getText();
					tableData[1] = txtUserName.getText();
					tableData[2] = role;
					tableData[3] = 1;
					tableModel.addRow(tableData);
				}
			}else {
				JOptionPane.showMessageDialog(getParent(), "Role is not defined!!!");
			}
		});
		
		//Delete button
		btnDelete.addActionListener(event -> {
			if (this.rightClicked) {
				String userNameDelete = (String) tableModel.getValueAt(rowClicked, 1);
				this.delete(userNameDelete);
				tableModel.removeRow(rowClicked);
			}
			this.rightClicked = false;
		});
		
		//Update button
		btnUpdate.addActionListener(event -> {
			if (this.rightClicked) {
				String fullName = txtFullName.getText();
				String contact = txtContact.getText();
				String userName = (String) tableModel.getValueAt(rowClicked, 1);
				passStr = new String(txtPass.getPassword());
				lock = (int) tableModel.getValueAt(rowClicked, 3);
				if (rdAdmin.isSelected()) role = "admin";
				else if (rdCall.isSelected()) role = "call";
				else role = "sale";
				if (check.checkEmpty(fullName)) {
					if (check.checkEmpty(contact)) {
						if (check.checkPhone(contact)) {
							if (check.checkEmpty(userName)) {
									if (check.checkEmpty(passStr)) {
										this.update(fullName, contact, userName, "", role, lock);
										tableModel.setValueAt(fullName, rowClicked, 0);
										tableModel.setValueAt(userName, rowClicked, 1);
										tableModel.setValueAt(role, rowClicked, 2);
										tableModel.setValueAt(lock, rowClicked, 3);
									}else 
										JOptionPane.showMessageDialog(getParent(), "Pass have not been filled yet!!!");
							}else		
								JOptionPane.showMessageDialog(getParent(), "User Name have not been filled yet!!!");
						}else 
							JOptionPane.showMessageDialog(getParent(), "Contact is invalid!!!");
					}else 
						JOptionPane.showMessageDialog(getParent(), "Contact have not been filled yet!!!");
				}else 
					JOptionPane.showMessageDialog(getParent(), "Full Name have not been filled yet!!!");
			}
			this.rightClicked = false;
		});
		
		//Lock button
		btnLock.addActionListener(event -> {
			if (this.rightClicked) {
				tableModel.setValueAt(lock, rowClicked, 3);
				this.update((String) tableModel.getValueAt(rowClicked, 0), 
							this.getSpecificContact((String) tableModel.getValueAt(rowClicked, 1)), 
							(String) tableModel.getValueAt(rowClicked, 1), 
							"", (String) tableModel.getValueAt(rowClicked, 2), lock);
				if (lock == 1) {
					btnLock.setText("Lock");
				}else {
					btnLock.setText("Unlock");
				}
			}
		});
	}
	
	public boolean checkCreate(String fullName, String contact, String userName, String pass) {
		if (check.checkEmpty(fullName)) {
			if (check.checkEmpty(contact)) {
				if (check.checkPhone(contact)) {
					if (check.checkEmpty(userName)) {
						if (!this.checkExist(userName)) {
							if (check.checkEmpty(pass)) {
								return true;
							}else 
								JOptionPane.showMessageDialog(getParent(), "Pass have not been filled yet!!!");
						}else
							JOptionPane.showMessageDialog(getParent(), "User Name does exist!!!");
					}else 
						JOptionPane.showMessageDialog(getParent(), "User Name have not been filled yet!!!");
				}else 
					JOptionPane.showMessageDialog(getParent(), "Contact is invalid!!!");
			}else 
				JOptionPane.showMessageDialog(getParent(), "Contact have not been filled yet!!!");
		}else 
			JOptionPane.showMessageDialog(getParent(), "Full Name have not been filled yet!!!");
		return false;
	}
	
	public void create(String fullName, String contact, String userName, String pass, String role, int lock) {
		String sql = "insert into account "
				+ "values(?, ?, ?, ?, ?, ?)";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] bytePass = md.digest(pass.getBytes());
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, fullName);
			pt.setString(2, contact);
			pt.setString(3, userName);
			pt.setString(4, new String(bytePass));
			pt.setString(5, role);
			pt.setInt(6, lock);
			pt.executeUpdate();
			JOptionPane.showMessageDialog(getParent(), "Create successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public String getSpecificContact(String userName) {
		String contact = "";
		String sql = "select contact from account "
				+ "where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			ResultSet rs =pt.executeQuery();
			if (rs.next()) contact = rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return contact;
	}
	
	public boolean checkExist(String userName) {
		String sql = "select * from account where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			if (pt.executeQuery().next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void delete(String userName) {
		String sql = "delete from account where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(String fullName, String contact, String userName, String pass, String role, int lock) {
		String sql = "update account "
				+ "set fullname = ?, "
				+ "contact = ?, "
				/*+ "username = ?, "*/
				+ "password = ?, "
				+ "role = ?, "
				+ "_lock = ? "
				+ "where username like ?";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] bytePass = md.digest(pass.getBytes());
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, fullName);
			pt.setString(2, contact);
			/*pt.setString(3, userName);*/
			pt.setString(3, new String(bytePass));
			pt.setString(4, role);
			pt.setInt(5, lock);
			pt.setString(6, userName);
			pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
