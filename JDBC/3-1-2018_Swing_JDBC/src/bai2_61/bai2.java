package bai2_61;

import java.awt.EventQueue;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.Connection;

import Myconnect.MyConnect;
import bai1_47.CheckInput;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class bai2 extends JFrame {

	private JPanel contentPane;
	private JTextField txtUserName;
	private JPasswordField passField;
	private CheckInput check;
	private Connection cn;
	private MessageDigest md;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bai2 frame = new bai2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public bai2() {
		this.check = new CheckInput();
		this.cn = new MyConnect().getcn();
		try {
			this.md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 371, 229);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JLabel lbUserName = new JLabel("User Name");
		lbUserName.setHorizontalAlignment(SwingConstants.CENTER);
		lbUserName.setBounds(10, 54, 85, 14);
		contentPane.add(lbUserName);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(10, 122, 85, 14);
		contentPane.add(lblNewLabel_1);
		
		txtUserName = new JTextField();
		txtUserName.setBounds(105, 51, 214, 20);
		contentPane.add(txtUserName);
		txtUserName.setColumns(10);
		
		passField = new JPasswordField();
		passField.setBounds(105, 119, 214, 20);
		contentPane.add(passField);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(20, 157, 89, 23);
		contentPane.add(btnRegister);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(230, 157, 89, 23);
		contentPane.add(btnLogin);
		
		//Register
		btnRegister.addActionListener(event -> {
			if (check.checkEmpty(txtUserName.getText())){
				if (!this.checkExist(txtUserName.getText())){
					if (check.checkEmpty(new String(passField.getPassword()))){
						byte[] bytePass = md.digest(new String(passField.getPassword()).getBytes());
						//ma hoa no can nhan 1 chuoi byte nen phai chuyen pass thanh chuoi byte (new String(passField.getPassword()).getBytes())
						this.insert(txtUserName.getText(), new String(bytePass));
					}else{
						JOptionPane.showMessageDialog(getParent(), "You have not filled user name yet!!!");
						txtUserName.requestFocus();
					}
				}else {
					JOptionPane.showMessageDialog(getParent(), "This User Name does exist!!!");
					txtUserName.requestFocus();
				}	
			}else {
				JOptionPane.showMessageDialog(getParent(), "You have not filled user name yet!!!");
				txtUserName.requestFocus();
			}
		});
		
		//Login
		btnLogin.addActionListener(event -> {
			if (this.checkExist(txtUserName.getText())){
				if (this.checkLogin(new String(passField.getPassword()), txtUserName.getText())){
					setVisible(false);
					/*bai2_Hello b2 = new bai2_Hello(txtUserName.getText());*/
					bai2_67_fixed b2 = new bai2_67_fixed();
					b2.setVisible(true);
				}else 
					JOptionPane.showMessageDialog(getParent(), "Login failed!!!");
			}else
				JOptionPane.showMessageDialog(getParent(), "Login failed!!!");
		});
	}
	
	public boolean checkExist(String userName){
		String sql = "select username from admin where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			ResultSet rs = pt.executeQuery();
			if (rs.next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void insert(String userName, String pass){
		String sql = "insert into admin "
				+ "values(?, ?) ";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			pt.setString(2, pass);
			pt.executeUpdate();
			JOptionPane.showMessageDialog(getParent(), "Register successfully!!!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkLogin(String pass, String userName){
		String sql = "select * from admin where username like ?";
		byte[] bytePass = md.digest(pass.getBytes());
		//Vif pass dang ky da bi ma hoa
		//nen trc khi login phia ma hoa
		//xem giong voi cai da luu khong
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, userName);
			ResultSet rs = pt.executeQuery();
			if (rs.next()) {
				if (rs.getString(2).equals(new String(bytePass)))
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
