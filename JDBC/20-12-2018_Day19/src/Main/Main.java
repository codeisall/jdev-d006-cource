package Main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import Myconnect.MyConnect;

public class Main {

	public static void main(String[] args) {
		Connection cn = new MyConnect().getcn();
		Scanner sc = new Scanner(System.in);
		String userName = null;
		String password = null;
		String sql;
		PreparedStatement ps;
		ResultSet rs = null;
		if(cn != null) {
			System.out.println("Connected");
			System.out.print("Input your user name : ");
			sc = new Scanner(System.in);
			userName = "aaa";
			System.out.print("Input your password : ");
			sc = new Scanner(System.in);
			password = "111";
			sql = "select * from ADMIN where Username = ?";
			try {
				ps = cn.prepareStatement(sql);
				ps.setString(1,  userName);
				rs = ps.executeQuery();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs.next()){
					if (password.equalsIgnoreCase(rs.getString("Password"))) System.out.println("Success");
				}else {
					System.out.println("Your password is incorrect !!!!!\nCannot log in");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else System.out.println("Cannot connect");
		
		System.out.println("\nDisplay Customer");
		
		sql = "select * from ADMIN";
		try {
			ps = cn.prepareStatement(sql);
			rs = ps.executeQuery();
			System.out.println("ID\tUsername\tPassWord\tEmail\tRole");
			while (rs.next()){
					System.out.print(rs.getInt(1)+"\t");
					System.out.print(rs.getString(2)+"\t");
					System.out.print(rs.getString(3)+"\t");
					System.out.print(rs.getString(4)+"\t");
					System.out.println(rs.getInt(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nAdd a new customer");
		int id = 4;
		String name = "ddd";
		String pass = "444";
		String email = "ddd@gmail.com";
		String role = "4";
		sql = "insert into ADMIN values(?, ?, ?, ?, ?)";
		try {
			ps = cn.prepareStatement(sql);
			
			//The gia tri
			ps.setInt(1, id);
			ps.setString(2,  name);
			ps.setString(3, pass);
			ps.setString(4, email);
			ps.setString(5, role);
			
			//Kiem tra update thanh cong khong
			if (ps.executeUpdate()!= 0) {
				System.out.println("Inserted");
				System.out.println("\nDisplay Customer");
				
				sql = "select * from ADMIN";
				try {
					ps = cn.prepareStatement(sql);
					rs = ps.executeQuery();
					System.out.println("ID\tUsername\tPassWord\tEmail\tRole");
					while (rs.next()){
							System.out.print(rs.getInt(1)+"\t");
							System.out.print(rs.getString(2)+"\t");
							System.out.print(rs.getString(3)+"\t");
							System.out.print(rs.getString(4)+"\t");
							System.out.println(rs.getInt(5));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}else{
				System.out.println("Cannot insert");
			}
		} catch (SQLException e) {
			System.out.println("Duplicate entry '4' for key 'PRIMARY'");
		}
		
		System.out.println("\nUpdate an avilable data");
		try {
			ps = cn.prepareStatement("update ADMIN set ADMIN.Username = ? where ADMIN.IDCus = ?");
			ps.setString(1, "Updated");
			ps.setInt(2, 1);
			if (ps.executeUpdate()!= 0) {
				System.out.println("Updated");
				System.out.println("\nDisplay Customer");
				
				sql = "select * from ADMIN";
				try {
					ps = cn.prepareStatement(sql);
					rs = ps.executeQuery();
					System.out.println("ID\tUsername\tPassWord\tEmail\tRole");
					while (rs.next()){
							System.out.print(rs.getInt(1)+"\t");
							System.out.print(rs.getString(2)+"\t");
							System.out.print(rs.getString(3)+"\t");
							System.out.print(rs.getString(4)+"\t");
							System.out.println(rs.getInt(5));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}else System.out.println("Cannot update");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nDelete a customer");
		try {
			ps = cn.prepareStatement("delete from ADMIN where ADMIN.IDCus = ?");
			ps.setInt(1, 1);
			if (ps.executeUpdate()!= 0) {
				System.out.println("Deleted");
				System.out.println("\nDisplay Customer");
				
				sql = "select * from ADMIN";
				try {
					ps = cn.prepareStatement(sql);
					rs = ps.executeQuery();
					System.out.println("ID\tUsername\tPassWord\tEmail\tRole");
					while (rs.next()){
							System.out.print(rs.getInt(1)+"\t");
							System.out.print(rs.getString(2)+"\t");
							System.out.print(rs.getString(3)+"\t");
							System.out.print(rs.getString(4)+"\t");
							System.out.println(rs.getInt(5));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}else System.out.println("Cannot delete");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nSearch a customer");
		try {
			ps = cn.prepareStatement("select * from ADMIN where ADMIN.IDCus = ?");
			ps.setInt(1, 2);
			rs = ps.executeQuery();
			if (rs.next()){
				System.out.println("This customer exist!!!");
			}else System.out.println("This customer does not exist!!!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
