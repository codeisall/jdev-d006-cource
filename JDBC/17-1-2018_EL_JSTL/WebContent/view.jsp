<%@page import="model.ProductModel"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Product</title>
</head>
<body>
	<h1 align="center">List Product</h1>
	<form action="manageShopping?task=add" method="post">
		<table border="2px" align="center">
			<tr>
				<th>Category Code</th>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Cost</th>
				<th>Image</th>
				<th>Action</th>
				<th>Action</th>
				<th>Action</th>
			</tr>
			<%ProductModel proModel = new ProductModel();%>
			<c:forEach  var="pro" items="<%=proModel.getListProduct()%>">
				<tr>
					<td align="center">${pro.cateCode}</td>
					<td align="center">${pro.productCode}</td>
					<td align="center">${pro.productName}</td>
					<td align="center">${pro.productCost}</td>
					<td align="center"><img alt="Phone" src="Images/${pro.image}" height="150px" width="120px"></td>
					<td><button type="submit" name="btnAdd" value="${pro.productCode}">Add cart</button></td>
					<td><button type="submit" name="btnUpdate" value="${pro.productCode}">Update</button></td>
					<td><button type="submit" name="btnTest" value="${pro.cateCode}">Test</button></td>
				</tr>
			</c:forEach>
		</table>
		<c:if test="${requestScope.numProduct > 0}">
			<p align="center"><button style="color: red;" type="submit" name="btnShowCart" value="viewcart.jsp">Your cart</button></p>
		</c:if>
	</form>
</body>
</html>