<%@page import="entities.Category"%>
<%@page import="model.CategoryModel"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<%ArrayList<Category> listCate = new CategoryModel().getListCategory();%>
		<select name="listCategory">
		<c:forEach var="cate" items="<%=listCate%>">
			<option value="${cate.cateCode}" ${cate.cateName == requestScope.selectedCateName ? 'selected': ''}>${cate.cateName}</option>
		</c:forEach>
		</select>
</body>
</html>