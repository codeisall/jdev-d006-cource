<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="entities.Category"%>
<%@page import="model.CategoryModel"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert</title>
<script type="text/javascript">
	//Bắt sự kiện ấn vào combobox
	function changeFunc() {
    	var selectBox = document.getElementById("selectBox");
    	var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    	//set giá trị cho category Code cho thẻ input
    	var category = document.getElementById("category");
    	category.value = selectedValue;
   	}
	
	/* function btnCate() {
		document.getElementById("cate").value = "category";
	}
	function btnPro() {
		document.getElementById("pro").value = "product";
	} */
</script>
</head>
<body>
	<form action="ProductServlet?action=insert" method="post" enctype="multipart/form-data">
		<h1 align="center">Insert Product</h1>

		<table align="center">

			<tr>
				<th align="center">Product Code</th>
				<td align="center"><input type="text" name="txtProductCode"></td>
			</tr>
			<tr>
				<th align="center">Product Name</th>
				<td align="center"><input type="text" name="txtProductName"></td>
			</tr>
			<tr>
				<th align="center">Product Cost</th>
				<td align="center"><input type="text" name="txtProductCost"></td>
			</tr>
			<tr>
				<th align="center">Caterogy Name</th>
				<td align="center">
				<%ArrayList<Category> listCate = new CategoryModel().getListCategory();%>
				<select name="listCategory" id="selectBox" onchange="changeFunc();">
					<option value="" selected="selected"></option>
					<c:forEach var="cate" items="<%=listCate%>">
						<option value="${cate.cateCode}">${cate.cateName}</option>
					</c:forEach>
				</select>
				</td>
				<c:set var="selectedCate" value=""></c:set>
			</tr>
			<tr>
				<th align="center">Image</th>
				<td align="center"><input type="file" name="fileImage"></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit" name="btnProduct" value="Insert"></td>
				<!-- <td align="center" colspan="2"><button id="pro" type="submit" name="insertPro" onclick="btnPro()">Insert</button></td> -->
			</tr>
		</table>
		
		<h1 align="center">Insert Cate</h1>
		<table align="center">
			<tr>
				<th align="center">Category Code</th>
				<td align="center"><input type="text" name="txtInsertCateCode"></td>
			</tr>
			<tr>
				<th align="center">Category Name</th>
				<td align="center"><input type="text" name="txtCateName"></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit"  name="btnCategory" value="Insert"></td>
				<!-- <td align="center" colspan="2"><button id="cate" type="submit" name="insertCate" onclick="btnCate()">Insert</button></td> -->
			</tr>
		</table>
	</form>
</body>
</html>