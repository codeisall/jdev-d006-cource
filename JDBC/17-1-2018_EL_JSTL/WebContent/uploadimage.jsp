<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="model.CategoryModel"%>
<%@page import="entities.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Upload Image</title>
</head>
<body>
	<form action="ProductServlet?action=upload" method="post" enctype="multipart/form-data">
	<%ArrayList<Category> listCate = new CategoryModel().getListCategory();%>
		<select name="listCategory">
			<c:forEach var="cate" items="<%=listCate%>">
				<option value="${cate.cateCode}">${cate.cateName}</option>
			</c:forEach>
		</select>
		
		<input type="file" name="file">
		<input type="submit">
	</form>
</body>
</html>