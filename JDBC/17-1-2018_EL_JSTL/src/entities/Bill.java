package entities;

public class Bill {
	private int billCode;
	private String billDate;
	
	public Bill(int billCode, String billDate) {
		this.billCode = billCode;
		this.billDate = billDate;
	}
	
	public Bill() {
		this.billCode = 0;
		this.billDate = "";
	}

	public int getBillCode() {
		return billCode;
	}

	public void setBillCode(int billCode) {
		this.billCode = billCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
}
