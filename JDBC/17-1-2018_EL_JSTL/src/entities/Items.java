package entities;

public class Items {
	private int nums;
	private Product pro;
	
	public Items(int nums, Product pro) {
		this.nums = nums;
		this.pro = pro;
	}
	
	public Items() {
		super();
		this.nums = 0;
		this.pro = null;
	}

	public int getNums() {
		return nums;
	}

	public void setNums(int nums) {
		this.nums = nums;
	}

	public Product getPro() {
		return pro;
	}

	public void setPro(Product pro) {
		this.pro = pro;
	}
}
