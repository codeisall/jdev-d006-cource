package entities;

public class Category {
	private String cateCode;
	private String cateName;
	
	public Category(String cateCode, String cateName) {
		this.cateCode = cateCode;
		this.cateName = cateName;
	}
	
	public Category() {
		this.cateCode = "";
		this.cateName = "";
	}

	public String getCateCode() {
		return cateCode;
	}

	public void setCateCode(String cateCode) {
		this.cateCode = cateCode;
	}

	public String getCateName() {
		return cateName;
	}

	public void setCateName(String cateName) {
		this.cateName = cateName;
	}
	
	
}
