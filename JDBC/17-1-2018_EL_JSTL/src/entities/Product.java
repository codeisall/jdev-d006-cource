package entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import MyConnect.MyConnect;

public class Product {
	private String productCode;
	private String productName;
	private int productCost;
	private String image;
	private String cateCode;
	
	public Product(String productCode, String productName, int productCost, String image, String cateCode) {
		this.productCode = productCode;
		this.productName = productName;
		this.productCost = productCost;
		this.image = image;
		this.cateCode = cateCode;
	}

	public Product() {
		this.productCode = "";
		this.productName = "";
		this.productCost = 0;
		this.image = "";
		this.cateCode = "";
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductCost() {
		return productCost;
	}

	public void setProductCost(int productCost) {
		this.productCost = productCost;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCateCode() {
		return cateCode;
	}

	public void setCateCode(String cateCode) {
		this.cateCode = cateCode;
	}
}
