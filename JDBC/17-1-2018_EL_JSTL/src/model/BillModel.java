package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import MyConnect.MyConnect;
import entities.Bill;

public class BillModel {
	private Bill bill;

	public BillModel(Bill bill) {
		this.bill = bill;
	}
	
	public BillModel() {
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
	public int insert() {
		Connection cn = new MyConnect().getcn();
		String sql = "insert into bill "
				+ "values(null, ?)";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, bill.getBillDate());
			return pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getNewestBill() {
		int maxBillCode = 0;
		Bill newestBill = new Bill();
		Connection cn = new MyConnect().getcn();
		String sql = "select Max(billcode) from bill";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			if (rs.next())
				maxBillCode = rs.getInt(1);
			pt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return maxBillCode;
	}
}
