package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import MyConnect.MyConnect;
import entities.Category;

public class CategoryModel {
	Category cate;

	public CategoryModel(Category cate) {
		this.cate = cate;
	}
	
	public CategoryModel() {
	}
	
	public ArrayList<Category> getListCategory(){
		ArrayList<Category> listCate = new ArrayList<>();
		String sql = "select * from category";
		Connection cn = new MyConnect().getcn();
		
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				listCate.add(new Category(rs.getString(1), rs.getString(2)));
			}
			pt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listCate;
	}
	
	public int insert(){
		int result = 0;
		String sql = "insert into category "
				+ "values(?, ?)";
		Connection cn = new MyConnect().getcn();
		
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, cate.getCateCode());
			pt.setString(2, cate.getCateName());
			result = pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
