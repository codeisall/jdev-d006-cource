package cotroller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.mysql.jdbc.Connection;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import MyConnect.MyConnect;
import entities.Category;
import entities.Product;
import model.CategoryModel;
import model.ProductModel;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String page = "";
		String error = "";

		if (action.equals("upload")) {
			Part file = request.getPart("file");
			ProductModel proModel = new ProductModel(file);
			String uploadRootPath = request.getServletContext().getRealPath("Images");
			proModel.uploadFile(uploadRootPath);
			page = "uploadimage.jsp";
		}else if (action.equals("update")) {
			String image = "";
			
			Product pro = new Product(request.getParameter("txtProductCode"),
									  request.getParameter("txtProductName"),
									  Integer.parseInt(request.getParameter("txtProductCost")),
									  image,
									  request.getParameter("txtCategoryCode"));
			ProductModel productModel = new ProductModel(pro);
			if (productModel.update() != 0)
				page = "view.jsp";
			else
				page = "updateproduct.jsp";
		}else if (action.equals("insert")){
			if (request.getParameter("btnCategory") != null) {
				Category cate = new Category(request.getParameter("txtInsertCateCode"), request.getParameter("txtCateName"));
				CategoryModel cateModel = new CategoryModel(cate);
				if (cateModel.insert() != 0) {
					page = "insertproduct.jsp";
				}else {
					page = "error.jsp";
					error = "Category cannot be inserted!!!";
				}
			}
			if (request.getParameter("btnProduct") != null) {
				String cateCode = request.getParameter("listCategory");
				/*System.out.println(request.getParameter("txtProductCode"));
				System.out.println(request.getParameter("txtProductName"));
				System.out.println(request.getParameter("txtProductCost"));
				System.out.println(request.getParameter("listCategory"));*/
				Product pro = new Product(request.getParameter("txtProductCode"), request.getParameter("txtProductName"),
						Integer.parseInt(request.getParameter("txtProductCost")),
						 request.getPart("fileImage").getSubmittedFileName(), cateCode);
				ProductModel productModel = new ProductModel(pro);
				if (productModel.insert() != 0){
					Part file = request.getPart("fileImage");
					ProductModel proModel = new ProductModel(file);
					String uploadRootPath = request.getServletContext().getRealPath("Images");
					proModel.uploadFile(uploadRootPath);
					page = "view.jsp";
				}else{
					page = "error.jsp";
					error = "Product cannot be inserted!!!";
				}
			}
		}
		request.setAttribute("error", error);
		request.getRequestDispatcher(page).forward(request, response);
	}

}
