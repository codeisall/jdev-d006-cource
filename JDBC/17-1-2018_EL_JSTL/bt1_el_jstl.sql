-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2019 at 12:56 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bt1_el_jstl`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `billcode` int(11) NOT NULL,
  `billdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`billcode`, `billdate`) VALUES
(1, '2019-01-26');

-- --------------------------------------------------------

--
-- Table structure for table `billdetail`
--

CREATE TABLE `billdetail` (
  `billcode` int(11) NOT NULL,
  `productcode` varchar(50) NOT NULL,
  `num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billdetail`
--

INSERT INTO `billdetail` (`billcode`, `productcode`, `num`) VALUES
(1, 'A1', 1),
(1, 'ASUS_UX370', 1),
(1, 'TV_SS_Q900', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catecode` varchar(50) NOT NULL,
  `catename` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catecode`, `catename`) VALUES
('LT', 'Laptop'),
('SP1', 'Smart Phone'),
('TV', 'Televison');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `productcode` varchar(50) NOT NULL,
  `productname` varchar(50) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `catecode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`productcode`, `productname`, `cost`, `image`, `catecode`) VALUES
('A1', 'Samsung S7 Edge', 15000000, 'h5.png', 'SP1'),
('A2', 'Iphone 7 Plus', 7000000, 'h2.png', 'SP1'),
('A3', 'Iphone 5S', 5000000, 'h8.png', 'SP1'),
('ASUS_UX370', 'ASUS ZenBook Flip S UX370UA', 20000000, 'zenbook-flip-s-01.jpg', 'LT'),
('TV_SS_Q900', '65\" Class Q900 QLED Smart 8K UHD TV (2019)', 20000000, '8KUHDTV(2019).jpg', 'TV');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`billcode`);

--
-- Indexes for table `billdetail`
--
ALTER TABLE `billdetail`
  ADD PRIMARY KEY (`billcode`,`productcode`),
  ADD KEY `productcode` (`productcode`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catecode`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productcode`),
  ADD KEY `catecode` (`catecode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `billcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `billdetail`
--
ALTER TABLE `billdetail`
  ADD CONSTRAINT `billdetail_ibfk_1` FOREIGN KEY (`billcode`) REFERENCES `bill` (`billcode`),
  ADD CONSTRAINT `billdetail_ibfk_2` FOREIGN KEY (`productcode`) REFERENCES `product` (`productcode`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`catecode`) REFERENCES `category` (`catecode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
