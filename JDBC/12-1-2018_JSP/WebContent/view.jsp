<%@page import="entities.Account"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">View Page</h1>
	<table align="center" border="3px" style="border-collapse: collapse;">
	<tr>
		<th>UserName</th>
		<th>Password</th>
		<th>Action</th>
		<th>Action</th>
	</tr>
	<%
	ArrayList list = (ArrayList)session.getAttribute("accountlist");
	for (Object obj : list){
		Account acc = (Account) obj;
	%>
	<tr>
		<td style="text-align: center"><%=acc.getUsername()%></td>
		<td style="text-align: center"><%=acc.getPassword()%></td>
		<td>
		<a href = "AccountServlet?task=delete&username=<%=acc.getUsername()%>&password=<%=acc.getPassword()%>">Delete</a>
		</td>
		<td>
		<a href = "update.jsp?username=<%=acc.getUsername()%>&password=<%=acc.getPassword()%>">Update</a>
		</td>
	</tr>
	<%}%>
	</table><br>
	<p align="center"><a href = "insert.jsp">Insert</a></p>
	<%if (request.getAttribute("Notify")!=null){%>
		<p align="center"><strong><%=request.getAttribute("Notify") %></strong>
		<%} %>
</body>
</html>