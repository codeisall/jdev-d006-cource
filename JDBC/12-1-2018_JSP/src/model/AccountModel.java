package model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

import MyConnect.MyConnect;
import entities.Account;

public class AccountModel {
	private Account acc;
	
	public AccountModel() {
	}
	
	public AccountModel(Account acc){
		this.acc  = acc;
	}
	
	public ArrayList getList(){
		ArrayList<Account> listAcc = new ArrayList<>();
		Connection cn = new MyConnect().getcn();
		
		if (cn == null){
			return null;
		}
		
		String sql = "select * from account";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			while (rs.next()){
					Account temp = new Account(rs.getString(1), rs.getString(2));
					listAcc.add(temp);
			}
			pt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listAcc;
	}
	
	public int insert(){
		Connection cn = new MyConnect().getcn();
		String sql = "insert into account "
				+ "values(?, ?)";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, acc.getUsername());
			pt.setString(2, acc.getPassword());
			return pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public int delete(){
		Connection cn = new MyConnect().getcn();
		String sql = "delete from account "
				+ "where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, acc.getUsername());
			return pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int update(){
		Connection cn = new MyConnect().getcn();
		String sql = "update account "
				+ "set password = ? "
				+ "where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, acc.getPassword());
			pt.setString(2, acc.getUsername());
			return pt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
