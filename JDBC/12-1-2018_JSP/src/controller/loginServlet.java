package controller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

import MyConnect.MyConnect;
import model.AccountModel;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection cn;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
    	cn = new MyConnect().getcn();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("txtname");
		String password = request.getParameter("txtpass");
		String page = "";
		String message = "";
		HttpSession session = request.getSession();
		
		String sql = "select password from account where username like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, username);
			ResultSet rs = pt.executeQuery();
			if (rs.next()){
				if (password.equals(rs.getString(1))){
					page = "view.jsp";
					session.setAttribute("accountlist", new AccountModel().getList());
					/*message = "Login Successfully!!!";*/
				}else{
					page = "error.jsp";
					/*message = "User Name does not exist!!!";*/
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/*if (username.equals("a") && password.equals("a")){
			page = "view.jsp";
		}else {
			page = "error.jsp";
		}*/
		/*request.setAttribute("Notify", message);//Tao 1 bien de dua ra client va thong bao mot thong diep message
*/		request.getRequestDispatcher(page).forward(request, response);
	}

}
