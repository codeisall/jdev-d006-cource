package controller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

import MyConnect.MyConnect;
import entities.Account;
import model.AccountModel;

/**
 * Servlet implementation class AccountServlet
 */
@WebServlet("/AccountServlet")
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection cn;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountServlet() {
        super();
        cn = new MyConnect().getcn();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String pass = request.getParameter("password");
		HttpSession session = request.getSession();
		String page = "";
		String message = "";
		AccountModel accModel = new AccountModel(new Account(username, pass));
		String task = request.getParameter("task");
		
		if (task.equals("delete")){
			if (accModel.delete() != 0){
				page = "view.jsp";
				message = "Deleted Successfully!!!";
				session.setAttribute("accountlist", new AccountModel().getList());
			}else {
				page = "error.jsp";
				message = "This username does not exist!!!";
			}
		}
		request.setAttribute("Notify", message);
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String task = request.getParameter("task");
		
		String username = request.getParameter("username");
		String pass = request.getParameter("password");
		HttpSession session = request.getSession();
		String page = "";
		String message = "";
		AccountModel accModel = new AccountModel(new Account(username, pass));
		
		if (task.equals("insert")){
			if (accModel.insert() != 0){
				page = "view.jsp";
				message = "Inserted Successfully!!!";
				session.setAttribute("accountlist", new AccountModel().getList());
			}else {
				page = "error.jsp";
				message = "This username does exist!!!";
			}
		}else if (task.equals("update")){
			if (accModel.update() != 0){
				page = "view.jsp";
				message = "Updated Successfully!!!";
				session.setAttribute("accountlist", new AccountModel().getList());
			}else {
				page = "error.jsp";
				message = "This username does exist!!!";
			}
		}else if (task.equals("login")){
			username = request.getParameter("txtname");
			pass = request.getParameter("txtpass");
			String sql = "select password from account where username like ?";
			try {
				PreparedStatement pt = cn.prepareStatement(sql);
				pt.setString(1, username);
				ResultSet rs = pt.executeQuery();
				if (rs.next()){
					if (pass.equals(rs.getString(1))){
						page = "view.jsp";
						session.setAttribute("accountlist", new AccountModel().getList());
						/*message = "Login Successfully!!!";*/
					}else{
						page = "error.jsp";
						/*message = "User Name does not exist!!!";*/
					}
				}
				pt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("Notify", message);
		request.getRequestDispatcher(page).forward(request, response);
	}

}
