-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2018 at 12:13 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bai4_111`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Username` varchar(50) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Password` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Role` varchar(30) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Username`, `Password`, `Role`) VALUES
('aaa', '111', '1');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `IDbook` varchar(50) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Title` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `IDcate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`IDbook`, `Title`, `Price`, `IDcate`) VALUES
('1', 'Chìa khóa vũ trụ của George - Stephen & Lucy Hawki', 1, 1),
('10', 'Tư duy tự do', 10, 5),
('11', 'Kẻ lãng mạn đi qua', 11, 6),
('12', 'Chinh phụ ngâm và tâm thức lãng mạn của kẻ lưu đày', 12, 6),
('13', 'Đừng Mở Cửa...', 13, 7),
('14', 'Mặt Nạ Trắng', 14, 7),
('15', 'Gặp lại ấu thơ', 15, 8),
('16', 'Năm tháng thơ ngây trôi qua', 16, 8),
('17', 'Tìm lại hương vị cổ truyền xưa', 17, 9),
('18', '200 món ná̂u ăn chay', 18, 9),
('19', 'Em học sử Việt', 19, 10),
('2', 'Người về từ Sao Hỏa - Andy Weir', 2, 1),
('20', 'Luyện giải toán 4', 20, 10),
('21', 'Object-Oriented Programming', 12, 1),
('3', '  21 Cách Học Tiếng Anh Du Kích - FuSuSu (Song ngữ', 3, 2),
('4', '  Tiếng Nhật Cho Mọi Người - Sơ Cấp 1 - Bản Dịch V', 4, 2),
('5', 'Sống Và Khát Vọng', 5, 3),
('6', 'Thiên Nga Đen', 6, 3),
('7', 'Chàng trai kỳ lạ', 7, 4),
('8', 'Subasa đội trưởng đội bóng đá nhí', 8, 4),
('9', 'Truy Tìm Người Hùng', 9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `IDcate` int(11) NOT NULL,
  `Name` varchar(30) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`IDcate`, `Name`) VALUES
(1, 'Khoa Hoc Vien Tuong'),
(2, 'Ngoai Ngu'),
(3, 'Tam Ly Hoc'),
(4, 'Truyen Tranh'),
(5, 'Hanh Dong Va Phieu Luu'),
(6, 'Lang Man'),
(7, 'Kinh Di'),
(8, 'Tho'),
(9, 'Sach Day Nau An'),
(10, 'Sach Giao Khoa'),
(11, 'Programing Language');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`IDbook`),
  ADD KEY `IDcate` (`IDcate`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`IDcate`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`IDcate`) REFERENCES `category` (`IDcate`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
