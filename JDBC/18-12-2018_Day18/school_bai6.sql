-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 03:14 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_bai6`
--

-- --------------------------------------------------------

--
-- Table structure for table `dkien`
--

CREATE TABLE `dkien` (
  `MAMH` varchar(30) DEFAULT NULL,
  `MAMH_TRUOC` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dkien`
--

INSERT INTO `dkien` (`MAMH`, `MAMH_TRUOC`) VALUES
('COSC3320', 'COSC3321'),
('COSC3321', 'COSC3319');

-- --------------------------------------------------------

--
-- Table structure for table `kqua`
--

CREATE TABLE `kqua` (
  `MASV` varchar(30) DEFAULT NULL,
  `MAKH` varchar(30) DEFAULT NULL,
  `DIEM` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kqua`
--

INSERT INTO `kqua` (`MASV`, `MAKH`, `DIEM`) VALUES
('8', '102', 9),
('25', '135', 9),
('7', '102', 10);

-- --------------------------------------------------------

--
-- Table structure for table `k_hoc`
--

CREATE TABLE `k_hoc` (
  `MAKH` varchar(30) NOT NULL,
  `MAMH` varchar(30) DEFAULT NULL,
  `HOCKY` int(11) DEFAULT NULL,
  `NAME` int(11) DEFAULT NULL,
  `GV` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_hoc`
--

INSERT INTO `k_hoc` (`MAKH`, `MAMH`, `HOCKY`, `NAME`, `GV`) VALUES
('101', 'COSC3319', 1, 2014, 'Thanh'),
('102', 'COSC3320', 2, 2015, 'Dang B'),
('135', 'COSC3319', 1, 2015, 'Tran Van A'),
('136', 'COSC3320', 2, 2016, 'Michael');

-- --------------------------------------------------------

--
-- Table structure for table `mhoc`
--

CREATE TABLE `mhoc` (
  `MAMH` varchar(30) NOT NULL,
  `TEN_MH` varchar(30) DEFAULT NULL,
  `TINCHI` int(11) DEFAULT NULL,
  `KHOA` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mhoc`
--

INSERT INTO `mhoc` (`MAMH`, `TEN_MH`, `TINCHI`, `KHOA`) VALUES
('COSC3319', 'Toan Roi Rac', 5, 'CNTT'),
('COSC3320', 'Co So Du Lieu', 4, 'CNTT'),
('COSC3321', 'Cau Truc Du Lieu', 4, 'CNTT');

-- --------------------------------------------------------

--
-- Table structure for table `svien`
--

CREATE TABLE `svien` (
  `MASV` varchar(30) NOT NULL,
  `TENSV` varchar(50) DEFAULT NULL,
  `NAM` int(11) DEFAULT NULL,
  `KHOA` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `svien`
--

INSERT INTO `svien` (`MASV`, `TENSV`, `NAM`, `KHOA`) VALUES
('25', 'NAM', 2, 'CNTT'),
('7', 'Loc', 2, 'CNTT'),
('8', 'Trang', 2, 'CNTT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dkien`
--
ALTER TABLE `dkien`
  ADD PRIMARY KEY (`MAMH_TRUOC`),
  ADD KEY `MAMH` (`MAMH`);

--
-- Indexes for table `kqua`
--
ALTER TABLE `kqua`
  ADD KEY `MASV` (`MASV`),
  ADD KEY `MAKH` (`MAKH`);

--
-- Indexes for table `k_hoc`
--
ALTER TABLE `k_hoc`
  ADD PRIMARY KEY (`MAKH`),
  ADD KEY `MAMH` (`MAMH`);

--
-- Indexes for table `mhoc`
--
ALTER TABLE `mhoc`
  ADD PRIMARY KEY (`MAMH`);

--
-- Indexes for table `svien`
--
ALTER TABLE `svien`
  ADD PRIMARY KEY (`MASV`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dkien`
--
ALTER TABLE `dkien`
  ADD CONSTRAINT `dkien_ibfk_1` FOREIGN KEY (`MAMH`) REFERENCES `mhoc` (`MAMH`);

--
-- Constraints for table `kqua`
--
ALTER TABLE `kqua`
  ADD CONSTRAINT `kqua_ibfk_1` FOREIGN KEY (`MASV`) REFERENCES `svien` (`MASV`),
  ADD CONSTRAINT `kqua_ibfk_2` FOREIGN KEY (`MAKH`) REFERENCES `k_hoc` (`MAKH`);

--
-- Constraints for table `k_hoc`
--
ALTER TABLE `k_hoc`
  ADD CONSTRAINT `k_hoc_ibfk_1` FOREIGN KEY (`MAMH`) REFERENCES `mhoc` (`MAMH`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
