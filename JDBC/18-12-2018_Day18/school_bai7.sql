-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 03:14 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_bai7`
--

-- --------------------------------------------------------

--
-- Table structure for table `bienlai`
--

CREATE TABLE `bienlai` (
  `MAKH` varchar(30) DEFAULT NULL,
  `MALH` varchar(30) DEFAULT NULL,
  `MAHV` varchar(30) DEFAULT NULL,
  `DIEM` float DEFAULT NULL,
  `KQUA` varchar(20) DEFAULT NULL,
  `XEPLOAI` varchar(20) DEFAULT NULL,
  `TIENNOP` int(11) DEFAULT NULL,
  `SOBL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bienlai`
--

INSERT INTO `bienlai` (`MAKH`, `MALH`, `MAHV`, `DIEM`, `KQUA`, `XEPLOAI`, `TIENNOP`, `SOBL`) VALUES
('PT297', 'L01', 'HV02', 8, 'Dau', 'Gioi', 900, 100),
('PT297', 'L01', 'HV01', 10, 'Khong Dau', 'Yeu', 900, 101);

-- --------------------------------------------------------

--
-- Table structure for table `giaovien`
--

CREATE TABLE `giaovien` (
  `MAGV` varchar(30) NOT NULL,
  `HOTEN` varchar(50) DEFAULT NULL,
  `NTNS` date DEFAULT NULL,
  `DC` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `giaovien`
--

INSERT INTO `giaovien` (`MAGV`, `HOTEN`, `NTNS`, `DC`) VALUES
('GV01', 'Ho Nhan', '1983-04-19', 'Nguyen Trai'),
('GV02', 'Tran Thanh', '1978-04-19', 'Tran Hung Dao'),
('GV03', 'Nguyen Lam Thanh', '1999-05-14', 'Thu Duc');

-- --------------------------------------------------------

--
-- Table structure for table `hocvien`
--

CREATE TABLE `hocvien` (
  `MAHV` varchar(30) NOT NULL,
  `HO` varchar(20) DEFAULT NULL,
  `TEN` varchar(30) DEFAULT NULL,
  `NTNS` date DEFAULT NULL,
  `DCI` varchar(50) DEFAULT NULL,
  `NNGHIEP` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hocvien`
--

INSERT INTO `hocvien` (`MAHV`, `HO`, `TEN`, `NTNS`, `DCI`, `NNGHIEP`) VALUES
('HV01', 'Nguyen', 'Tranh', '1990-11-20', 'Nguyen Hue', 'Giao Vien'),
('HV02', 'Le', 'Anh', '1996-08-09', 'Le Loi', 'SinHH Vien');

-- --------------------------------------------------------

--
-- Table structure for table `khoahoc`
--

CREATE TABLE `khoahoc` (
  `MAKH` varchar(30) NOT NULL,
  `TENMH` varchar(30) DEFAULT NULL,
  `BD` date DEFAULT NULL,
  `KT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `khoahoc`
--

INSERT INTO `khoahoc` (`MAKH`, `TENMH`, `BD`, `KT`) VALUES
('PT297', 'Tieng Phap Pho Thong', '1997-04-17', '1998-03-17'),
('PT298', 'Tieng Anh Co Ban', '1997-06-18', '1997-05-18');

-- --------------------------------------------------------

--
-- Table structure for table `lophoc`
--

CREATE TABLE `lophoc` (
  `MALOP` varchar(30) NOT NULL,
  `TENLOP` varchar(30) DEFAULT NULL,
  `MAKH` varchar(30) DEFAULT NULL,
  `MAGV` varchar(30) DEFAULT NULL,
  `SISODK` int(11) DEFAULT NULL,
  `LTRG` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lophoc`
--

INSERT INTO `lophoc` (`MALOP`, `TENLOP`, `MAKH`, `MAGV`, `SISODK`, `LTRG`) VALUES
('L01', 'Lop Tieng Phap', 'PT297', 'GV02', 15, 'HV01'),
('L02', 'Lop Tieng Anh', 'PT298', 'GV01', 20, 'HV02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bienlai`
--
ALTER TABLE `bienlai`
  ADD PRIMARY KEY (`SOBL`),
  ADD KEY `MAKH` (`MAKH`),
  ADD KEY `MALH` (`MALH`),
  ADD KEY `MAHV` (`MAHV`);

--
-- Indexes for table `giaovien`
--
ALTER TABLE `giaovien`
  ADD PRIMARY KEY (`MAGV`);

--
-- Indexes for table `hocvien`
--
ALTER TABLE `hocvien`
  ADD PRIMARY KEY (`MAHV`);

--
-- Indexes for table `khoahoc`
--
ALTER TABLE `khoahoc`
  ADD PRIMARY KEY (`MAKH`);

--
-- Indexes for table `lophoc`
--
ALTER TABLE `lophoc`
  ADD PRIMARY KEY (`MALOP`),
  ADD KEY `MAKH` (`MAKH`),
  ADD KEY `MAGV` (`MAGV`),
  ADD KEY `LTRG` (`LTRG`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bienlai`
--
ALTER TABLE `bienlai`
  ADD CONSTRAINT `bienlai_ibfk_1` FOREIGN KEY (`MAKH`) REFERENCES `khoahoc` (`MAKH`),
  ADD CONSTRAINT `bienlai_ibfk_2` FOREIGN KEY (`MALH`) REFERENCES `lophoc` (`MALOP`),
  ADD CONSTRAINT `bienlai_ibfk_3` FOREIGN KEY (`MAHV`) REFERENCES `hocvien` (`MAHV`);

--
-- Constraints for table `lophoc`
--
ALTER TABLE `lophoc`
  ADD CONSTRAINT `lophoc_ibfk_1` FOREIGN KEY (`MAKH`) REFERENCES `khoahoc` (`MAKH`),
  ADD CONSTRAINT `lophoc_ibfk_2` FOREIGN KEY (`MAGV`) REFERENCES `giaovien` (`MAGV`),
  ADD CONSTRAINT `lophoc_ibfk_3` FOREIGN KEY (`LTRG`) REFERENCES `hocvien` (`MAHV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
